var Program = require("../models/Program");
var ProgramService = require("../services/ProgramService");

/*exports.getProgram = async function (req, res, next) {
    var page = req.params.page ? req.params.page : 1;
    var limit = req.params.limit ? req.params.limit : 10;
    try {
        var data = await ProgramService.getProgram({} , page , limit);
        return res.status(200).json({
            programs: data
        });
    } catch (e) {
        return res.status(400).json({
            status: 400,
            message: e.message,
        });
    }
};*/

exports.getProgram = async function (req, res) {
    Program.find((err, c) => {
        if (err)
            res.json(err);
        else
            res.json(c)
    });
};

exports.addProgram = async function (req, res, next) {
    try {
                var content = ProgramService.addProgram(req.body);
                return res.status(200).json({
                    status: 200,
                    data: content,
                    message: "Program added succesfully",
                });
    }
    catch (e) {
        return res.status(400).json({
            status: 400,
            message: e.message,
        });
    }
};
