var Module = require("../models/Module");
var Training = require("../models/Training");
var ModuleService = require("../services/ModuleService");

exports.getModule = async function (req , res) {
    Module.find(function (err , modules) {
        if (err) res.json({message: 'There are no modules here.'});
        Module.find({})
            .populate('Training')
            .exec(function(err, modules) {
                res.json(modules);
            })
    });
};

exports.addModule = async function (req,res){
    try {
        var module = new Module(req.body);
        module.save(function (err) {
            return res.status(200).json({
                status: 200,
                data: module,
                message: "Module added succesfully",
            });
        });
    }
    catch (e) {
        return res.status(400).json({
            status: 400,
            message: e.message,
        });
    }
};

exports.getModuleById = async function (req, res , next){
    const id = req.params.id;
    Module.findById(id).exec((err, module) => {
        if (err || !module) {
            return res.status(400).json({
                error: 'module not found'
            });
        }
        res.json(module);
    });
};

exports.getModuleByTrainingId = async function (req, res , next){
    Module.find({training : req.params.training}).populate('training')
        .exec(function (err, modules) {
            if (err || !modules) {
                return res.status(400).json({
                    error: 'modules not found'
                });
            }
            res.status(200).json({
                modules : modules
            });
        });
};

