var Progression = require("../models/Progression");

exports.getProgression = async function (req , res) {
    Progression.find(function (err , progression) {
        if (err) res.json({message: 'There are no Progression here.'});
        Progression.find({})
            .populate('participation')
            .populate('module')
            .exec(function(err, progression) {
                res.json(progression);
            })
    });
};

exports.addProgression = async function (req,res){
    try {
        var progression = new Progression(req.body);
        progression.save(function (err) {
            return res.status(200).json({
                status: 200,
                data: Progression,
                message: "Progression added succesfully",
            });
        });
    }
    catch (e) {
        return res.status(400).json({
            status: 400,
            message: e.message,
        });
    }
};
