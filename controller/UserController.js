var UserService = require("../services/UserService");
var User = require("../models/User");
/*
var jwt = require("jsonwebtoken");
var bcrypt = require("bcrypt-nodejs");
var async = require("async");
const expressJwt = require('express-jwt');
var Role = require('../models/Role');
*/

exports.getUser = async function (req, res, next) {
    var page = req.params.page ? req.params.page : 1;
    var limit = req.params.limit ? req.params.limit : 10;
    try {
        var data = await UserService.getUser({} , page , limit);
        return res.status(200).json({
            users: data
        });
    } catch (e) {
        return res.status(400).json({
            status: 400,
            message: e.message,
        });
    }
};
exports.readController = async function (req, res) {
    const userId = req.params.id;
    User.findById(userId).exec((err, user) => {
        if (err || !user) {
            return res.status(400).json({
                error: 'User not found'
            });
        }
        user.hashed_password = undefined;
        user.salt = undefined;
        res.json(user);
    });
};
/*exports.readController = async function (req, res) {
    User.findOne({_id: req.params.id}, (err, c) => {
        if (c)
            res.json(c);
        else
            res.status(401).json('user Introuvable')
    });
};*/
exports.getUserById = async function (req, res, next) {
    try {
        var content = await UserService.getUserById(req.params.id);
        return res.status(200).json({
            status: 200,
            data: content,
            message: "User Succesfully found",
        });
    } catch (e) {
        return res.status(400).json({
            status: 400,
            message: e.message,
        });
    }
};
exports.updateController = (req, res) => {
    // console.log('UPDATE USER - req.user', req.user, 'UPDATE DATA', req.body);
    const { name, password, email, phoneNumber, address, businessName } = req.body;
    User.findOne({ _id: req.user._id }, (err, user) => {
        if (err || !user) {
            return res.status(400).json({
                error: 'User not found'
            });
        }
        if (!name) {
            return res.status(400).json({
                error: 'Name is required'
            });
        } else {
            user.name = name;
        }
        if (!phoneNumber) {
            return res.status(400).json({
                error: 'phoneNumber is required'
            });
        } else {
            user.phoneNumber = phoneNumber;
        }
        if (!email) {
            return res.status(400).json({
                error: 'email is required'
            });
        } else {
            user.email = email;
        }
        if (!businessName) {
            return res.status(400).json({
                error: 'businessName is required'
            });
        } else {
            user.businessName = businessName;
        }
        if (!address) {
            return res.status(400).json({
                error: 'address is required'
            });
        } else {
            user.address = address;
        }
        if (password) {
            if (password.length < 6) {
                return res.status(400).json({
                    error: 'Password should be min 6 characters long'
                });
            } else {
                user.password = password;
            }
        }
        user.save((err, updatedUser) => {
            if (err) {
                console.log('USER UPDATE ERROR', err);
                return res.status(400).json({
                    error: 'User update failed'
                });
            }
            updatedUser.hashed_password = undefined;
            updatedUser.salt = undefined;
            res.json(updatedUser);
        });
    });
};
exports.addUser = async function (req, res, next) {
    try {
        User.findOne({ Email: req.body.Email }).then(user => {
            if (user) {
                var content = UserService.addUser(req.body);
                return res.status(200).json({
                    status: 200,
                    data: content,
                    message: "User added succesfully",
                });
            }else {
                res.json("User already exist")
            }
        });
    }
    catch (e) {
        return res.status(400).json({
            status: 400,
            message: e.message,
        });
    }
};

/*exports.deleteUser = async function (req,res) {
  try {
      User.findOne({ id : req.body.__id}).then(user => {
          if (user) {
              var content = UserService.removeUser(req.body);
              return res.status(200).json({
                  status: 200,
                  data: content,
                  message: "User deleted succesfully",
              });
          }else {
              res.json("User not found")
          }
      });
  }  catch (e) {
      return res.status(400).json({
          status: 400,
          message: e.message,
      });
  }
};*/


/*const storage = multer.diskStorage({
    destination: "./client/src/assests/img/faces",
    filename: (req, file, cb) => {
        cb(null,"IMAGE-" + Date.now() + path.extname(file.originalname));
    }
});

const upload = multer({
    storage: storage,
    limits:{fileSize: 1000000},
}).single("image");

exports.upload = async function (req, res) {
        console.log("Request ---", req.body);
        console.log("Request file ---", req.file);//Here you get file.
        const url = req.protocol + '://' + req.get('host');
        Now do where ever you want to do
        User.findOneAndUpdate({_id : req.body._id} ,  {image:  req.file.filename } , {res: true} , function (err,u) {
            if (err) res.json(err);
            else res.json(u);
        });
       /* if(!err) {
            return res.send(200).end();
        }
};*/
/*
exports.getById = async function (req, res, next) {
    // regular users can get their own record and admins can get any record
    if (req.params.id !== req.user.id && req.user.role !== Role.Admin) {
        return res.status(401).json({ message: 'Unauthorized' });
    }
    UserService.getById(req.params.id)
        .then(user => user ? res.json(user) : res.sendStatus(404))
        .catch(next);
};
exports.getAll = async function (req, res, next) {
    UserService.getAll()
        .then(users => res.json(users))
        .catch(next);
};
 */
