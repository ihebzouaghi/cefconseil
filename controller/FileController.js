const File = require("../models/File");
const FileService = require("../services/FileService");


exports.getFiles = async function (req , res) {
    File.find(function (err , modules) {
        if (err) res.json({message: 'There are no File here.'});
        File.find({})
            .populate('step')
            .populate({
                path: 'step',
                populate: {path: 'module',
                    populate: {path: 'training'}
                }

            })
            .exec(function(err, modules) {
                res.json({files:modules});
            })
    });
};

exports.getFileById = async function (req, res , next){
    const id = req.params.id;
    File.findById(id).exec((err, file) => {
        if (err || !file) {
            return res.status(400).json({
                error: 'File not found'
            });
        }
        res.json(file);
    });
};

exports.deleteFile = async (req, res) => {
    const {id} = req.params;
    let file = await File.findByIdAndDelete(id);
    return res.status(202).send({
        error: false,
        file
    })
};
