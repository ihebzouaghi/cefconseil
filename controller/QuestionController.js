const Question = require("../models/Question");
const QuestionService = require("../services/QuestionService");


exports.addQuestion = async function (req, res) {
    try {
        Question.findOne({ title: req.body.title }).then(question => {
           // if (!question) {
                const content = QuestionService.addQuestion(req.body);
                return res.status(200).json({
                    status: 200,
                    data: content,
                    message: "Question added succesfully",
                });
           /* }else {
                res.json("Question already exist")
            }*/
        });
    }
    catch (e) {
        return res.status(400).json({
            status: 400,
            message: e.message,
        });
    }
};

exports.getQuestion = async function (req , res) {
    Question.find(function (err , modules) {
        if (err) res.json({message: 'There are no Question here.'});
        Question.find({})
            .populate('step')
            .populate({
                path: 'step',
                populate: { path: 'module',
                            populate:{ path:'training'}}
            })
            .exec(function(err, modules) {
                res.json({questions:modules});
            })
    });
};



exports.getQuestionById = async function (req, res , next){
    const id = req.params.id;
    Question.findById(id).exec((err, question) => {
        if (err || !question) {
            return res.status(400).json({
                error: 'Question not found'
            });
        }
        res.json(question);
    });
};


exports.updateQuestion = async (req, res , next) => {
    const {id} = req.params;
    let question = await Step.findByIdAndUpdate(id, req.body , { new : true} , next);
    return res.status(202).send({
        error: false,
        question
    })
};

exports.deleteQuestion = async (req, res) => {
    const {id} = req.params;
    let question = await Question.findByIdAndDelete(id);
    return res.status(202).send({
        error: false,
        question
    })
};
