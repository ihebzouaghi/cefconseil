var Training = require("../models/Training");
var TrainingService = require("../services/TrainingService");

exports.getTraining = async function (req, res) {
    Training.find((err, c) => {
        if (err)
            res.json(err);
        else
            res.status(200).json({
                trainings : c
            });
    });
};

exports.addTraining = async function (req, res, next) {
    try {
        var content = TrainingService.addTraining(req.body);
        return res.status(200).json({
            status: 200,
            data: content,
            message: "Training added succesfully",
        });
    }
    catch (e) {
        return res.status(400).json({
            status: 400,
            message: e.message,
        });
    }
};

exports.getTrainingById = async function (req, res , next){
    const id = req.params.id;
    Training.findById(id).exec((err, training) => {
        if (err || !training) {
            return res.status(400).json({
                error: 'Training not found'
            });
        }
        res.json(training);
    });
};

/*exports.update = async function (req, res, next) {
    try {
        var content = await TrainingService.updateTraining(req.body.id, req.body);
        return res.status(200).json({
            status: 200,
            data: content,
            message: "Succesfully updated",
        });
    } catch (e) {
        return res.status(400).json({
            status: 400,
            message: e.message,
        });
    }
};
 */

exports.updateTraining = async (req, res , next) => {
    const {id} = req.params;
    let training = await Training.findByIdAndUpdate(id, req.body , { new : true} , next);
    return res.status(202).send({
        error: false,
        training
    })
};

exports.deleteTraining = async (req, res) => {
    const {id} = req.params;
    let training = await Training.findByIdAndDelete(id);
    return res.status(202).send({
        error: false,
        training
    })
};

