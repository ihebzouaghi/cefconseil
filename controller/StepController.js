const Step = require("../models/Step");
const StepService = require("../services/StepService");


exports.addStep = async function (req, res) {
    try {
        Step.findOne({ title: req.body.title }).then(step => {
            if (!step) {
                const content = StepService.addStep(req.body);
                return res.status(200).json({
                    status: 200,
                    data: content,
                    message: "Step added succesfully",
                });
            }else {
                res.json("Step already exist")
            }
        });
    }
    catch (e) {
        return res.status(400).json({
            status: 400,
            message: e.message,
        });
    }
};

exports.getStep = async function (req , res) {
    Step.find(function (err , modules) {
        if (err) res.json({message: 'There are no Step here.'});
        Step.find({})
            .populate('module')
            .populate({
            path: 'module',
            populate: { path: 'training' }
        })
            .exec(function(err, modules) {
                res.json({steps:modules});
            })
    });
};



exports.getStepById = async function (req, res , next){
    const id = req.params.id;
    Step.findById(id).exec((err, step) => {
        if (err || !step) {
            return res.status(400).json({
                error: 'Step not found'
            });
        }
        res.json(step);
    });
};


exports.updateStep = async (req, res , next) => {
    const {id} = req.params;
    let step = await Step.findByIdAndUpdate(id, req.body , { new : true} , next);
    return res.status(202).send({
        error: false,
        step
    })
};

exports.deleteStep = async (req, res) => {
    const {id} = req.params;
    let step = await Step.findByIdAndDelete(id);
    return res.status(202).send({
        error: false,
        step
    })
};
