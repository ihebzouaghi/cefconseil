var Participation = require("../models/Participation");

exports.getParticipation = async function (req , res) {
    Participation.find(function (err , participation) {
        if (err) res.json({message: 'There are no participation here.'});
        Participation.find({})
            .populate('learner')
            .populate('training')
            .exec(function(err, participation) {
                res.status(200).json({
                    participation : participation
                });
            })
    });
};

exports.addParticipation = async function (req,res){
    try {
        var participation = new Participation(req.body);
        participation.save(function (err) {
            return res.status(200).json({
                status: 200,
                data: participation,
                message: "Participation added succesfully",
            });
        });
    }
    catch (e) {
        return res.status(400).json({
            status: 400,
            message: e.message,
        });
    }
};

exports.getParticipationByLearner = async function (req,res) {

    Participation.find({learner : req.params.learner}).populate('learner').populate('training')
        .exec(function (err, participations) {
            if (err || !participations) {
                return res.status(400).json({
                    error: 'participations not found'
                });
            }
            res.status(200).json({
                participations : participations
            });
        });

};

exports.getParticipationById = async function (req, res , next){
    const id = req.params.id;
    Participation.findById(id).populate('learner').populate('training').exec((err, participation) => {
        if (err || !participation) {
            return res.status(400).json({
                error: 'participation not found'
            });
        }
        res.json(participation);
    });
};

exports.addLikes = async function (req,res,next){
    const id = req.params.id;
    let counter = req.body.likes;
        Participation. updateOne({_id : id},{$inc:{likes : 1}}).exec()
            .then(rest=>{
                res.status(200).json({message:'liked'});
            })
};

exports.deleteLearnerFromParticipation = async (req, res) => {
    let member = await Participation.splice({learner : req.params.learner} , 1).populate('learner').populate('training')
    return res.status(202).send({
        error: false,
        member
    })
};




