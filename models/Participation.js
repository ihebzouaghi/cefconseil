const mongoose = require('mongoose');

const participationSchema = new mongoose.Schema(
    {
        learner: [
            {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
        }
        ],
        training: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Training'
        },
        likes: [
            {
                user: {
                    type: mongoose.Schema.Types.ObjectId,
                    ref: "User",
                },
            },
        ],
        dislikes : [
            {
                user: {
                    type: mongoose.Schema.Types.ObjectId,
                    ref: "User",
                },
            },
        ],
    }

);

module.exports = mongoose.model('Participation', participationSchema);
