const mongoose = require('mongoose');

const trainingSchema = new mongoose.Schema(
    {
        title: {
            type: String,
            trim: true,
            required: true
        },
        image : {
            type:String,
            default:"me.jpg"
        },
        description : {
            type : String
        }

    }

);

module.exports = mongoose.model('Training', trainingSchema);
