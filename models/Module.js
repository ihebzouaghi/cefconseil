const mongoose = require('mongoose');

const moduleSchema = new mongoose.Schema(
    {
        titleModule: {
            type: String,
            trim: true,
            required: true
        },
        training: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Training'
        }
    }

);

module.exports = mongoose.model('Module', moduleSchema);
