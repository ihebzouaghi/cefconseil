const mongoose = require('mongoose');

const stepSchema = new mongoose.Schema(
    {
       step :
            {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'Step'
            },

        name: {
            type: String,
            required: true,
        },
        options: [
            {
                OptionName:{
                    type: String,
                    required: true,
                },
                OptionId:{
                    type: String,
                    required: true,

                }
            }
        ],

        selectedOption: {
            type: String,
            required: false,
        },

        trueOption:  [
            {
                selectId:{
                    type: String,
                    required: false,
                    unique:true
                },
                selectState:{
                    type: String,
                    required: false,

                }
            }
        ],



    }
);

module.exports = mongoose.model('Question', stepSchema);
