const mongoose = require('mongoose');

const stepSchema = new mongoose.Schema(
    {
        module :
            {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'Module'
            },

        type: {
            type: String,
            //required: true,
            enum: ['Lesson', 'Quiz','Hybrid']
        },
        title: {
            type: String,
            trim: true,
            //required: true
        },
        textSimple: {
            type: String,
            trim: true,
            //required: true
        },
        externalPath: {
            type: String,
            trim: true,
            //required: true
        }

    }
);

module.exports = mongoose.model('Step', stepSchema);
