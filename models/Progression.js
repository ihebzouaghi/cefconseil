const mongoose = require('mongoose');

const progressionSchema = new mongoose.Schema(
    {
        participation: {
            type : Schema.Types.ObjectId,
            ref : 'Participation'
        },
        module: {
            type : Schema.Types.ObjectId,
            ref : 'Module'
        },
    }
);

module.exports = mongoose.model('Progression', progressionSchema);
