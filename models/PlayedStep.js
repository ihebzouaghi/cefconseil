const mongoose = require('mongoose');

const playedStepSchema = new mongoose.Schema(
    {
        state: {
            type: String,
            default : 'Not Yet'
        },
        answer : {
          type : String
        },
        progression : {
            type: Schema.Types.ObjectId,
            ref: 'Progression'
        },
        step : {
            type: Schema.Types.ObjectId,
            ref: 'Step'
        }
    }
);

module.exports = mongoose.model('PlayedStep', playedStepSchema);
