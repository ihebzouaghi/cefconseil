const mongoose = require('mongoose');

const programSchema = new mongoose.Schema(
    {
        email: {
            type: String,
            trim: true,
            required: true,
            unique: true,
            lowercase: true
        },
        title: {
            type: String,
            trim: true,
            required: true
        },
        start: {
            type: Date,
            required: true
        },
        end: {
            type: Date,
            required: true
        }
    }

);

module.exports = mongoose.model('Program', programSchema);
