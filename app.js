const createError = require("http-errors");
const express = require("express");
const path = require("path");
const cookieParser = require("cookie-parser");
const logger = require("morgan");
const cors = require('cors');

//var expressJwt = require("express-jwt");
require("dotenv").config({
    path: path.join(__dirname, ".env"),
});

const app = express();

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

const authRouter = require('./routes/auth.route');
const userRouter = require('./routes/User');
const programRouter = require('./routes/ProgramRoute');
const trainingRouter = require('./routes/TrainingRoute');
const moduleRouter = require('./routes/ModuleRoute');
const participationRouter = require('./routes/ParticipationRoute');
const stepRouter = require ('./routes/StepRoute');
const fileRoute = require('./routes/file');
const questionRoute = require('./routes/QuestionRoute');


//cors
if (process.env.NODE_ENV === 'development') {
    app.use(cors({
        origin: process.env.CLIENT_URL
    }));
    app.use(logger('dev'));
    app.use(fileRoute);
}

app.use("/api/user", userRouter);
app.use("/api/user", authRouter);
app.use("/api/program" , programRouter);
app.use("/api/training" , trainingRouter);
app.use("/api/module" , moduleRouter);
app.use("/api/participation" , participationRouter);
app.use("/api/step" , stepRouter );
app.use("/api/file", fileRoute);
app.use("/api/question", questionRoute);
//app.use("/api/training", require("./routes/Training"));

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get("env") === "development" ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render("error");
});
const port = process.env.PORT;
console.log(`Your port is ${port}`);
const mongoose = require("mongoose");

// Don't forget to set "MONGODB_URI" in ~/server/.env
const uri =
    process.env.MONGODB_URI ||
    `mongodb://localhost/please-set-process-env-mongodb-uri`;


// add if error  , useFindAndModify: false

mongoose
    .connect(uri, { useNewUrlParser: true, useUnifiedTopology: true , useCreateIndex: true , useFindAndModify: false })
    .then((x) => {
        console.log(
            `Connected to Mongo! Database name: "${x.connections[0].name}"`
        );
    })
    .catch((err) => {
        console.error("Error connecting to mongo", err);
    });

module.exports = app;
