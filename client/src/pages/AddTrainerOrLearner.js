import React, {useEffect, useState} from 'react';
import { toast } from 'react-toastify';
import { Redirect } from 'react-router-dom';
import {makeStyles} from "@material-ui/core/styles";
import CardHeader from "../dashboard/Card/CardHeader";
import Card from "../dashboard/Card/Card";
import CardBody from "./../dashboard/Card/CardBody.js";
import CustomInput from "../dashboard/CustomInput/CustomInput";
import GridItem from "../dashboard/Grid/GridItem.js";
import GridContainer from "../dashboard/Grid/GridContainer.js";
import Button from "../dashboard/CustomButtons/Button.js";
import CardFooter from "../dashboard/Card/CardFooter.js";
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import { isAuth, getCookie } from '../helpers/auth';
import axios from 'axios';
import {auto} from "async";



const styles = {
    cardCategoryWhite: {
        color: "rgba(255,255,255,.62)",
        margin: "0",
        fontSize: "14px",
        marginTop: "0",
        marginBottom: "0"
    },
    cardTitleWhite: {
        color: "#FFFFFF",
        marginTop: "0px",
        minHeight: "auto",
        fontWeight: "300",
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
        marginBottom: "3px",
        textDecoration: "none"
    },
    table: {
        minWidth: 700,
        maxWidth:800,
    },
    formControl: {
        margin: "1",
        maxWidth: '100%',
        minWidth:'100%',
        marginBottom: "",
        top: 27,
    },
    selectEmpty: {
        marginTop: "2",
    },

    //select: { textAlignLast: "left" },
    //option :{ textAlignLast: "left"  }

};
const useStyles = makeStyles(styles);



const Register = () => {

    const [formData, setFormData] = useState({
        name: '',
        email: '',
        businessName:'',
        phoneNumber: '',
        address: '',
        role:'',
        image:'',
        password1: '',
        textChange: 'Add'
    });



//Load profile info (businessName)
    useEffect(() => {
        loadProfile();
    }, []);

    const loadProfile = () => {
        const token = getCookie('token');
        axios
            .get(`${process.env.REACT_APP_API_URL}/${isAuth()._id}`, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            })
            .then(res => {
                const { role, name, email,businessName } = res.data;
                setFormData({ ...formData, role, name, email, businessName });
            })
            .catch(err => {
                toast.error(`Error To Your Information ${err.response.statusText}`);
                if (err.response.status === 401) {
                    console.log(name);
                }
            });
    };






    const { name, email, businessName ,phoneNumber, address, role, image, textChange } = formData;
    const handleChange = text => e => {
        setFormData({ ...formData, [text]: e.target.value });
    };


    const handleSubmit = e => {
        e.preventDefault();

        if (name && email && phoneNumber && businessName && address && role ) {
            setFormData({ ...formData, textChange: 'Submitting' });

            axios
                .post(`http://localhost:5000/api/user/add`, {
                    name,
                    email,
                    businessName,
                    phoneNumber,
                    address,
                    role,
                    image,
                    password: phoneNumber
                })
                .then(res => {
                    setFormData({
                        ...formData,
                        name: '',
                        email: '',
                        businessName:'',
                        phoneNumber: '',
                        address: '',
                        role:'',
                        image:'',
                        password1: phoneNumber,
                        textChange: 'Submitted'
                    });

                    toast.success(res.data.message);
                    console.log(formData.password1);
                })
                .catch(err => {
                    setFormData({
                        ...formData,
                        name: '',
                        email: '',
                        businessName:'' ,
                        phoneNumber: '',
                        address: '',
                        role:'',
                        password1: '',
                        textChange: 'Sign Up'
                    });
                    console.log(err.response);
                    toast.error(err.response.data.errors);
                });

        } else {

            toast.error('Please fill all fields');
        }
    };
    const classes = useStyles();
    return (
        <div className='min-h-screen bg-gray-100 text-gray-900 flex justify-center'>
            {!isAuth() ? <Redirect to='/' /> : null}

            <div>
                <GridContainer>
                    <GridItem xs={12} sm={12} md={8}>
                        <Card>
                            <CardHeader color="primary">
                                <h4 className={classes.cardTitleWhite}>Add Trainer/Learner</h4>
                                <p className={classes.cardCategoryWhite}>Complete User Information</p>
                            </CardHeader>
                            <CardBody>


                                <form
                                    className='w-full flex-1 mt-8 text-indigo-500'
                                    onSubmit={handleSubmit}
                                >
                                    <GridContainer>
                                        <GridItem xs={12} sm={12} md={5}>
                                            <CustomInput
                                                labelText='Business Name...'
                                                id="businessName"
                                                formControlProps={{
                                                    fullWidth: true,
                                                    disabled: false
                                                }}
                                                inputProps={{
                                                    onChange: handleChange('businessName'),
                                                    type: "name",

                                                }}
                                            />
                                        </GridItem>
                                        <GridItem xs={12} sm={12} md={7}>
                                            <CustomInput
                                                type='text'
                                                labelText="Name..."
                                                id="name"
                                                formControlProps={{
                                                    fullWidth: true
                                                }}
                                                inputProps={{
                                                    onChange: handleChange('name'),
                                                    type: "name",

                                                }}
                                            />
                                        </GridItem>

                                    </GridContainer>
                                    <GridContainer>
                                        <GridItem xs={12} sm={12} md={6}>
                                            <CustomInput
                                                type='email'
                                                labelText="Email..."
                                                id="email"
                                                formControlProps={{
                                                    fullWidth: true
                                                }}
                                                inputProps={{
                                                    onChange: handleChange('email'),
                                                    type: "email",

                                                }}
                                            />
                                        </GridItem>
                                        <GridItem xs={12} sm={12} md={6}>
                                            <CustomInput
                                                type='tel'
                                                required
                                                minLength='8'
                                                labelText="Phone..."
                                                id="phoneNumber"
                                                formControlProps={{
                                                    fullWidth: true
                                                }}
                                                inputProps={{
                                                    onChange: handleChange('phoneNumber'),
                                                    type: "phoneNumber",

                                                }}
                                            />
                                        </GridItem>
                                        </GridContainer>
                                        <GridContainer>
                                        <GridItem xs={12} sm={12} md={8}>
                                            <CustomInput
                                                type='text'
                                                required
                                                labelText="Address..."
                                                id="address"
                                                formControlProps={{
                                                    fullWidth: true
                                                }}
                                                inputProps={{
                                                    onChange: handleChange('address'),
                                                    type: "address",

                                                }}
                                            />
                                        </GridItem>
                                            <GridItem xs={12} sm={12} md={4}>
                                                <FormControl className={classes.formControl}>
                                                    <InputLabel id="demo-simple-select-label">Role...</InputLabel>
                                                    <Select
                                                        style={styles}
                                                        labelId="demo-simple-select-label"
                                                        id="role"
                                                        formControlProps={{
                                                            fullWidth: true
                                                        }}
                                                        inputProps={{

                                                            onChange: handleChange('role'),


                                                        }}
                                                       >

                                                        <MenuItem value="Learner">Learner</MenuItem>
                                                        <MenuItem value="Trainer">Trainer</MenuItem>
                                                    </Select>
                                                </FormControl>
                                            </GridItem>
                                        </GridContainer>

                                    <GridContainer>
                                        <CardFooter>
                                            <Button
                                                type='submit'
                                                color="primary"

                                            >
                                                <span className='ml-3'>{textChange}</span>
                                            </Button>
                                        </CardFooter>
                                    </GridContainer>



                                </form>

                            </CardBody>
                            <CardFooter>



                            </CardFooter>


                        </Card>
                    </GridItem>
                </GridContainer>
            </div>
        </div>

    );
};

export default Register;
