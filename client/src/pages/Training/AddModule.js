import React, {useEffect, useState} from "react";
import axios from "axios";
import {toast} from "react-toastify";
import DialogContentText from "@material-ui/core/DialogContentText";
import CustomInput from "../../components/CustomInput/CustomInput";
import Button from "@material-ui/core/Button";
import DialogContent from "@material-ui/core/DialogContent/DialogContent";
import { withRouter , useLocation } from 'react-router-dom';

const AddModule = (props) => {
    const [formData, setFormData] = useState({
        title: '',
        training : '',
        textChange: 'Add'
    });

    const TrainingId = props.match.params.id ;
   /* const location = useLocation();
    const {name} = location.state;
    console.log(name);
    */

    const { title, textChange , training } = formData;

    const handleChange = text => e => {
        setFormData({ ...formData, [text]: e.target.value });
    };

    const handleSubmit = e => {
        e.preventDefault();
        if (title) {
            setFormData({ ...formData, textChange: 'Submitting' });
            axios
                .post(`http://localhost:5000/api/module/add`, {
                    title,
                    training : TrainingId
                })
                .then(res => {
                    setFormData({
                        ...formData,
                        title: '',
                        training : TrainingId,
                        textChange: 'Submitted'
                    });
                    console.log(formData);
                    window.location.reload(false);
                })
                .catch(err => {
                    setFormData({
                        ...formData,
                        title: '',
                        training : TrainingId,
                        textChange: 'Module Added'
                    });
                    console.log(err.response);
                });

        } else {
            toast.error('Please fill all fields');
        }
    };

    return (
        <DialogContent>
            <DialogContentText>
                Here you can add a new Module
            </DialogContentText>
            <form onSubmit={handleSubmit}>
                <CustomInput
                    labelText="Title..."
                    id="title"
                    formControlProps={{
                        fullWidth: true
                    }}
                    inputProps={{
                        onChange: handleChange('title'),
                        type: "text"
                    }}
                />
                < input id="training" value={TrainingId} onChange={handleChange('training')} hidden={true}/>
                <Button variant="contained" type='submit' color="primary">ADD MODULE
                </Button>
            </form>
        </DialogContent>

    );
};

export default withRouter(AddModule);
