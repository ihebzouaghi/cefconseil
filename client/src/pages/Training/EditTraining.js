import React, {useEffect, useState} from 'react';
import {useParams} from "react-router-dom";
import PropTypes from 'prop-types';
import clsx from 'clsx';
import SwipeableViews from 'react-swipeable-views';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Zoom from '@material-ui/core/Zoom';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import EditIcon from '@material-ui/icons/Edit';
import UpIcon from '@material-ui/icons/KeyboardArrowUp';
import { green } from '@material-ui/core/colors';
import Box from '@material-ui/core/Box';
import axios from "axios";
import {toast} from "react-toastify";
import CustomInput from "../../components/CustomInput/CustomInput";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import {useHistory} from "react-router-dom";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import LinearProgress from '@material-ui/core/LinearProgress';
import Tooltip from "@material-ui/core/Tooltip/Tooltip";
import Dialog from "@material-ui/core/Dialog/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent/DialogContent";
import DialogActions from "@material-ui/core/DialogActions/DialogActions";
import AddModule from "./AddModule";
import { withRouter } from 'react-router-dom';
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import Accordion from "@material-ui/core/Accordion";
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import {DndProvider, DropTarget, useDrag, useDrop} from 'react-dnd';
import {HTML5Backend} from "react-dnd-html5-backend";
import './dnd.css';
import Avatar from "@material-ui/core/Avatar";
import Draggable from 'react-draggable';
import Board from "../DragAndDrop/Board";
import Card from "../DragAndDrop/Card";
import Chip from "@material-ui/core/Chip";
import Pdf from "react-to-pdf";
import Autocomplete from "@material-ui/lab/Autocomplete";
import Checkbox from "@material-ui/core/Checkbox";
import TextField from "@material-ui/core/TextField";
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from '@material-ui/icons/Delete';
import DialogContentText from "@material-ui/core/DialogContentText";
import CardContent from "@material-ui/core/CardContent";
import GridContainer from "../../components/Grid/GridContainer";
import classNames from "classnames";
import {isAuth} from "../../helpers/auth";

const icon = <CheckBoxOutlineBlankIcon fontSize="small" />;
const checkedIcon = <CheckBoxIcon fontSize="small" />;


function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <Typography
            component="div"
            role="tabpanel"
            hidden={value !== index}
            id={`action-tabpanel-${index}`}
            aria-labelledby={`action-tab-${index}`}
            {...other}
        >
            {value === index && <Box p={3}>{children}</Box>}
        </Typography>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};

function a11yProps(index) {
    return {
        id: `action-tab-${index}`,
        'aria-controls': `action-tabpanel-${index}`,
    };
}

const useStyles = makeStyles((theme) => ({
    root: {
        backgroundColor: theme.palette.background.paper,
        //width: 500,
        position: 'relative',
        minHeight: 600,
        maxHeight : 700,
        fullWidth : true,
        flexGrow: 1,
        '& > *': {
            margin: theme.spacing(1),
        },
    },
    fab: {
        position: 'absolute',
        bottom: theme.spacing(2),
        right: theme.spacing(2),
    },
    fabGreen: {
        color: theme.palette.common.white,
        backgroundColor: green[500],
        '&:hover': {
            backgroundColor: green[600],
        },
    },
    paper: {
        height: 600,
        width: 500,
        boxShadow: "none"
    },
    control: {
        padding: theme.spacing(2),
    },
    input: {
        display: 'none',
    },
    iheb : {
        position: 'absolute',
        bottom: 60,
        left: 0,
    },
    ahmed : {
        position: 'absolute',
        bottom: 0,
        left: 0,
    },
    heading: {
        fontSize: theme.typography.pxToRem(15),
        flexBasis: '33.33%',
        flexShrink: 0,
    },
    secondaryHeading: {
        fontSize: theme.typography.pxToRem(15),
        color: theme.palette.text.secondary,
    },
    avatar: {
        display: "inline-block",
        border: "2px solid white",
        "&:not(:first-of-type)": {
            marginLeft: -theme.spacing.unit
        }
    },
    avatar2 : {
        display: "inline-block",
        border: "4px solid #3A9FEC",
        "&:not(:first-of-type)": {
            marginLeft: -theme.spacing.unit
        }
    },
    ekhtafi : {
        width: 803,
        height: 1132 ,
    },
    main: {
        top : 70,
        background: "#FFFFFF",
        position: "relative",
        zIndex: "3"
    },
    mainRaised: {
        margin: "-60px 30px 0px",
        borderRadius: "6px",
        boxShadow:
            "0 16px 24px 2px rgba(0, 0, 0, 0.14), 0 6px 30px 5px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(0, 0, 0, 0.2)"
    },
}));

function LinearProgressWithLabel(props) {
    return (
        <Box display="flex" alignItems="center">
            <Box width="100%" mr={1}>
                <LinearProgress variant="determinate" {...props} />
            </Box>
            <Box minWidth={35}>
                <Typography variant="body2" color="textSecondary">{`${Math.round(
                    props.value,
                )}%`}</Typography>
            </Box>
        </Box>
    );
}

const EditTraining = (props) => {

    let { id } = useParams();
    const history = useHistory();
    const navigateToModule = () => {
        props.history.push('/AddModule', { id : id });
    };

    //pdf
    const ref = React.createRef();
    const options = {
        orientation: 'landscape',
        unit: 'in',
        format: [4,2]
    };

    const [formData, setFormData] = useState({
        title: '',
        description: '',
        textChange: 'Update',
        file : null,

        currentFile: undefined,
        previewImage: undefined,
        message: "",
        isError: false,
        imageInfos: [],
    });

    const [iheb , setIheb] = useState({
        titleModule : '',
        modules : [],
        training : {}
    });

    const [part , setPart] = useState({
        participation : [],
        learner : [],
        training : {}
    });

    const [members , setMembers] = useState({
        users : []
    });

    useEffect(() => {
        loadTraining();
        loadModules();
        loadParticipation();
        loadMembers();
    }, []);

    const loadMembers = () => {
       axios
        .get('http://localhost:5000/api/user/')
           .then(res=> {
               setMembers(res.data);
               console.log(res.data);
           })
           .catch(err => {
               if (err.res.status === 401) {
                   console.log(members);
               }
           });
    };

    const loadModules = () => {
        axios
            .get(`http://localhost:5000/api/module/all/`+id)
            .then(res => {
               setIheb(res.data);
                console.log(res.data);
            })
            .catch(err => {
                if (err.res.status === 401) {
                    console.log(iheb);
                }
            });
    };

    const loadTraining = () => {
        axios
            .get(`http://localhost:5000/api/training/`+id)
            .then(res => {
            const {title, description , image} = res.data;
            setFormData({...formData, title, description , image});
            console.log(title);
        })
            .catch(err => {
                toast.error(`Error To Your Information ${err.response.statusText}`);
                if (err.response.status === 401) {
                }
            });
    };

    const loadParticipation = () => {
        axios.get('http://localhost:5000/api/participation/')
            .then(res => {
                setPart(res.data);
                console.log(res.data);
            })
            .catch(err => {
            if (err.res.status === 401) {
                console.log(part);
            }
        });
    };

    const deleteMember = (idb) => {
        axios.delete(`http://localhost:5000/api/participation/delete/`+idb + '/' + part.participation[1]._id)
            .then(response => {
                console.log(response.data);
                window.location.reload();
            })
            .catch(e => {
                console.log(e);
            });
    };

    const handleMembers = newValue =>  {
        setPart({ ...part, learner : newValue });
        console.log(newValue);
    };

    const addMembers = e => {
        e.preventDefault();

        setPart({ ...part });
        axios.post('http://localhost:5000/api/participation/add', {
            training:id,
            learner: part.learner
    })
        .then(res => {
            setPart({
                ...part,
                training: id,
                learner: part.learner,
             });
                console.log(formData);
                window.location.reload(false);
        })
    .catch(err => {
        setPart({
            ...part,
            learner: part.learner,
        });
        console.log(err.response);
    });
};

    const {users} = members;

    const {title , description , file , image , previewImage, message, isError} = formData;

    /* here we try new draganddrop
    const eventLogger = (e: MouseEvent, data: DraggableData) => {
        console.log('Event: ', e);
        console.log('Data: ', data);
    };
    type DraggableData = {
        node: HTMLElement,
        // lastX + deltaX === x
        x: number, y: number,
        deltaX: number, deltaY: number,
        lastX: number, lastY: number
    };

    function startDrag(ev) {
        ev.dataTransfer.setData("drag-item", props.dataItem);
    }

    function dragOver(ev) {
        ev.preventDefault();
    }

    function drop(ev) {
        const droppedItem = ev.dataTransfer.getData("drag-item");
        if (droppedItem) {
            props.onDrop(droppedItem);
        }
    }

    */

    const fileSelectedHandler = e => {
        setFormData({ ...formData, file : e.target.files[0] ,
            previewImage: URL.createObjectURL(e.target.files[0]),
            progress: "LOADING...",
            message: ""
        });
        console.log(file);
    };

    const fileUploadHandler = () => {
        const fd = new FormData();
        fd.append('image',file,file.name);
        fd.append('_id',id);
        axios.post(`http://localhost:5000/api/training/upload`,fd).then(res => {
            console.log(res);
            window.location.reload(false);
        });
    };

    const handleShange = text => e => {
        setFormData({ ...formData, [text]: e.target.value });
    };
    const handleSubmit = e => {
        e.preventDefault();
        setFormData({ ...formData, textChange: 'Submitting' });
        axios
            .put(
                `http://localhost:5000/api/training/update/`+id,
                {
                    title: formData.title,
                    description: formData.description
                }
            )
            .then(res => {
                    setFormData({ ...formData });
                    props.history.push('/TrainingList');
                    console.log(formData);
            })
            .catch(err => {
                console.log(err.response);
            });
    };

    const deleteTraining = (id) => {
        axios.delete(`http://localhost:5000/api/training/delete/`+id)
            .then(response => {
                console.log(response.data);
                window.location.reload();
            })
            .catch(e => {
                console.log(e);
            });
    };

    const classes = useStyles();
    const theme = useTheme();
    const [value, setValue] = React.useState(0);
    const [spacing, setSpacing] = React.useState(2);
    const [progress, setProgress] = React.useState(10);
    const [show , setShow ] = React.useState(false);
    const [expanded, setExpanded] = React.useState(false);
    const [pdf , setPdf ] = React.useState(false);
    const [memberr , setMemberr] = React.useState(false);

    const handleChangee = (panel) => (event, isExpanded) => {
        setExpanded(isExpanded ? panel : false);
    };

    React.useEffect(() => {
        const timer = setInterval(() => {
            setProgress((prevProgress) => (prevProgress >= 100 ? 10 : prevProgress + 10));
        }, 800);
        return () => {
            clearInterval(timer);
        };
    }, []);

    const handleMemberr = () => {
        setMemberr(true);
    };

    const handleNotMemberr = () => {
        setMemberr(false);
    };

    const handleShow = () => {
        setShow(true);
    };

    const handleDontShow = () => {
        setShow(false);
    };

    const handlePdf = () => {
        setPdf(true);
    };

    const handleNotPdf = () => {
        setPdf(false);
    };

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    const handleChangeIndex = (index) => {
        setValue(index);
    };

    const transitionDuration = {
        enter: theme.transitions.duration.enteringScreen,
        exit: theme.transitions.duration.leavingScreen,
    };

    const fabs = [
        {
            color: 'primary',
            className: classes.fab,
            icon: <AddIcon />,
            label: 'Add',
        },
        {
            color: 'secondary',
            className: classes.fab,
            icon: <EditIcon />,
            label: 'Edit',
        },
        {
            color: 'inherit',
            className: clsx(classes.fab, classes.fabGreen),
            icon: <UpIcon />,
            label: 'Expand',
        },
    ];

    return (
        <div className={classes.root}>
            <AppBar position="static" color="default">
                <Tabs
                    value={value}
                    onChange={handleChange}
                    indicatorColor="primary"
                    textColor="primary"
                    variant="fullWidth"
                    aria-label="action tabs example"
                >
                    <Tab label="General" {...a11yProps(0)} />
                    <Tab label="Content" {...a11yProps(1)} />
                    <Tab label="Members" {...a11yProps(2)} />
                </Tabs>
            </AppBar>
            <SwipeableViews
                axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
                index={value}
                onChangeIndex={handleChangeIndex}
            >
                <TabPanel value={value} index={0} dir={theme.direction}>

                    <Grid container justify="left" className={classes.root} spacing={0}>
                        <Paper className={classes.paper} >
                    <form onSubmit={handleSubmit}>
                        <CustomInput
                            labelText={title}
                            id="title"
                            formControlProps={{
                                fullWidth: true,
                            }}
                            inputProps={{
                                onChange: handleShange('title')
                            }}
                        />
                        <CustomInput
                            labelText={description}
                            id="description"
                            formControlProps={{
                                fullWidth: true,
                            }}
                            inputProps={{
                                onChange: handleShange('description')
                            }}
                        />
                        <Grid  >
                        <ButtonGroup variant="contained" aria-label="contained primary button group" className={classes.iheb}>
                            <Button color="primary"  type='submit'>UPDATE</Button>
                            <Button onClick={() => history.goBack()}>CANCEL</Button>
                            <Button onClick={handlePdf} >EXPORT</Button>
                            <Dialog open={pdf} onClose={handleNotPdf} aria-labelledby="form-dialog-title">
                                <DialogTitle id="form-dialog-title">EXPORT PDF</DialogTitle>
                                <DialogContent>
                                    <div ref={ref} className={classes.ekhtafi}>
                                        <img style={{width: 200, height: 200, position: 'relative' , top : -10 , left : 10}} src={"/images/cef.png"} />
                                        <h1 style={{ position : 'relative' , top : -80 , right : 0 , fontFamily : 'serif' , fontVariant : 'small-caps' }} >{title}</h1>
                                        <img style={{width: 400, height: 200, position: 'relative' , top : -20 , left : 40}} src={ "/images/training/"+image } />
                                        <h2 style={{ position : 'relative' , top : -230 , left : 470 , fontFamily : 'Arial' }}>
                                            <h2 style={{ fontWeight : 600 }}> Description : </h2> {description}</h2>
                                        <h2 style={{ position : 'relative' , top : -40 , left : 40 , fontFamily : 'Arial' , fontWeight : 600 }} > The modules : </h2>
                                        {iheb.modules.map((row) => (
                                        <ul key={row._id}>
                                            <li style={{  fontFamily : 'Times New Roman' , position : 'relative' , top : -50 , left : 40 }}>{row.titleModule}</li>
                                        </ul>
                                        ))}
                                        <div style={{ position : 'relative' , top : 360 , left : 150 }}>
                                            <img style={{width: 30, height: 30}}  src={"/images/phone.png"} /> (+216) 97 841 899 - (+216) 23 143 733
                                            <img style={{width: 30, height: 30}}  src={"/images/mail.png"} /> contact@cef-conseil.com
                                        </div>
                                    </div>
                                    <Pdf targetRef={ref} filename={title+".pdf"} >
                                        {({ toPdf }) =>
                                            <Button variant="contained" color="primary" onClick={toPdf}>DOWNLOAD</Button>
                                        }
                                    </Pdf>
                                </DialogContent>
                                <DialogActions>
                                    <Button onClick={handleNotPdf} variant="contained" color="secondary">
                                        Cancel
                                    </Button>
                                </DialogActions>
                            </Dialog>
                        </ButtonGroup>
                            <Button variant="contained" color="secondary" className={classes.ahmed} onClick={deleteTraining}>DELETE TRAINING</Button>
                        </Grid>
                    </form>
                        </Paper>
                        <Paper className={classes.paper} >

                            <div className="mg20">
                                <label htmlFor="btn-upload">
                                    <input
                                        id="btn-upload"
                                        name="btn-upload"
                                        style={{ display: 'none' }}
                                        type="file"
                                        accept="image/*"
                                        onChange={fileSelectedHandler} />
                                    <Button
                                        className="btn-choose"
                                        variant="outlined"
                                        component="span" >
                                        Choose Image
                                    </Button>
                                </label>
                                <div className="file-name">
                                    {file ? file.name : null}
                                </div>
                                <Button
                                    className="btn-upload"
                                    color="primary"
                                    variant="contained"
                                    component="span"
                                    disabled={!file}
                                    onClick={fileUploadHandler}>
                                    Upload
                                </Button>
                                {file && (
                                    <div>
                                        <LinearProgressWithLabel value={progress} />
                                    </div>)
                                }
                                {previewImage ? (
                                    <div>
                                        <img width="400" height="400" className="preview my20" src={ previewImage } alt="" />
                                    </div>
                                ) :
                                    <div>
                                        <img width="400" height="400" className="preview my20" src={ "/images/training/"+image } alt="" />
                                    </div>}
                                {message && (
                                    <Typography variant="subtitle2" className={`upload-message ${isError ? "error" : ""}`}>
                                        {message}
                                    </Typography>
                                )}
                            </div>
                        </Paper>
                        <Tooltip
                            title="Add a Module"
                            placement={window.innerWidth > 959 ? "top" : "left"}
                        >
                            <Fab color="primary" aria-label="add" onClick={navigateToModule && handleShow}>
                                <AddIcon />
                            </Fab>
                        </Tooltip>
                        <Dialog open={show} onClose={handleDontShow} aria-labelledby="form-dialog-title">
                            <DialogTitle id="form-dialog-title">Add new module</DialogTitle>
                            <DialogContent>
                                <AddModule/>
                            </DialogContent>
                            <DialogActions>
                                <Button onClick={handleDontShow} variant="contained" color="secondary">
                                    Cancel
                                </Button>
                            </DialogActions>
                        </Dialog>
                    </Grid>
                </TabPanel>

                <TabPanel value={value} index={1} dir={theme.direction} >

                    <div className={classes.root}>
                        {iheb.modules.map((row) => (
                        <Accordion expanded={expanded === row._id} onChange={handleChangee(row._id)} key={row._id}>
                            <AccordionSummary
                                expandIcon={<ExpandMoreIcon />}
                                aria-controls="panel1bh-content"
                                id="panel1bh-header"
                            >
                                <Typography className={classes.heading}>{row.titleModule}</Typography>
                                <Typography className={classes.secondaryHeading}>I am a module</Typography>
                            </AccordionSummary>
                            <AccordionDetails>
                                <Typography>
                                    {row.training.title}
                                </Typography>
                            </AccordionDetails>
                        </Accordion>
                        ))}
                    </div>

                </TabPanel>
                <TabPanel value={value} index={2} dir={theme.direction}>
                    <div className={classes.root}>


                        <Autocomplete
                            multiple
                            id="checkboxes-tags-demo"
                            options={users}
                            disableCloseOnSelect
                            getOptionLabel={(option) => option.name}
                            renderOption={(option, { selected }) => (
                                <React.Fragment>
                                    <Checkbox
                                        icon={<Avatar  className={classes.avatar} src={"/images/"+option.image} />}
                                        checkedIcon={<Avatar  className={classes.avatar2} src={"/images/"+option.image} />}
                                        style={{ marginRight: 8 }}
                                        checked={selected}
                                    />
                                    {option.name}
                                </React.Fragment>
                            )}
                            getOptionSelected={(option, value) => option.name === value.name }
                            onChange={(event, newValue) => {
                                handleMembers(newValue);
                            }}
                            style={{ fullWidth : true }}
                            renderInput={(params) => (
                                <TextField {...params} variant="outlined" label="Add learners" placeholder="Members" />
                            )}
                        />
                        <Button variant="contained" color="primary" onClick={addMembers}>
                            Add Members
                        </Button>

                        {  part.participation.map((row) => (
                            row.training.title === title ?
                        <div className={classNames(classes.main, classes.mainRaised)} key={row._id}>
                            <h3> MEMBERS </h3>
                            { row.learner.map((e) =>
                            <div className={classes.container} >
                                <GridContainer justify="left" style={{ position : 'relative' , left : 50 , lineHeight : 4 }} key={e._id}>
                                    {<Avatar  className={classes.avatar} src={"/images/"+e.image} />}
                                    {e.name}
                                    <span>
                                                 <Tooltip
                                                     title="Delete Member"
                                                     placement={window.innerWidth > 959 ? "top" : "left"}
                                                 >
                                          <IconButton aria-label="Delete" style={{ position : 'absolute' , right : 80 }}  onClick={() => handleMemberr(e._id)} color="secondary">
                                            <DeleteIcon />
                                          </IconButton>
                                                 </Tooltip>
                                          <Dialog open={memberr} onClose={handleNotMemberr} fullWidth={true}  >
                                            <DialogTitle >{"Delete "+e.name}</DialogTitle>
                                            <DialogContent >
                                              <DialogContentText>
                                               Are you sure you want to remove {e.name}.
                                              </DialogContentText>
                                            </DialogContent>
                                            <DialogActions>
                                              <Button onClick={handleNotMemberr} color="primary">
                                                Cancel
                                              </Button>
                                              <Button onClick={() => deleteMember(e._id)} color="danger" autoFocus="autoFocus">
                                                Confirm
                                              </Button>
                                            </DialogActions>
                                          </Dialog>
                                        </span>
                                </GridContainer>
                            </div>
                            )}
                        </div>
                         : null )) }

{/*                        {  part.participation.map((row) => (
                            row.training.title === title ?
                            <Accordion expanded={expanded === row._id} onChange={handleChangee(row._id)} key={row._id}>
                                    <AccordionSummary
                                    expandIcon={<ExpandMoreIcon />}
                                    aria-controls="panel1bh-content"
                                    id="panel1bh-header"
                                >
                                        <Typography className={classes.secondaryHeading}>MEMBERS : </Typography>
                                </AccordionSummary>
                                { row.learner.map((e) =>
                                    <AccordionDetails>
                                        <Typography>
                                            {<Avatar  className={classes.avatar} src={"/images/"+e.image} />}
                                            {e.name}

                                            <span>
                                                 <Tooltip
                                                     title="Delete Member"
                                                     placement={window.innerWidth > 959 ? "top" : "left"}
                                                 >
                                          <IconButton aria-label="Delete"  onClick={handleMemberr} color="secondary">
                                            <DeleteIcon />
                                          </IconButton>
                                                 </Tooltip>
                                          <Dialog open={memberr} onClose={handleNotMemberr} fullWidth={true}>
                                            <DialogTitle>{"Delete "+e.name}</DialogTitle>
                                            <DialogContent>
                                              <DialogContentText>
                                               Are you sure you want to remove {e.name}.
                                              </DialogContentText>
                                            </DialogContent>
                                            <DialogActions>
                                              <Button onClick={handleNotMemberr} color="primary">
                                                Cancel
                                              </Button>
                                              <Button onClick={() => deleteMember(e._id)} color="danger" autoFocus="autoFocus">
                                                Confirm
                                              </Button>
                                            </DialogActions>
                                          </Dialog>
                                        </span>

                                        </Typography>
                                    </AccordionDetails>
                                )}
                            </Accordion>
                        : null )) }*/}

{/*                        <div className="container">
                            <main className="flexbox">
                            <Board id="board-1" className="board">
                                {members.users.map((m) => (
                                    <Card key={m._id} id={m._id} className="card" draggable="true">
                                        <Chip
                                            avatar={<Avatar  className={classes.avatar} src={"/images/"+m.image} />}
                                            label={m.name}
                                        />
                                    </Card>
                                ))}
                            </Board>
                            <Board id="board-2" className="board">
                                {  part.participation.map((row) => (
                                    row.training.title === title ?
                                    <div>
                                        { row.learner.map((e) =>
                                <Card id={e._id} key={e._id} className="card" draggable="true">
                                    <Chip
                                        avatar={<Avatar  className={classes.avatar} src={"/images/"+e.image} />}
                                        label={e.name}
                                    />
                                </Card>
                                  )}</div>
                                : null ))}
                            </Board>
                        </main>
                        </div>*/}

                    </div>

                </TabPanel>
            </SwipeableViews>
            {fabs.map((fab, index) => (
                <Zoom
                    key={fab.color}
                    in={value === index}
                    timeout={transitionDuration}
                    style={{
                        transitionDelay: `${value === index ? transitionDuration.exit : 0}ms`,
                    }}
                    unmountOnExit
                >
                    <Fab aria-label={fab.label} className={fab.className} color={fab.color} >
                        {fab.icon}
                    </Fab>
                </Zoom>
            ))}
        </div>
    );
};

export default withRouter(EditTraining);
