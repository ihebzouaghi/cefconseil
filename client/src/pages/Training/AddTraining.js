import React, {useState} from "react";
import axios from "axios";
import {toast} from "react-toastify";
import DialogContentText from "@material-ui/core/DialogContentText";
import CustomInput from "../../components/CustomInput/CustomInput";
import Button from "@material-ui/core/Button";
import DialogContent from "@material-ui/core/DialogContent/DialogContent";
import TextField from "@material-ui/core/TextField";
import EditTraining from "./EditTraining";

const AddTraining = () => {
    const [formData, setFormData] = useState({
        title: '',
        description: '',
        textChange: 'Add'
    });

    const { title, description,  textChange } = formData;

    const handleChange = text => e => {
        setFormData({ ...formData, [text]: e.target.value });
    };

    const handleSubmit = e => {
        e.preventDefault();
        if (title && description ) {
            setFormData({ ...formData, textChange: 'Submitting' });
            axios
                .post(`http://localhost:5000/api/training/add`, {
                    title,
                    description
                })
                .then(res => {
                    setFormData({
                        ...formData,
                        title: '',
                        description: '',
                        textChange: 'Submitted'
                    });
                    console.log(formData);
                })
                .catch(err => {
                    setFormData({
                        ...formData,
                        title: '',
                        description: '',
                        textChange: 'Training Added'
                    });
                    console.log(err.response);
                });

        } else {
            toast.error('Please fill all fields');
        }
    };

return (
    <DialogContent>
        <DialogContentText>
            Here you can add a new training
        </DialogContentText>
        <form onSubmit={handleSubmit}>
            <CustomInput
                labelText="Title..."
                id="title"
                formControlProps={{
                    fullWidth: true
                }}
                inputProps={{
                    onChange: handleChange('title'),
                    type: "text"
                }}
            />
            <TextField
                id="outlined-multiline-static"
                label="Description"
                multiline
                rows={5}
                variant="outlined"
                fullWidth={300}
                inputProps={onchange = handleChange('description')}
            />
            <Button variant="contained" type='submit' color="primary">ADD TRAINING
            </Button>
        </form>
    </DialogContent>

);
};

export default AddTraining;
