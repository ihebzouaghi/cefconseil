import React, { useState, useEffect} from "react";
import axios from "axios";

import { makeStyles } from "@material-ui/core/styles";

import GridItem from "./../../components/Grid/GridItem.js";
import GridContainer from "./../../components/Grid/GridContainer.js";
import Card from "./../../components/Card/Card.js";
import CardHeader from "./../../components/Card/CardHeader.js";
import CardBody from "./../../components/Card/CardBody.js";
import CardFooter from "./../../components/Card/CardFooter.js";
import AddIcon from '@material-ui/icons/Add';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import IconButton from "@material-ui/core/IconButton";

import styles from "./../../dashboard/jss/material-dashboard-react/views/dashboardStyle.js";
import Fab from "@material-ui/core/Fab";
import Tooltip from "@material-ui/core/Tooltip";
import Button from "../../components/CustomButtons/Button";
import Dialog from "@material-ui/core/Dialog/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent/DialogContent";
import DialogActions from "@material-ui/core/DialogActions/DialogActions";
import AddTraining from "./AddTraining";
import DialogContentText from "@material-ui/core/DialogContentText";
import CustomInput from "../../dashboard/CustomInput/CustomInput";
import Search from "@material-ui/icons/Search";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import Divider from "@material-ui/core/Divider";
import Typography from "@material-ui/core/Typography";
import Avatar from "@material-ui/core/Avatar";
import Accordion from "@material-ui/core/Accordion/Accordion";
import {whiteColor} from "../../dashboard/jss/material-dashboard-react";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from '@material-ui/core/MenuItem';
//const useStyles = makeStyles(styles);

const useStyles = makeStyles( muiBaseTheme => ({
    card: {
        maxWidth: 300,
        margin: 30,
        transition: "0.3s",
        boxShadow: "0 8px 40px -12px rgba(0,0,0,0.3)",
        "&:hover": {
            boxShadow: "0 16px 70px -12.125px rgba(0,0,0,0.5)"
        }
    },
    media: {
        paddingTop: "56.25%"
    },
    content: {
        textAlign: "left",
        padding: muiBaseTheme.spacing.unit * 3
    },
    divider: {
        margin: `${muiBaseTheme.spacing.unit * 3}px 0`
    },
    heading: {
        fontWeight: "bold"
    },
    subheading: {
        lineHeight: 1.8
    },
    avatar: {
        display: "inline-block",
        border: "2px solid white",
        "&:not(:first-of-type)": {
            marginLeft: -muiBaseTheme.spacing.unit
        }
    },
    searchWrapper: {
        [muiBaseTheme.breakpoints.down("sm")]: {
            width: "-webkit-fill-available",
            margin: "10px 15px 0"
        },
        display: "inline-block",
        position: 'absolute',
        top : 50,
        right : 100
    },
    search: {
        "& > div": {
            marginTop: "0"
        },
        [muiBaseTheme.breakpoints.down("sm")]: {
            margin: "10px 15px !important",
            float: "none !important",
            paddingTop: "1px",
            paddingBottom: "1px",
            padding: "0!important",
            width: "60%",
            marginTop: "40px",
            "& input": {
                color: whiteColor
            }
        }
    },
    margin: {
        zIndex: "4",
        margin: "0"
    },
    button : {
        position : 'absolute',
        right : 10 ,
        bottom : 0
    },
    button2 : {
        position : 'absolute',
        right : 50 ,
        bottom : 0
    },
    text : {
        fontFamily : 'sans-serif',
        textAlign : 'center'
    },
    formControl: {
        margin: muiBaseTheme.spacing(1),
        minWidth: 120,
    },
}));

const TrainingList = (props) => {

    const [formData,setData]= useState({
        trainings:[],
        data: [],
        title: '',
        description: '',
        sort: "asc"
    });

    useEffect(async () => {
        const result = await axios(
            'http://localhost:5000/api/training/',
        );
        setData(result.data);
        console.log(result.data);
        loadParticipation();
    }, []);

    const [part , setPart] = useState({
        participation : [],
        learner : [],
        training : {}
    });

    const loadParticipation = () => {
        axios.get('http://localhost:5000/api/participation/')
            .then(res => {
                setPart(res.data);
                console.log(res.data);
            })
            .catch(err => {
                if (err.res.status === 401) {
                    console.log(part);
                }
            });
    };


    const trainingDetails = (productID) => {
        axios.get(`http://localhost:5000/api/training/`+productID)
            .then(res => {
                console.log(res.data);
                props.history.push('/EditTraining/'+productID);
            })
            .catch(e => {
                console.log(e);
            });
    };

    const deleteTraining = (id) => {
        axios.delete(`http://localhost:5000/api/training/delete/`+id)
            .then(response => {
                console.log(response.data);
                window.location.reload();
            })
            .catch(e => {
                console.log(e);
            });
    };

    const [open, setOpen  ] = React.useState(false);
    const [show , setShow ] = React.useState(false);

    const [searchTerm, setSearchTerm] = React.useState("");
    const [searchResults, setSearchResults] = React.useState([]);
    const handleChange = e => {
        setSearchTerm(e.target.value);
    };

    const results = !searchTerm
        ? formData.trainings
        : formData.trainings.filter(title  =>
            title.title.toString().toLowerCase().includes(searchTerm.toLocaleLowerCase())
        );

    const toggleSort = () => {
        const newUsers = formData.trainings ;
        newUsers.sort((a, b) => a.title.toString().localeCompare(b.title));
        setData({ trainings: newUsers });
    };

    const [alpha, setAlpha] = React.useState('');

    const handleAlpha = (event) => {
        setAlpha(event.target.value);
    };

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const handleShow = () => {
        setShow(true);
    };

    const handleDontShow = () => {
        setShow(false);
    };

    const { ...rest } = props;
    const classes = useStyles();

    return(
        <GridContainer { ...rest }>
            <div className={classes.searchWrapper}>
                <CustomInput
                    formControlProps={{
                        className: classes.margin + " " + classes.search
                    }}
                    inputProps={{
                        onChange : handleChange,
                        placeholder: "Search",
                        inputProps: {
                            "aria-label": "Search"
                        }
                    }}
                />
                <Button color="white" aria-label="edit" justIcon round>
                    <Search />
                </Button>

                <FormControl className={classes.formControl} style={{top : -15}}>
                    <InputLabel id="demo-simple-select-label">Sorted by</InputLabel>
                    <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        value={alpha}
                        onChange={handleAlpha}
                    >
                        <MenuItem value="Alphabetically" onClick={() => toggleSort(results) }>Alphabetically</MenuItem>
                        <MenuItem value="Date">Date</MenuItem>
                    </Select>
                </FormControl>

            </div>

            {results.map((row) => (
            <Card className={classes.card}>
                <CardMedia
                    className={classes.media}
                    image={"/images/training/"+row.image}
                />
                <CardContent className={classes.content}>
                    <Typography
                        className={"MuiTypography--heading"}
                        variant={"h6"}
                        gutterBottom
                    >
                        {row.title}
                    </Typography>
                    <Typography
                        className={"MuiTypography--subheading"}
                        variant={"caption"}
                    >
                        {row.description}
                    </Typography>
                    <Divider className={classes.divider} light />
                    {  part.participation.map((p) => (
                        p.training.title === row.title ?
                            <div key={p._id} >
                            { p.learner.map((e) => (
                                <Tooltip
                                    title={e.name}
                                    placement={window.innerWidth > 959 ? "top" : "left"}
                                >
                        <Avatar className={classes.avatar}  src={"/images/"+e.image} />
                                </Tooltip>
                            ))} </div> : null
                    ))}
                    <Tooltip
                        title="Edit the Training"
                        placement={window.innerWidth > 959 ? "top" : "left"}
                    >
                        <Button color="transparent" className={classes.button2}  onClick={() => trainingDetails(row._id)}>
                            <EditIcon color="primary" />
                        </Button>
                    </Tooltip>
                    <span>
                                                <Tooltip
                                                    title="Delete Training"
                                                    placement={window.innerWidth > 959 ? "top" : "left"}
                                                >
      <IconButton aria-label="Delete"  className={classes.button}  onClick={handleShow} color="secondary">
        <DeleteIcon />
      </IconButton>
                                                </Tooltip>
      <Dialog open={show} onClose={handleDontShow} fullWidth={true}>
        <DialogTitle>{"Delete "+row.title}</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Confirm to delete your training {row.title}.
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleDontShow} color="primary">
            Cancel
          </Button>
          <Button onClick={() => deleteTraining(row._id)} color="danger" autoFocus="autoFocus">
            Confirm
          </Button>
        </DialogActions>
      </Dialog>
    </span>
                </CardContent>
            </Card>
            ))}
                <Tooltip
                    title="Add a new Training"
                    placement={window.innerWidth > 959 ? "top" : "left"}
                >
                    <Fab color="secondary" aria-label="add" onClick={AddTraining && handleClickOpen}>
                        <AddIcon />
                    </Fab>
                </Tooltip>
                <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title">Add new training</DialogTitle>
                    <DialogContent>
                        <AddTraining/>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={handleClose} color="primary">
                            Cancel
                        </Button>
                    </DialogActions>
                </Dialog>
        </GridContainer>
    );
};

export default TrainingList;
