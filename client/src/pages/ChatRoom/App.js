import { ChatEngine } from 'react-chat-engine';
//import ChatFeed from './ChatFeed';
//import LoginForm from './../Login';
import './App.css';
import React from "react";
import {isAuth} from "../../helpers/auth";

const projectID = 'd249f4a3-8c42-4ad3-b8d6-b3d0f3ed3702';



const App = () => {

    const userName = isAuth().name;

    return (
        isAuth()  ?
        <ChatEngine
            height="100vh"
            projectID={projectID}
            userName={userName}
            userSecret="25795193"
            //renderChatFeed={(chatAppProps) => <ChatFeed {...chatAppProps} />}
            onNewMessage={() => new Audio('https://chat-engine-assets.s3.amazonaws.com/click.mp3').play()}
        />
        : null
);
};


export default App;
