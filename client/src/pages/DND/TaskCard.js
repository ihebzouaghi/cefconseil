import { ItemTypes } from './utils/items';
import { useDrag } from 'react-dnd';
import React from "react";
import Box from "@material-ui/core/Box";
import Badge from "../../components/Badge/Badge";
import Text from "@material-ui/core/TextField";

const TaskCard = props => {
    const [{ isDragging }, drag] = useDrag({
        type : ItemTypes.CARD,
        item: {
            id: props._id,
        },
        collect: monitor => ({
            isDragging: !!monitor.isDragging(),
        }),
    });

    return (
        <Box
            ref={drag}
            my='4'
            p={3}
            bg='gray.500'
            opacity={isDragging ? '0.5' : '1'}
            boxShadow='sm'
            w='100%'
            rounded='md'
            color='white'
            display="flex"
            justifyContent="space-between"
        >
                <Text fontSize='lg' fontWeight='semibold'>
                    {props.title}
                </Text>
                <Badge
                    variantColor={props.category === 'Chores' ? 'green' : 'red'}
                    h='100%'>
                    {props.category}
                </Badge>
            <Text textAlign='center' fontSize='md'>
                {props.details}
            </Text>
        </Box>
    );
};

export default TaskCard;
