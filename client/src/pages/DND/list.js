import React, { createContext, useContext, useEffect, useState } from 'react';
import DragCard from './TaskCard';
import BoxTarget from './BoxTarget';
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import {HTML5Backend} from "react-dnd-html5-backend";
import {DndProvider} from "react-dnd";


export const CardContext = createContext({
    WIP: null,
    setWIP: null,
});

const List = (props) => {
    const [WIP, setWIP] = useState(true);

    return (

        <DndProvider backend={HTML5Backend}>
        <CardContext.Provider value={{ WIP, setWIP }}>
            <Grid
                gap={6}
                templateColumns='1fr 3fr'
                bg='gray.500'
                w='80vw'
                h='93vh'
                p={3}>
                <Box bg='gray.200' rounded='md' p={3} boxShadow='md'>
                    <Box spacing={3}>{WIP && <DragCard />}</Box>
                </Box>
                <Box bg='blue.200' rounded='md' p={3} boxShadow='md'>
                    <Box>
                        <BoxTarget>{!WIP && <DragCard />}</BoxTarget>
                    </Box>
                </Box>
            </Grid>
        </CardContext.Provider>
        </DndProvider>
    );
};

export default List;
