import { DndProvider } from 'react-dnd';
import {HTML5Backend} from 'react-dnd-html5-backend';
import React from "react";
import Flex from "react-calendar/dist/umd/Flex";

function jareb({ Component, pageProps }) {
    return (

                <DndProvider backend={HTML5Backend}>
                    <Flex direction='column' align='center' justify='center'>
                        <Flex justify='center' align='center' w='100%' h='93vh'>
                            <Component {...pageProps} />
                        </Flex>
                    </Flex>
                </DndProvider>
    );
}

export default jareb;
