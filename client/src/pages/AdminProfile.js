import React, {useEffect, useState} from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
// core components
import GridItem from "./../dashboard/Grid/GridItem.js";
import GridContainer from "./../dashboard/Grid/GridContainer.js";
import CustomInput from "./../dashboard/CustomInput/CustomInput.js";
import Button from "./../dashboard/CustomButtons/Button.js";
import Card from "./../dashboard/Card/Card.js";
import CardHeader from "./../dashboard/Card/CardHeader.js";
import CardAvatar from "./../dashboard/Card/CardAvatar.js";
import CardBody from "./../dashboard/Card/CardBody.js";
import CardFooter from "./../dashboard/Card/CardFooter.js";

import {getCookie, isAuth, updateUser} from "../helpers/auth";
import axios from "axios";
import {toast} from "react-toastify";

const styles = {
    cardCategoryWhite: {
        color: "rgba(255,255,255,.62)",
        margin: "0",
        fontSize: "14px",
        marginTop: "0",
        marginBottom: "0"
    },
    cardTitleWhite: {
        color: "#FFFFFF",
        marginTop: "0px",
        minHeight: "auto",
        fontWeight: "300",
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
        marginBottom: "3px",
        textDecoration: "none"
    }
};

const useStyles = makeStyles(styles);

export default function AdminProfile() {

    const [formData, setFormData] = useState({
        name: '',
        email: '',
        password1: '',
        businessName:'',
        phoneNumber: '',
        address: '',
        textChange: 'Update',
        role: ''
    });

    useEffect(() => {
        loadProfile();
    }, []);

    const loadProfile = () => {
        const token = getCookie('token');
        axios
            .get(`${process.env.REACT_APP_API_URL}/${isAuth()._id}`, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            })
            .then(res => {
                const { role, name, email , businessName , phoneNumber , address } = res.data;
                setFormData({ ...formData, role, name, email , businessName , phoneNumber , address});
            })
            .catch(err => {
                toast.error(`Error To Your Information ${err.response.statusText}`);
                if (err.response.status === 401) {
                    console.log(name);
                }
            });
    };
    const { name, email, password1, businessName, phoneNumber, address, textChange, role} = formData;
    const handleChange = text => e => {
        setFormData({ ...formData, [text]: e.target.value });
    };
    const handleSubmit = e => {
        const token = getCookie('token');
        console.log(token);
        e.preventDefault();
        setFormData({ ...formData, textChange: 'Submitting' });
        axios
            .put(
                `${process.env.REACT_APP_API_URL}/update`,
                {
                    name,
                    email,
                    password: password1,
                    businessName,
                    phoneNumber,
                    address
                },
                {
                    headers: {
                        Authorization: `Bearer ${token}`
                    }
                }
            )
            .then(res => {
                updateUser(res, () => {
                    toast.success('Profile Updated Successfully');
                    setFormData({ ...formData, textChange: 'Update' });
                });
            })
            .catch(err => {
                console.log(err.response);
            });
    };

    const classes = useStyles();
    return (
        <div>
            <GridContainer>
                <GridItem xs={12} sm={12} md={8}>
                    <form onSubmit={handleSubmit} >
                    <Card>
                        <CardHeader color="primary">
                            <h4 className={classes.cardTitleWhite}>Edit Profile</h4>
                            <p className={classes.cardCategoryWhite}>Complete your profile</p>
                        </CardHeader>
                        <CardBody>

                            <GridContainer>
                                <GridItem xs={12} sm={12} md={5}>
                                    <CustomInput
                                        labelText={role}
                                        id="role"
                                        inputProps={role}
                                        formControlProps={{
                                            fullWidth: true,
                                            disabled: true
                                        }}
                                    />
                                </GridItem>
                                <GridItem xs={12} sm={12} md={3}>
                                    <CustomInput
                                        labelText="Email : "
                                        id="email"
                                        formControlProps={{
                                            fullWidth: true,
                                        }}
                                        inputProps={{
                                            onChange: handleChange('email')
                                        }}
                                    />
                                </GridItem>
                                <GridItem xs={12} sm={12} md={4}>
                                    <CustomInput
                                        labelText="User Name :"
                                        id="name"
                                        formControlProps={{
                                            fullWidth: true,
                                        }}
                                        inputProps={{
                                            onChange: handleChange('name')
                                        }}
                                    />
                                </GridItem>
                            </GridContainer>
                            <GridContainer>
                                <GridItem xs={12} sm={12} md={6}>
                                    <CustomInput
                                        labelText="Business Name : "
                                        id="businessName"
                                        formControlProps={{
                                            fullWidth: true,
                                        }}
                                        inputProps={{
                                            onChange: handleChange('businessName')
                                        }}
                                    />
                                </GridItem>
                                <GridItem xs={12} sm={12} md={6}>
                                    <CustomInput
                                        labelText="Address : "
                                        id="address"
                                        formControlProps={{
                                            fullWidth: true,

                                        }}
                                        inputProps={{
                                            onChange: handleChange('address')
                                        }}
                                    />
                                </GridItem>
                            </GridContainer>
                            <GridContainer>
                                <GridItem xs={12} sm={12} md={4}>
                                    <CustomInput
                                        labelText="Phone Number :"
                                        id="phoneNumber"
                                        formControlProps={{
                                            fullWidth: true,
                                        }}
                                        inputProps={{
                                            onChange: handleChange('phoneNumber')
                                        }}
                                    />
                                </GridItem>
                                <GridItem xs={12} sm={12} md={4}>
                                    <CustomInput
                                        labelText="password"
                                        id="password1"
                                        formControlProps={{
                                            fullWidth: true,
                                        }}
                                        inputProps={{
                                            onChange: handleChange('password1'),
                                            type: "password"
                                        }}
                                    />
                                </GridItem>
                            </GridContainer>
                        </CardBody>
                        <CardFooter>
                            <Button type="submit" color="primary">{textChange}</Button>
                        </CardFooter>
                    </Card>
                </form>
                </GridItem>
                <GridItem xs={12} sm={12} md={4}>
                    <Card profile>
                        <CardAvatar profile>
                            <img
                                alt="..."
                                src={"/images/"+isAuth().image} />
                        </CardAvatar>
                        <CardBody profile>
                            <h2 className={classes.cardTitle}>Business Name : {businessName}</h2>
                            <h3 className={classes.cardCategory}>User Name : {name}</h3>
                            <h4 className={classes.cardTitle}>Email : {email}</h4>
                            <p className={classes.description}>Address : {address}</p>
                            <h>{isAuth().image}</h>
                        </CardBody>
                    </Card>
                </GridItem>
            </GridContainer>
        </div>
    );
}
