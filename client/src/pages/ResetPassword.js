import React, { useState, useEffect } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import axios from 'axios';
import Dialog from "@material-ui/core/Dialog/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent/DialogContent";
import DialogActions from "@material-ui/core/DialogActions/DialogActions";
import Button from "../components/CustomButtons/Button";
import CustomInput from "../components/CustomInput/CustomInput";
import InputAdornment from "@material-ui/core/InputAdornment";
import LockIcon from "@material-ui/core/SvgIcon/SvgIcon";
import {makeStyles} from "@material-ui/core";
import styles from "../assests/jss/material-kit-react/views/loginPage";
import image from "../assests/img/bg7.jpg";

const useStyles = makeStyles(styles);

const ResetPassword = ({match}) => {
    const [formData, setFormData] = useState({
        password1: '',
        password2: '',
        token: '',
        textChange: 'Submit'
    });

    const [open, setOpen] = React.useState(true);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const { password1, password2, textChange, token } = formData;

    useEffect(() => {
        let token = match.params.token
        if(token) {
            setFormData({...formData, token,})
        }

    }, [])
    const handleChange = text => e => {
        setFormData({ ...formData, [text]: e.target.value });
    };
    const handleSubmit = e => {
        console.log(password1, password2)
        e.preventDefault();
        if ((password1 === password2) && password1 && password2) {
            setFormData({ ...formData, textChange: 'Submitting' });
            axios
                .put(`${process.env.REACT_APP_API_URL}/resetpassword`, {
                    newPassword: password1,
                    resetPasswordLink: token
                })
                .then(res => {
                    console.log(res.data.message)
                    setFormData({
                        ...formData,
                        password1: '',
                        password2: ''
                    });
                    toast.success(res.data.message);

                })
                .catch(err => {
                    toast.error('Something is wrong try again');
                });
        } else {
            toast.error('Passwords don\'t matches');
        }
    };
    const classes = useStyles();
    return (
        <div
            className={classes.pageHeader}
            style={{
                backgroundImage: "url(" + image + ")",
                backgroundSize: "cover",
                backgroundPosition: "top center"
            }}
        >
            <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                <form onSubmit={handleSubmit}>
                <DialogTitle id="form-dialog-title">Reset Your Password</DialogTitle>
                <DialogContent>
                    <CustomInput
                        labelText="Password"
                        id="password1"
                        formControlProps={{
                            fullWidth: true
                        }}
                        inputProps={{
                            onChange: handleChange('password1'),
                            type: "password",
                            endAdornment: (
                                <InputAdornment position="end">
                                    <LockIcon className={classes.inputIconsColor}>
                                    </LockIcon>
                                </InputAdornment>
                            ),
                            autoComplete: "off"
                        }}
                    />
                    <CustomInput
                        labelText="Password"
                        id="password2"
                        formControlProps={{
                            fullWidth: true
                        }}
                        inputProps={{
                            onChange: handleChange('password2'),
                            type: "password",
                            endAdornment: (
                                <InputAdornment position="end">
                                    <LockIcon className={classes.inputIconsColor}>
                                    </LockIcon>
                                </InputAdornment>
                            ),
                            autoComplete: "off"
                        }}
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        Cancel
                    </Button>
                    <Button type="submit" color="primary">
                        Confirm
                    </Button>
                </DialogActions>
            </form>
            </Dialog>
        </div>
    );
};

export default ResetPassword;
