import React, { useState } from 'react';
import Calendar from 'react-calendar';
import 'react-calendar/dist/Calendar.css';
import { differenceInCalendarDays } from 'date-fns';

function isSameDay(a, b) {
    return differenceInCalendarDays(a, b) === 0;
}

const datesToAddContentTo = [new Date()];

function tileContent({ date, view }) {
    // Add class to tiles in month view only
    if (view === 'month') {
        // Check if a date React-Calendar wants to check is on the list of dates to add class to
        if (datesToAddContentTo.find(dDate => isSameDay(dDate, date))) {
            return 'Iheb';
        }
    }
}

const MyCalendar = (props) =>{
    const [value, setValue] = useState(new Date());

    function onChange(nextValue) {
        setValue(nextValue);
    }

    const dd = () => {
        alert(datesToAddContentTo);
    };

    return (
        <Calendar
            onChange={onChange}
            value={value}
            tileContent={tileContent}
            onClickDay={dd}
        />
    );
};
export default MyCalendar;
