import React, { useState } from 'react';
import { toast } from 'react-toastify';
import axios from 'axios';
import { makeStyles } from "@material-ui/core/styles";

// @material-ui/icons

// core components

import styles from "./../assests/jss/material-kit-react/views/componentsSections/tabsStyle.js";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogContent from "@material-ui/core/DialogContent/DialogContent";
import CustomInput from "../components/CustomInput/CustomInput";
import InputAdornment from "@material-ui/core/InputAdornment";
import Email from "@material-ui/core/SvgIcon/SvgIcon";
import Button from "@material-ui/core/Button";

const useStyles = makeStyles(styles);

const ForgetPassword = ({history}) => {
    const [formData, setFormData] = useState({
        email: '',
        textChange: 'Submit'
    });
    const classes = useStyles();
    const { email, textChange } = formData;
    const handleChange = text => e => {
        setFormData({ ...formData, [text]: e.target.value });
    };
    const handleSubmit = e => {
        e.preventDefault();
        if (email) {
            setFormData({ ...formData, textChange: 'Submitting' });
            axios
                .put(`http://localhost:5000/api/user/forgotpassword`, {
                    email
                })
                .then(res => {

                    setFormData({
                        ...formData,
                        email: '',
                    });

                    toast.success(`Please check your email`);

                })
                .catch(err => {
                    console.log(err.response);
                    toast.error(err.response.data.error);
                });
        } else {
            toast.error('Please fill all fields');
        }
    };
    return (
        <DialogContent>
        <DialogContentText>
            To change your password, please enter your email address here. We will send you a reset link.
        </DialogContentText>
                <form onSubmit={handleSubmit}>
                    <CustomInput
                        labelText="Email..."
                        id="email"
                        formControlProps={{
                            fullWidth: true
                        }}
                        inputProps={{
                            onChange: handleChange('email'),
                            type: "email",
                            endAdornment: (
                            <InputAdornment position="end">
                            <Email className={classes.inputIconsColor} />
                            </InputAdornment>
                        )
                        }}
                    />
                    <Button type='submit' color="primary">
                        Send
                    </Button>
                </form>
        </DialogContent>
    );
};

export default ForgetPassword;
