import React, {useEffect, useRef, useState} from 'react';
import { makeStyles,useTheme  } from '@material-ui/core/styles';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import AddIcon from '@material-ui/icons/Add';
import Fab from '@material-ui/core/Fab';
import PropTypes from 'prop-types';
import SwipeableViews from 'react-swipeable-views';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Box from '@material-ui/core/Box';
import '../UploadAndDownload/styles.scss';
import axios from "axios";
import AddStep from "./AddStep";
import Dialog from "@material-ui/core/Dialog/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent/DialogContent";
import DialogActions from "@material-ui/core/DialogActions/DialogActions";
import Button from "../../components/CustomButtons/Button";
import DeleteIcon from "@material-ui/icons/Delete";
import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import GridItem from "./../../components/Grid/GridItem.js";
import GridContainer from "./../../components/Grid/GridContainer.js";
import Card from "./../../components/Card/Card.js";
import CardActionArea from '@material-ui/core/CardActionArea';
import { useConfirm } from 'material-ui-confirm';
//File import
import {API_URL} from "../UploadAndDownload/utils/constants";
import img from "../../dashboard/img/Upload.png";
import {Col, Form, Row} from "react-bootstrap";
import Dropzone from "react-dropzone";
import TextField from '@material-ui/core/TextField';






/********************************       Panel Initialization       ****************************************************/
function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <Typography
            component="div"
            role="tabpanel"
            hidden={value !== index}
            id={`action-tabpanel-${index}`}
            aria-labelledby={`action-tab-${index}`}
            {...other}
        >
            {value === index && <Box p={3}>{children}</Box>}
        </Typography>
    );
}
TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};
function a11yProps(index) {
    return {
        id: `full-width-tab-${index}`,
        'aria-controls': `full-width-tabpanel-${index}`,
    };
}
const useStyles = makeStyles((theme) => ({
    headingStep: {
        fontSize: theme.typography.pxToRem(15),
        fontWeight: theme.typography.fontWeightRegular,
    },
    root: {
        width: '100%',
    },
    heading: {
        fontSize: theme.typography.pxToRem(20),
        flexBasis: '200px',
        flexShrink: 0,
    },
    secondaryHeading: {
        fontSize: theme.typography.pxToRem(15),
        color: theme.palette.text.secondary,
        flexBasis: '1100px',
    },
    fab: {
        position: 'absolute',
        bottom: theme.spacing(2),
        right: theme.spacing(2),
    },
    rot: {
        maxWidth: 345,
        minWidth:345,
        maxHeight:200,
        minHeight:200,
        "& .hidden-button": {
            display: "none",

        },
        "&:hover .hidden-button": {
            display: 'flex',
            position: "absolute",

        }
    },
    embed: {
        maxWidth: 345,
        minWidth:345,
        maxHeight:200,
        minHeight:200,
        borderRadius:5,

    },
    tabSimple: {
        display: 'none'
    },
    tabSimpleActive: {
        display: 'block'
    },
    tabQuestion:{
        display:'none'
    },
    tabQuestionActive:{
        display:'block'
    },

}));
TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};
/**********************************        Steplist2 function         *************************************************/
const StepList2 = props => {
    const [formData,setData]= useState({
        steps:[],
        data:[],
        files:[],
        title:'',
        type: '',
        textSimple: '',
        externalPat: '',
        textChange: 'Add',
        last:''
    });
    const [formDataF,setDataF]= useState(
        {
            files:[],
            data:[]
        }
    );
    const Navigate = () => {
      props.history.push("/AddStep");
    };

    /**********************************      get step      ************************************************************/

    useEffect(async () => {
        const result = await axios(
            'http://localhost:5000/api/step/get',
        );
        setData(result.data);
    }, []);

    /**********************************      delete step       ********************************************************/
    const confirm = useConfirm();

    const handleDelete = (item,name) => {
          confirm({ description: `This will permanently delete ${name}.` })
            .then(() => axios.delete(`http://localhost:5000/api/step/delete/`+item).then(response => {
                console.log(response.data);
                window.location.reload();
            }))
            .catch(() => console.log("Deletion cancelled."));
    };

    /**********************************      delete File       ********************************************************/
    const deleteFile = (id) => {
        axios.delete(`http://localhost:5000/api/file/delete/`+id)
            .then(response => {
                console.log(response.data);
                window.location.reload();
            })
            .catch(e => {
                console.log(e);
            });
    };

    /***********************************      Add file      ***********************************************************/

    const [file, setFile] = useState(null); // state for storing actual image
    const [previewSrc, setPreviewSrc] = useState(''); // state for storing previewImage
    const [state, setState] = useState({
        title: '',
        description: ''
    });
    const [errorMsg, setErrorMsg] = useState('');
    const [isPreviewAvailable, setIsPreviewAvailable] = useState(false); // state to show preview only for images
    const dropRef = useRef(); // React ref for managing the hover state of droppable area
    const pRef = useRef();
    const previewRef = useRef();

    const handleInputChange = (event) => {
        setState({
            ...state,
            [event.target.name]: event.target.value
        });
    };
    const onDrop = (files) => {
        const [uploadedFile] = files;
        setFile(uploadedFile);

        const fileReader = new FileReader();
        fileReader.onload = () => {
            setPreviewSrc(fileReader.result);

        };
        fileReader.readAsDataURL(uploadedFile);
        setIsPreviewAvailable(uploadedFile.name.match(/\.(jpeg|jpg|png|pdf|doc|docx|xlsx|xls|PNG|txt|mp3|mp4)$/));
        dropRef.current.style.border = '4px dashed #e9ebeb';
        dropRef.current.style.backgroundImage= `` ;
    };

    const updateBorder = (dragState) => {
        if (dragState === 'over') {
            dropRef.current.style.border = '2px dashed #C0C0C0';
            pRef.current.style.color = ' ';
            pRef.current.style.fontSize = ' 0em';
            dropRef.current.style.textContent = "DRAG";
            dropRef.current.style.backgroundImage= `url(${img})` ;
            dropRef.current.style.backgroundPosition= 'center';
            dropRef.current.style.backgroundSize= '150px 150px';
            dropRef.current.style.backgroundRepeat= 'no-repeat';


        } else if (dragState === 'leave') {
            dropRef.current.style.border = '4px dashed #e9ebeb';
            pRef.current.style.color = '';
            pRef.current.style.fontSize = '';
            dropRef.current.style.backgroundImage= `` ;

        }
    };

    const handleOnSubmit = async (event) => {
        event.preventDefault();

        try {
            const {title, description } = state;
             const step=expanded;
            if (title.trim() !== '' && description.trim() !== '') {
                if (file) {
                    const formDataFile = new FormData();
                    formDataFile.append('file', file);
                    formDataFile.append('title', title);
                    formDataFile.append('description', description);
                    formDataFile.append('step', step);

                    setErrorMsg('');
                    await axios.post(`${API_URL}/upload`, formDataFile, {
                        headers: {
                            'Content-Type': 'multipart/form-data'
                        }
                    });
                    window.location.reload();
                } else {
                    setErrorMsg('Please select a file to add.');
                }
            } else {
                setErrorMsg('Please enter all the field values.');
            }
        } catch (error) {
            error.response && setErrorMsg(error.response.data);
        }
    };

    /*******************************           get file           *****************************************************/

    useEffect(async () => {
        const result = await axios(
            'http://localhost:5000/api/file/get',
        );
        setDataF(result.data);
    }, []);

    /******************************           dialog state         ****************************************************/

    const [show , setShow ] = React.useState(false);
    const handleShow = (id) => {
        setShow(true,id);
    };

    const handleDontShow = () => {
        setShow(false);
    };
    const [open, setOpen  ] = React.useState(false);
    const handleClickOpen = () => {
        setOpen(true);
    };
    const handleClose = () => {
        setOpen(false);
    };
    /**********************************       File Dialog        ******************************************************/

    const [openF, setOpenF  ] = React.useState(false);
    const handleClickOpenF = () => {
        setOpenF(true);
    };
    const handleCloseF = () => {
        setOpenF(false);
    };
    /************************************      Step formData       ****************************************************/

    const theme = useTheme();
    const [value, setValue] = React.useState(0);
    const { ...rest } = props;
    const classes = useStyles();
    const handleShange = text => e => {
        setData({ ...formData, [text]: e.target.value });
    };

    /*********************************       Step add & update        *************************************************/
    const handleSubmit = (id) => e => {
        e.preventDefault();
        setData({ ...formData, textChange: 'Submitting' });
        axios
            .put(
                `http://localhost:5000/api/step/update/`+id,
                {
                    title: formData.title,
                    type: formData.type,
                    textSimple: formData.textSimple,
                    externalPath: formData.externalPath,
                    textChange: 'Submitted'
                }
            )
            .then(res => {
                setData({ ...formData });
                console.log(formData);
            })
            .catch(err => {
                console.log(err.response);
            });
    };
    /************************************       Accordion State        ************************************************/

    const [expanded, setExpanded] = React.useState(false);

    const handleChange = (panel) => (event, isExpanded) => {
        setExpanded(isExpanded ? panel : false);
    };
    const handleChangeN = (event, newValue) => {
        setValue(newValue);
    };
    const handleChangeNIndex = (index) => {
        setValue(index);
    };
    /************************************       Accordion State        ************************************************/
    const [AccordionOpen, setAccordionOpen] = React.useState(false);
    const handleChangeAccordion = (tab) => (event, isAccordionOpen) => {
        setAccordionOpen(isAccordionOpen ? tab : false);
    };
    /************************************       Get Question        ***************************************************/

    const [formDataQ,setDataQ]= useState(
        {
            questions:[],
            data:[]
        }
    );
    useEffect(async () => {
        const result = await axios(
            'http://localhost:5000/api/question/get',
        );
        setDataQ(result.data);
    }, []);




    /************************************       Add Quizz        ******************************************************/
    const AddQuizz = (StepId) => {

                props.history.push(`/admin/AddQuestion/`+StepId);
    };


    return (
        <div className={classes.root}>
                    <div className={classes.root}>
                        {formData.steps.map((row) => (

                        <Accordion key={row._id} expanded={expanded === row._id} onChange={handleChange(row._id)}>

                            <AccordionSummary
                                expandIcon={<ExpandMoreIcon />}
                                aria-controls="panel1a-content"
                                id="panel1bh-header"
                            >
                                <Typography className={classes.heading} >{row.title}</Typography>
                                <Typography className={classes.secondaryHeading}>{row.type}</Typography>
                                                            <span>
                                                                            <Tooltip
                                                                                title="Delete Step"
                                                                                placement={window.innerWidth > 959 ? "top" : "right"}
                                                                            >
                                  <IconButton aria-label="Delete" onClick={() => handleDelete(row._id,row.title)} color="default">
                                    <DeleteIcon />
                                  </IconButton>
                                                                            </Tooltip>

                                </span>
                            </AccordionSummary>
                            <AccordionDetails>

        {/******************************************         Step details        **********************************************/}

                                <div className={classes.root}>
                                    <AppBar position="static" color="default" >

                                        <Tabs
                                            value={value}
                                            onChange={handleChangeN}
                                            indicatorColor="primary"
                                            textColor="primary"
                                            variant="fullWidth"
                                            aria-label="full width tabs example"
                                        >
                                            <Tab className={ row.type==='Hybrid' || row.type==='Lesson'? classes.tabSimpleActive :classes.tabSimple } label="LESSON"  {...a11yProps(0)}  />
                                            <Tab className={ row.type==='Hybrid' || row.type==='Quiz'? classes.tabQuestionActive :classes.tabQuestion} label="QUIZ" {...a11yProps(1)}  />
                                        </Tabs>
                                    </AppBar>
                                    <SwipeableViews
                                        axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
                                        index={row.type === 'Hybrid'? value : (row.type === 'Lesson'?0 :1)}
                                        onChangeIndex={handleChangeNIndex}
                                    >
                                        <TabPanel value={value} index={0} dir={theme.direction}>
                                            <div className={classes.root}>
                                                <form onSubmit={handleSubmit}>
                                                <Accordion >
                                                    <AccordionSummary
                                                        expandIcon={<ExpandMoreIcon />}
                                                        aria-controls="panel1a-content"
                                                        id="panel1a-header"
                                                    >
                                                        <Typography className={classes.headingStep} >Simple Text</Typography>
                                                    </AccordionSummary>
                                                    <AccordionDetails>
                                                        <TextField
                                                            id="textSimple"
                                                            multiline
                                                            fullWidth
                                                            defaultValue={row.textSimple}
                                                            inputProps={{
                                                                onChange: handleShange('textSimple')
                                                            }}
                                                        />
                                                    </AccordionDetails>
                                                </Accordion>
                                                <Accordion>
                                                    <AccordionSummary
                                                        expandIcon={<ExpandMoreIcon />}
                                                        aria-controls="panel2a-content"
                                                        id="panel2a-header"
                                                    >
                                                        <Typography className={classes.headingStep}>
                                                            File
                                                        </Typography>
                                                    </AccordionSummary>
                                                    <AccordionDetails >


                          {/******************************************************    File Upload    ********************************************/}

                                                        <Dialog open={openF} onClose={handleCloseF} aria-labelledby="form-dialog-title" >
                                                            <DialogTitle id="form-dialog-title" >New File</DialogTitle>

                                                            <DialogContent>
                                                                <React.Fragment>
                                                                    <Form className="search-form" onSubmit={handleOnSubmit}>
                                                                        {errorMsg && <p className="errorMsg">{errorMsg}</p>}
                                                                        <Row>
                                                                            <Col>
                                                                                <Form.Group controlId="title">
                                                                                    <Form.Control
                                                                                        type="text"
                                                                                        name="title"
                                                                                        value={state.title || ''}
                                                                                        placeholder="Enter title"
                                                                                        onChange={handleInputChange}
                                                                                    />
                                                                                </Form.Group>
                                                                            </Col>
                                                                        </Row>
                                                                        <Row>
                                                                            <Col>
                                                                                <Form.Group controlId="description">
                                                                                    <Form.Control
                                                                                        type="text"
                                                                                        name="description"
                                                                                        value={state.description || ''}
                                                                                        placeholder="Enter description"
                                                                                        onChange={handleInputChange}
                                                                                    />
                                                                                </Form.Group>
                                                                            </Col>
                                                                        </Row>

                                                                        <div className="upload-section">
                                                                            <Dropzone onDrop={onDrop}

                                                                                      onDragEnter={() => updateBorder('over')}
                                                                                      onDragLeave={() => updateBorder('leave')}
                                                                            >

                                                                                {({ getRootProps, getInputProps }) => (
                                                                                    <div {...getRootProps({ className: 'drop-zone' })} ref={dropRef} >
                                                                                        <input {...getInputProps()} />
                                                                                        <p ref={pRef}>Drag and drop a file OR click here to select a file</p>
                                                                                        {file && (
                                                                                            <div>
                                                                                                <strong>Selected file:</strong> {file.name}
                                                                                            </div>
                                                                                        )}
                                                                                    </div>
                                                                                )}
                                                                            </Dropzone>


                                                                            {previewSrc ? (
                                                                                isPreviewAvailable ? (

                                                                                    <div className="image-preview" ref={previewRef} >
                                                                                        <embed className="preview-image" src={previewSrc} alt="Preview" />
                                                                                    </div>
                                                                                ) : (
                                                                                    <div className="preview-message">
                                                                                        <p>No preview available for this file</p>
                                                                                    </div>
                                                                                )
                                                                            ) : (
                                                                                <div className="preview-message">
                                                                                    <p>Image preview will be shown here after selection</p>
                                                                                </div>
                                                                            )}

                                                                        </div>
                                                                        <Button variant="primary" type="submit" color="primary">
                                                                            Submit
                                                                        </Button>
                                                                    </Form>
                                                                </React.Fragment>
                                                            </DialogContent>
                                                        </Dialog>



                        {/******************************************        File Visualisation      ******************************************/}

                                                          <div className={classes.root}>
                                                              <GridContainer { ...rest }>
                                                                  {formDataF.files.map((fow) => row._id === fow.step._id && String( row._id) !== '0' && String(fow.step._id) !== '0' ?
                                                                      (
                                                                          <GridItem xs={12} sm={12} md={3} key={fow._id}>
                                                                              <Card className={classes.rot}  >
                                                                                  <CardActionArea className={classes.rot} >
                                                                                      <Tooltip
                                                                                          title="Delete File"
                                                                                          placement={window.innerWidth > 959 ? "top" : "right"}
                                                                                      >
                                                                                      <IconButton size="small"
                                                                                                  className="hidden-button"
                                                                                                  variant="contained"
                                                                                                  color="secondary"
                                                                                                  onClick={() => deleteFile(fow._id)}
                                                                                      >
                                                                                          <DeleteIcon  />
                                                                                      </IconButton>
                                                                                      </Tooltip>
                                                                                      <embed
                                                                                          component="img"
                                                                                          alt="..."
                                                                                          className={classes.embed}
                                                                                          src={"/images/files/"+(String(fow.file_path).slice(27))}

                                                                                     />
                                                                                  </CardActionArea>
                                                                              </Card>
                                                                          </GridItem>
                                                                  ):null)}



                                                              </GridContainer>
                                                              <Button variant="contained"
                                                                       color="default"
                                                                       className={classes.button}
                                                                       startIcon={<CloudUploadIcon />}
                                                                       onClick={ ()=>handleClickOpenF(expanded) }>
                                                                  Upload
                                                              </Button>
                                                        </div>
             {/**************************************     File upload and download      ********************************************/}

                                                    </AccordionDetails>
                                                </Accordion>
                                                <Accordion >
                                                    <AccordionSummary
                                                        expandIcon={<ExpandMoreIcon />}
                                                        aria-controls="panel3a-content"
                                                        id="panel3a-header"
                                                    >
                                                        <Typography className={classes.headingStep}>External path</Typography>
                                                    </AccordionSummary>
                                                    <AccordionDetails>
                                                        <TextField
                                                            id="externalPath"
                                                            multiline
                                                            fullWidth
                                                            defaultValue={row.externalPath}
                                                            inputProps={{
                                                                onChange: handleShange('externalPath')
                                                            }}
                                                        />
                                                    </AccordionDetails>
                                                </Accordion>
                                                    <Button type='submit' onClick={handleSubmit(row._id)}>UPDATE</Button>
                                                </form>
                                            </div>
                                        </TabPanel>
                                        <TabPanel value={value} index={1} dir={theme.direction}>

                                            {formDataQ.questions.map((Qow) => Qow.step._id === row._id ?
                                                ( <p   key={Qow._id}> {Qow.name}

                                                </p>
                                                ) :null
                                            )}



                                            <Fab color="primary" aria-label="add"   >
                                                <AddIcon  onClick={() => AddQuizz(row._id)} />
                                            </Fab>
                                        </TabPanel>
                                    </SwipeableViews>
                                </div>
                            </AccordionDetails>
                        </Accordion>

                            ))}
                        <br></br>
                    </div>
   {/******************************************    Step action     *******************************************************/}
            <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                        <DialogTitle id="form-dialog-title">Add New Step</DialogTitle>
                        <DialogContent>
                            <AddStep/>
                        </DialogContent>
            </Dialog>
            <Tooltip
                title="New Step"
                placement={window.innerWidth > 959 ? "top" : "right"}
            >
            <Fab color="secondary" aria-label="add"   onClick={ Navigate && handleClickOpen}>
                <AddIcon />
                </Fab>
            </Tooltip>
        </div>
    );
};
export default StepList2 ;

