import React, {useEffect, useState} from 'react';
import { makeStyles,useTheme  } from '@material-ui/core/styles';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import Typography from '@material-ui/core/Typography';
import AddIcon from '@material-ui/icons/Add';
import Fab from '@material-ui/core/Fab';
import App from '../UploadAndDownload/components/App.js';
import CustomInput from "../../components/CustomInput/CustomInput";
import PropTypes from 'prop-types';
import SwipeableViews from 'react-swipeable-views';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Box from '@material-ui/core/Box';
import TextField from "@material-ui/core/TextField";
import '../UploadAndDownload/styles.scss';
import 'bootstrap/dist/css/bootstrap.min.css';
import {useHistory, useParams} from "react-router-dom";
import axios from "axios";
import {toast} from "react-toastify";






function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`full-width-tabpanel-${index}`}
            aria-labelledby={`full-width-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box p={3}>
                    <Typography>{children}</Typography>
                </Box>
            )}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};

function a11yProps(index) {
    return {
        id: `full-width-tab-${index}`,
        'aria-controls': `full-width-tabpanel-${index}`,
    };
}

const useStyles = makeStyles((theme) => ({
    root: {
        backgroundColor: theme.palette.background.paper,
       // width: 500,
        width: '100%',
    },
    heading: {
        fontSize: theme.typography.pxToRem(15),
        fontWeight: theme.typography.fontWeightRegular,
    },

}));

export default function FullWidthTabs(props) {

    const classes = useStyles();
    const theme = useTheme();
    const [value, setValue] = React.useState(0);

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    const handleChangeIndex = (index) => {
        setValue(index);
    };



    //edit Step
    let { id } = useParams();
    const history = useHistory();

    const [formData, setFormData] = useState({
        title: '',
        description: '',
        textChange: 'Update',
        file : null
    });

    useEffect(() => {
        loadStep();
    }, []);

    const loadStep = () => {
        axios
            .get(`http://localhost:5000/api/step/`+id)
            .then(res => {
                const {title, type ,textSimple,externalPath, image} = res.data;
                setFormData({...formData, title, type , image});
                console.log(title);
            })
            .catch(err => {
                toast.error(`Error To Your Information ${err.response.statusText}`);
                if (err.response.status === 401) {
                }
            });
    };



    return (
        <div className={classes.root}>
            <AppBar position="static" color="default">
                <Tabs
                    value={value}
                    onChange={handleChange}
                    indicatorColor="primary"
                    textColor="primary"
                    variant="fullWidth"
                    aria-label="full width tabs example"
                >
                    <Tab label="Data" {...a11yProps(0)} />
                    <Tab label="Question" {...a11yProps(1)} />

                </Tabs>
            </AppBar>
            <SwipeableViews
                axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
                index={value}
                onChangeIndex={handleChangeIndex}
            >
                <TabPanel value={value} index={0} dir={theme.direction}>
                    <div className={classes.root}>
                        <Accordion>
                            <AccordionSummary
                                expandIcon={<Fab color="primary" aria-label="add">
                                    <AddIcon />
                                </Fab>}
                                aria-controls="panel1a-content"
                                id="panel1a-header"
                            >
                                <Typography className={classes.heading}>Simple Text</Typography>
                            </AccordionSummary>
                            <AccordionDetails>

                                    <TextField
                                        id="outlined-multiline-static"
                                        label="Your text"
                                        multiline
                                        rows={5}
                                        variant="outlined"
                                        fullWidth={1000}
                                        //inputProps={onchange = handleChange('description')}
                                    />

                            </AccordionDetails>
                        </Accordion>
                        <Accordion>
                            <AccordionSummary
                                expandIcon={<Fab color="primary" aria-label="add">
                                    <AddIcon />
                                </Fab>}
                                aria-controls="panel2a-content"
                                id="panel2a-header"
                            >
                                <Typography className={classes.heading}>
                                    File
                                </Typography>
                            </AccordionSummary>
                            <AccordionDetails >
                                <div fullWidth={1000}>
                                    <App />
                                </div>

                            </AccordionDetails>
                        </Accordion>
                        <Accordion >
                            <AccordionSummary
                                expandIcon={<Fab color="primary" aria-label="add">
                                    <AddIcon />
                                </Fab>}
                                aria-controls="panel3a-content"
                                id="panel3a-header"
                            >
                                <Typography className={classes.heading}>External path</Typography>
                            </AccordionSummary>
                            <AccordionDetails>
                                <CustomInput
                                    labelText="www.CEF-Conseil.com..."
                                    id="title"
                                    type="url"
                                />

                            </AccordionDetails>
                        </Accordion>
                    </div>
                </TabPanel>
                <TabPanel value={value} index={1} dir={theme.direction}>
                    question
                </TabPanel>

            </SwipeableViews>
        </div>




    );
}
