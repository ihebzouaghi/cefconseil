import React, {useEffect, useState} from "react";
import axios from "axios";
import {toast} from "react-toastify";
import DialogContentText from "@material-ui/core/DialogContentText";
import CustomInput from "../../components/CustomInput/CustomInput";
//import Button from "@material-ui/core/Button";
import DialogContent from "@material-ui/core/DialogContent/DialogContent";
import InputLabel from "@material-ui/core/InputLabel";
import FormControl from "@material-ui/core/FormControl";
import {makeStyles} from "@material-ui/core/styles";
import CardFooter from "../../dashboard/Card/CardFooter";
import Button from "../../dashboard/CustomButtons/Button.js";
import { withRouter } from "react-router-dom";
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import DialogActions from "@material-ui/core/DialogActions/DialogActions";
import Dialog from "@material-ui/core/Dialog/Dialog";


const styles = {
    cardCategoryWhite: {
        color: "rgba(255,255,255,.62)",
        margin: "0",
        fontSize: "14px",
        marginTop: "0",
        marginBottom: "0"
    },
    cardTitleWhite: {
        color: "#FFFFFF",
        marginTop: "0px",
        minHeight: "auto",
        fontWeight: "300",
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
        marginBottom: "3px",
        textDecoration: "none"
    },
    table: {
        minWidth: 700,
        maxWidth:800,
    },
    formControl: {
        margin: "1",
        minWidth: 320,
        marginBottom: "",
        top: 27,
    },
    selectEmpty: {
        marginTop: "2",
    },
    //select: { textAlignLast: "left" },
    //option :{ textAlignLast: "left"  }

};




const AddStep = (props) => {


    const [formDataa,setData]= useState({
        steps:[],
        data: [],
        title:'',
        //type: '',
        textSimple: '',
        externalPat: '',
        textChange: 'Add',
        g:''

    });


    const g=0;
    const a = formDataa.steps[formDataa.steps.length - 1];
    const b = formDataa.steps.length ;
    const c = formDataa.steps.map((b) => (b.title));
    const d = c[c.length-1];
    const [count, setCount] = useState(1);
    const f =  parseInt(String(d).slice(5, 7));
    const h = String(d).slice(5, 6);
    const shouldShow = f >= 1
    console.log(d,count,f,h);

    useEffect(async () => {
        const result = await axios(
            'http://localhost:5000/api/step/get',
        );

        setData(result.data);

    }, []);

   // h==="NaN"   ?setCount(count+1) :setCount(f)




    const [formData, setFormData] = useState({
        title: '',
        type: '',
        textChange: 'Add'
    });


    const useStyles = makeStyles(styles);

    const { type,  textChange } = formData;

    const handleChange = text => e => {
        setFormData({ ...formData, [text]: e.target.value });

    };

    const handleSubmit = e => {


        e.preventDefault();
        if (  type ) {
            setFormData({ ...formData, textChange: 'Submitting' });
            axios
                .post(`http://localhost:5000/api/step/add`, {
                    title:("Step "+(shouldShow ?f+1 :1)),
                    type
                })
                .then(res => {
                    setFormData({
                        ...formData,
                        title: ("Step "+(shouldShow ?f+1 :1)),
                        type: '',
                        textChange: 'Submitted'
                    });
                    console.log(formData);
                    window.location.reload(false);
                })
                .catch(err => {
                    setFormData({
                        ...formData,
                        title: '',
                        type: '',
                        textChange: 'Not submitted'
                    });
                    console.log(err.response);
                });

        } else {
            toast.error('Please fill all fields');
        }
    };
    const classes = useStyles();

    return (

        <DialogContent>
            <DialogContentText>
                Here you can add a new Step
            </DialogContentText>
            <form onSubmit={handleSubmit}>


                <CustomInput
                    //labelText={("Step "+(f+1))}
                    labelText={"Step " +(shouldShow ?f+1 :1)}
                    id="title"
                    inputProps={("Step "+(f+1))}
                    formControlProps={{
                        fullWidth: true,
                        disabled: true
                    }}

                />

                <FormControl className={classes.formControl}>
                <InputLabel id="demo-simple-select-label">Type...</InputLabel>
                <Select
                    style={styles}
                    id="type"
                    fullWidth={500}
                    formControlProps={{
                        fullWidth: true
                    }}
                    inputProps={{

                        onChange: handleChange('type'),


                    }}
                >
                    <MenuItem value="Quiz">Quiz</MenuItem>
                    <MenuItem value="Lesson">Lesson</MenuItem>
                    <MenuItem value="Hybrid">Hybrid</MenuItem>
                </Select>
                </FormControl>
                <CardFooter>

                </CardFooter>
                <br></br><br></br>
            <Button type='submit' color="primary" >
                <span className='ml-3'  >{textChange}</span>
            </Button>

            </form>
            <br></br>
        </DialogContent>


    );
};

export default withRouter(AddStep);
