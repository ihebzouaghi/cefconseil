import React, { useState, useEffect} from "react";
import axios from "axios";

import { makeStyles } from "@material-ui/core/styles";

import GridItem from "./../../components/Grid/GridItem.js";
import GridContainer from "./../../components/Grid/GridContainer.js";
import Card from "./../../components/Card/Card.js";
import CardHeader from "./../../components/Card/CardHeader.js";
import CardBody from "./../../components/Card/CardBody.js";
import CardFooter from "./../../components/Card/CardFooter.js";
import AddIcon from '@material-ui/icons/Add';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import IconButton from "@material-ui/core/IconButton";

import styles from "./../../dashboard/jss/material-dashboard-react/views/dashboardStyle.js";
import Fab from "@material-ui/core/Fab";
import Tooltip from "@material-ui/core/Tooltip";
import Button from "../../components/CustomButtons/Button";
import Dialog from "@material-ui/core/Dialog/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent/DialogContent";
import DialogActions from "@material-ui/core/DialogActions/DialogActions";
import AddStep from "./AddStep";
import DialogContentText from "@material-ui/core/DialogContentText";
const useStyles = makeStyles(styles);

const StepList = (props) => {

    const [formData,setData]= useState({
        steps:[],
        data: [],
        title: '',
        type: ''
    });

    useEffect(async () => {
        const result = await axios(
            'http://localhost:5000/api/step/get',
        );
        setData(result.data);
    }, []);


    const stepDetails = (productID) => {
        axios.get(`http://localhost:5000/api/step/`+productID)
            .then(res => {
                console.log(res.data);
                props.history.push('/EditTraining/'+productID);
            })
            .catch(e => {
                console.log(e);
            });
    };
/*
    const deleteTraining = (id) => {
        axios.delete(`http://localhost:5000/api/training/delete/`+id)
            .then(response => {
                console.log(response.data);
                window.location.reload();
            })
            .catch(e => {
                console.log(e);
            });
    };
*/
    const [open, setOpen  ] = React.useState(false);
    const [show , setShow ] = React.useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const handleShow = () => {
        setShow(true);
    };

    const handleDontShow = () => {
        setShow(false);
    };

    const { ...rest } = props;
    const classes = useStyles();

    return(
        <GridContainer { ...rest }>
            {formData.steps.map((row) => (
                <GridItem xs={12} sm={12} md={4} key={row._id}>

                    <Card chart>
                        <CardHeader color="transparent" >
                            <img width="320"
                                 height="200"
                                 alt="..."
                                 src={"/images/training/"+row.image}
                            />
                        </CardHeader>
                        <CardBody>
                            <h4 className={classes.cardTitle}>{row.title}</h4>
                            <p className={classes.cardCategory}>{row.type}</p>
                        </CardBody>
                        <CardFooter chart>
                            <Tooltip
                                title="Edit the Training"
                                placement={window.innerWidth > 959 ? "top" : "left"}
                            >
                                <Button color="transparent"  onClick={() => stepDetails(row._id)}>
                                    <EditIcon />
                                </Button>
                            </Tooltip>
                            <span>
                                                <Tooltip
                                                    title="Delete Training"
                                                    placement={window.innerWidth > 959 ? "top" : "left"}
                                                >
      <IconButton aria-label="Delete" onClick={handleShow} color="secondary">
        <DeleteIcon />
      </IconButton>
                                                </Tooltip>
      <Dialog open={show} onClose={handleDontShow} fullWidth={true}>
        <DialogTitle>{"Delete "+row.title}</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Confirm to delete your training {row.title}.
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleDontShow} color="primary">
            Cancel
          </Button>
          <Button color="danger" autoFocus="autoFocus">
            Confirm
          </Button>
        </DialogActions>
      </Dialog>
    </span>
                        </CardFooter>
                    </Card>
                </GridItem>
            ))}
            <Tooltip
                title="Add a new Training"
                placement={window.innerWidth > 959 ? "top" : "left"}
            >
                <Fab color="secondary" aria-label="add" onClick={AddStep && handleClickOpen}>
                    <AddIcon />
                </Fab>
            </Tooltip>
            <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Add new training</DialogTitle>
                <DialogContent>
                    <AddStep/>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        Cancel
                    </Button>
                </DialogActions>
            </Dialog>
        </GridContainer>
    );
};

export default StepList;
