import React, { useState, useEffect} from "react";
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import axios from 'axios';
import CardHeader from "../dashboard/Card/CardHeader";
import Card from "../dashboard/Card/Card";
import CardBody from "./../dashboard/Card/CardBody.js";
import DeleteIcon from "@material-ui/icons/Delete";
import IconButton from "@material-ui/core/IconButton";
import {useConfirm} from "material-ui-confirm";


const StyledTableCell = withStyles((theme) => ({
    head: {
        backgroundColor: theme.palette.common.black,
        color: theme.palette.common.white,
    },
    body: {
        fontSize: 14,
    },
}))(TableCell);
const StyledTableRow = withStyles((theme) => ({
    root: {
        '&:nth-of-type(odd)': {
            backgroundColor: theme.palette.action.hover,
        },
    },
}))(TableRow);
const styles = {
    cardCategoryWhite: {
        color: "rgba(255,255,255,.62)",
        margin: "0",
        fontSize: "14px",
        marginTop: "0",
        marginBottom: "0"
    },
    cardTitleWhite: {
        color: "#FFFFFF",
        marginTop: "0px",
        minHeight: "auto",
        fontWeight: "300",
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
        marginBottom: "3px",
        textDecoration: "none"
    },
    table: {
        minWidth: 700,
        maxWidth:800,
    },
};
const useStyles = makeStyles(styles);

const ListClient = (props) => {
    const [data,setData]= useState({
        users:[],
        data: []
    });
    useEffect(async () => {
        const result = await axios(
            'http://localhost:5000/api/user/',
        );
        setData(result.data);
    }, []);

    const confirm = useConfirm();

    const handleDelete = (item,name) => {
        confirm({ description: `This will permanently delete ${name}.` })
            .then(() => axios.delete(`http://localhost:5000/api/user/delete/`+item).then(response => {
                console.log(response.data);
                window.location.reload();
            }))
            .catch(() => console.log("Deletion cancelled."));
    };



    const classes = useStyles();
    const { ...rest } = props;

    return(
        <Card {...rest}>
            <CardHeader color="primary">
                <h4 className={classes.cardTitleWhite}>List Client</h4>
            </CardHeader>
            <CardBody>
                <div style={{ height: 2000, width: '100%' }}>
                    <TableContainer>
                        <Table className={useStyles.table} aria-label="customized table">
                            <TableHead >
                                <TableRow >
                                    <StyledTableCell>Name</StyledTableCell>
                                    <StyledTableCell>Email</StyledTableCell>
                                    <StyledTableCell>Role</StyledTableCell>
                                    <StyledTableCell>Options</StyledTableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {data.users.map((row) => (
                                    <StyledTableRow key={row._id}>
                                        <StyledTableCell component="th" scope="row">
                                            {row.name}
                                        </StyledTableCell>
                                        <StyledTableCell component="th" scope="row">
                                            {row.email}
                                        </StyledTableCell>
                                        <StyledTableCell component="th" scope="row">
                                            {row.role}
                                        </StyledTableCell>
                                        <StyledTableCell component="th" scope="row">
                                            <IconButton aria-label="Delete" onClick={() => handleDelete(row._id,row.name)} color="default">
                                                <DeleteIcon />
                                            </IconButton>
                                        </StyledTableCell>
                                    </StyledTableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </TableContainer>
                </div>
            </CardBody>
        </Card>
    );
};
export default ListClient;
