import React from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

// @material-ui/icons
import Chat from "@material-ui/icons/Chat";
import VerifiedUser from "@material-ui/icons/VerifiedUser";
import Fingerprint from "@material-ui/icons/Fingerprint";
// core components
import GridContainer from "./../../../components/Grid/GridContainer.js";
import GridItem from "./../../../components/Grid/GridItem.js";
import InfoArea from "./../../../components/InfoArea/InfoArea.js";

import styles from "./../../../assests/jss/material-kit-react/views/landingPageSections/productStyle.js";

const useStyles = makeStyles(styles);

export default function ProductSection() {
  const classes = useStyles();
  return (
    <div className={classes.section}>
      <GridContainer justify="center">
        <GridItem xs={12} sm={12} md={8}>
          <h2 className={classes.title}>About us</h2>
          <h5 className={classes.description}>
            CEF Conseil est un cabinet d’Etudes - formation et conseil dans les laboratoires principalement le volet
            de la (ISO17025, Métrologie, Etalonnage des Instruments, Formulation, AUDIT…) les Systèmes de Managements
            & Amélioration Continue dans les industries Pharmaceutiques, Agro-alimentaire, Chimique, Biologiques,
            Automobiles, construction Navales ( QHSE , ISO13485, ISO22716, ISO22000, FSSC22000, BRC22000…) et
            Santé sécurité de travail ( ISO45001, Etude des dangers, POI, secourisme, Plan d’évacuation, Traçage au sol, éclairage, Acoustique….)
            CEF Conseil s’engage dans une perspective d’amélioration efficace et durable des performances opérationnelles et stratégiques de l’entreprise.
          </h5>
        </GridItem>
      </GridContainer>
      <div>
        <GridContainer>
          <GridItem xs={12} sm={12} md={4}>
            <InfoArea
              title="Free Chat"
              description="Divide details about your product or agency work into parts. Write a few lines about each one. A paragraph describing a feature will be enough."
              icon={Chat}
              iconColor="info"
              vertical
            />
          </GridItem>
          <GridItem xs={12} sm={12} md={4}>
            <InfoArea
              title="Verified Users"
              description="Divide details about your product or agency work into parts. Write a few lines about each one. A paragraph describing a feature will be enough."
              icon={VerifiedUser}
              iconColor="success"
              vertical
            />
          </GridItem>
          <GridItem xs={12} sm={12} md={4}>
            <InfoArea
              title="Fingerprint"
              description="Divide details about your product or agency work into parts. Write a few lines about each one. A paragraph describing a feature will be enough."
              icon={Fingerprint}
              iconColor="danger"
              vertical
            />
          </GridItem>
        </GridContainer>
      </div>
    </div>
  );
}
