import React, {useEffect, useState , useReducer} from "react";
import axios from "axios";
import {toast} from "react-toastify";
import {useParams} from "react-router-dom";
import Header from "../../../components/Header/Header";
import HeaderLinks from "../../../components/Header/HeaderLinks";
import { makeStyles } from "@material-ui/core/styles";
import styles from "../../../assests/jss/material-kit-react/views/profilePage";
import classNames from "classnames";
import GridItem from "../../../components/Grid/GridItem";
import GridContainer from "../../../components/Grid/GridContainer";
import Parallax from "../../../components/Parallax/Parallax";
import './MyTraining.css';
import ThumbUpAltIcon from '@material-ui/icons/ThumbUpAlt';
import ThumbDownAltIcon from '@material-ui/icons/ThumbDownAlt';
import {isAuth} from "../../../helpers/auth";

const useStyles = makeStyles(styles);


const MyTraining = (props) => {

    let { id } = useParams();

  const [formData, setFormData] = useState({
      training : "",
      likes: [],
      dislikes: [],
      learner : []
    });

    useEffect(() => {
        loadParticipation();
    }, []);

    //const {training , likes , dislikes } = formData;

    const loadParticipation = () => {
        axios
            .get(`http://localhost:5000/api/participation/`+id)
            .then(res => {
                setFormData(res.data);
                //console.log(res.data);
            })
            .catch(err => {
                toast.error(`Error To Your Information ${err.response.statusText}`);
                if (err.response.status === 401) {
                }
            });
    };


   const appReducer = (state, action) => {
  switch(action.type) {
      case 'UPDATE_LIKES':
          return {
              ...state,
              likes: action.payload.likes,
          };
    case 'HANDLE_LIKE':
      return {
        ...state,
        likes: state.likes + action.payload
      };
      case 'UPDATE_DISLIKES' :
          return {
            ...state,
            dislikes: action.payload.dislikes,
          };
    case 'HANDLE_DISLIKE':
      return {
        ...state,
        dislikes: state.dislikes + action.payload
      };
    default:
      return state
  }
};

  const [state, dispatch] = useReducer(appReducer, formData);
  const { likes, dislikes ,training , learner} = formData;
  const [status, setStatus] = useState(null);

  const handleClickLike = () => {
    if (status==='like') {
      setStatus(null);
        try {
            const res =  axios.put(`http://localhost:5000/api/participation/unlike/`+id + '/' + isAuth()._id);
            dispatch({
                type: 'UPDATE_LIKES',
                payload: { id, likes: res.data },
            });
            alert("Like removed successfully", "danger");
        } catch (err) {
            alert("wrong")
        }
    } else {
        setStatus('like');
        if (status === 'dislike') {
            dispatch({
                type: 'HANDLE_DISLIKE',
                payload: -1,
            })
        }
        try {
            const res = axios.put(`http://localhost:5000/api/participation/like/` + id + '/' + isAuth()._id);
            dispatch({
                type: 'UPDATE_LIKES',
                payload: {id, likes: res.data},
            });
            alert("Like added successfully", "success");
            console.log(likes);
        } catch (err) {
            alert("Training already liked")
        }
    }};

  const handleClickDislike = () => {
    if (status==='dislike') {
      setStatus(null);
        try {
            const res =  axios.put(`http://localhost:5000/api/participation/undislike/`+id + '/' + isAuth()._id);
            dispatch({
                type: 'UPDATE_DISLIKES',
                payload: { id, dislikes: res.data },
            });
            alert("Dislike removed successfully", "danger");
        } catch (err) {
            alert("wrong")
        }
    } else {
      setStatus('dislike');
      if (status==='like') {
        dispatch({
          type: 'HANDLE_LIKE',
          payload: -1,
        })
      }
        try {
            const res =  axios.put(`http://localhost:5000/api/participation/dislike/`+id + '/' + isAuth()._id);
            dispatch({
                type: 'UPDATE_DISLIKES',
                payload: { id, dislikes: res.data },
            });
           alert("Dislike added successfully", "success");
        } catch (err) {
            alert("wrong")
        }
    }
  };

    const checkLiked = () => {

            const authUser = isAuth()._id;
            let checkLike = likes.some((like) => like.user === authUser);
            if (checkLike) {
                return true;
            } else {
                return false;
            }
    };

    const checkDisliked = () => {

        const authUser = isAuth()._id;
        let checkLike = dislikes.some((like) => like.user === authUser);
        if (checkLike) {
            return true;
        } else {
            return false;
        }
    };

   const ija  = learner.filter(l => l._id === isAuth()._id).map(filteredLearner => filteredLearner._id);
  console.log(ija);

    const classes = useStyles();
    const { ...rest } = props;

    return (

        <div>
            <Header
                color="transparent"
                brand="CEF CONSEIL"
                rightLinks={<HeaderLinks />}
                fixed
                changeColorOnScroll={{
                    height: 200,
                    color: "white"
                }}
                { ...rest }
            />
            <Parallax small filter image={"/images/training/"+training.image} />
            <div className={classNames(classes.main, classes.mainRaised)}>
                <div className={classes.container}>
                    <GridContainer justify="center">
                        <GridItem  xs={12} sm={12} md={6}>
                            <div className={classes.profile}>
                            <h3> {training.title} </h3>
                            <h6> By CEF Conseil </h6>
                            </div>
                        </GridItem>
                    </GridContainer>
                </div>
            </div>
            <div className={classNames(classes.main, classes.mainRaised)} style={{ position : 'relative' , top : 100 }}>
                <div className={classes.container}>
                <div >
                    <h2 style={{ fontSize : 35 , paddingTop : 70 }}> Welcome! </h2>
                    <p style={{ display: "flex" , alignItems: "stretch" }}> Welcome to <p style={{ fontWeight : "bold" , marginLeft : 5 , marginRight : 5 }}> { training.title}. </p>This course should take approximately 2 hours to finish. </p>
                    <h2 style={{ fontSize : 35 }}> Description : </h2>
                    <p>{training.description}</p>
                    <h2 style={{ fontSize : 35 }}> Course Learning Modules : </h2>
                </div>
                    <div className='jdid'>
                        {checkLiked() ? (
                            <button className={classNames(status === 'like' ? 'btn active' : 'btn' , checkLiked() ? 'btn active' : 'btn')}>
                                <ThumbUpAltIcon/>
                                <span>{likes.length + " "}</span>
                            </button>
                            ) : (
                            <button className={classNames(status === 'like' ? 'btn active' : 'btn' )}
                            onClick={handleClickLike}>
                            <ThumbUpAltIcon/>
                            <span>{likes.length + " "}</span>
                            </button>
                        )}
                        <button className='new'> </button>
                        {checkDisliked() ? (
                            <button className={classNames(status==='dislike'? 'btn inactive' : 'btn' , checkDisliked() ? 'btn inactive' : 'btn' )}>
                            <ThumbDownAltIcon/>
                            <span>{dislikes.length + " "}</span>
                            </button>
                        ) : (
                        <button className={status==='dislike'? 'btn inactive' : 'btn'} onClick={handleClickDislike}>
                            <ThumbDownAltIcon/>
                            <span>{dislikes.length + " "}</span>
                        </button>
                        )}
                    </div>
                </div>
            </div>

        </div>

    );
};

export default MyTraining;
