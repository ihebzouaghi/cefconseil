import React, { useState } from "react";
import Popup from 'reactjs-popup';
import 'reactjs-popup/dist/index.css';
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import InputAdornment from "@material-ui/core/InputAdornment";
import Icon from "@material-ui/core/Icon";
// @material-ui/icons
import Email from "@material-ui/icons/Email";
import People from "@material-ui/icons/People";
// core components
import Header from "./../../../components/Header/Header.js";
import HeaderLinks from "./../../../components/Header/HeaderLinks.js";
import Footer from "./../../../components/Footer/Footer.js";
import GridContainer from "./../../../components/Grid/GridContainer.js";
import GridItem from "./../../../components/Grid/GridItem.js";
import Button from "./../../../components/CustomButtons/Button.js";
import Card from "./../../../components/Card/Card.js";
import CardBody from "./../../../components/Card/CardBody.js";
import CardHeader from "./../../../components/Card/CardHeader.js";
import CardFooter from "./../../../components/Card/CardFooter.js";
import CustomInput from "./../../../components/CustomInput/CustomInput.js";
import { FacebookLoginButton } from "react-social-login-buttons";
import { GoogleLoginButton  } from "react-social-login-buttons";
import {Link, Redirect} from 'react-router-dom';

import image from "./../../../assests/img/bg7.jpg";
import axios from "axios";
import {authenticate, isAuth} from "../../../helpers/auth";
import {toast} from "react-toastify";
import {GoogleLogin} from "react-google-login";
import FacebookLogin from "react-facebook-login/dist/facebook-login-render-props";
import LockIcon from '@material-ui/icons/Lock';
import ForgetPassword from "../../ForgetPassword";

import styles from "./../../../assests/jss/material-kit-react/views/loginPage.js";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import TextField from "@material-ui/core/TextField";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContentText from "@material-ui/core/DialogContentText";


const useStyles = makeStyles(styles);



const LoginPage = ( {history },props) => {
    const [formData, setFormData , cardAnimaton ] = useState({
        email: '',
        password1: '',
        textChange: 'Sign In'
    });

    const [open, setOpen] = React.useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const classes = useStyles();
    const { ...rest } = props;

    const { email, password1, textChange } = formData;
    const handleChange = text => e => {
        setFormData({ ...formData, [text]: e.target.value });
    };

    const sendGoogleToken = tokenId => {
        axios
            .post(`http://localhost:5000/api/user/googlelogin`, {
                idToken: tokenId
            })
            .then(res => {
                console.log(res.data);
                informParent(res);
            })
            .catch(error => {
                console.log('GOOGLE SIGNIN ERROR', error.response);
            });
    };
    const informParent = response => {
        authenticate(response, () => {
            isAuth() && isAuth().role === 'Admin'
                ? history.push('/ProfilePage')
                : history.push('/landing-page');
        });
    };

    const sendFacebookToken = (userID, accessToken) => {
        axios
            .post(`http://localhost:5000/api/user/facebooklogin`, {
                userID,
                accessToken
            })
            .then(res => {
                console.log(res.data);
                informParent(res);
            })
            .catch(error => {
                console.log('GOOGLE SIGNIN ERROR', error.response);
            });
    };
    const responseGoogle = response => {
        console.log(response);
        sendGoogleToken(response.tokenId);
    };

    const responseFacebook = response => {
        console.log(response);
        sendFacebookToken(response.userID, response.accessToken)
    };

    const handleSubmit = e => {
        //console.log(process.env.REACT_APP_API_URL);
        e.preventDefault();
        if (email && password1) {
            setFormData({ ...formData, textChange: 'Submitting' });
            axios
                .post(`http://localhost:5000/api/user/login`, {
                    email,
                    password: password1
                })
                .then(res => {
                    authenticate(res, () => {
                        setFormData({
                            ...formData,
                            email: '',
                            password1: '',
                            textChange: 'Submitted'
                        });
                        isAuth() && isAuth().role === 'Admin'
                            ? history.push('/admin')
                            : history.push('/ProfilePage');
                        toast.success(`Hey ${res.data.user.name}, Welcome back!`);
                    });
                })
                .catch(err => {
                    setFormData({
                        ...formData,
                        email: '',
                        password1: '',
                        textChange: 'Sign In'
                    });
                    console.log(err.response);
                    toast.error(err.response.data.errors);
                });
        } else {
            toast.error('Please fill all fields');
        }
    };

    return (
        <div>
            <Header
                absolute
                color="transparent"
                brand="CEF CONSEIL"
                rightLinks={<HeaderLinks />}
                {...rest}
            />
            <div
                className={classes.pageHeader}
                style={{
                    backgroundImage: "url(" + image + ")",
                    backgroundSize: "cover",
                    backgroundPosition: "top center"
                }}
            >
                {isAuth() ? <Redirect to='/' /> : null}
                <div className={classes.container}>
                    <GridContainer justify="center">
                        <GridItem xs={12} sm={12} md={4}>
                            <Card className={classes[cardAnimaton]}>
                                <form className={classes.form} onSubmit={handleSubmit}>
                                    <CardHeader color="primary" className={classes.cardHeader}>
                                        <h4>Login</h4>
                                    </CardHeader>
                                    <CardBody>
                                        <CustomInput
                                            labelText="Email..."
                                            id="email"
                                            formControlProps={{
                                                fullWidth: true
                                            }}
                                            inputProps={{
                                                onChange: handleChange('email'),
                                                type: "email",
                                                endAdornment: (
                                                    <InputAdornment position="end">
                                                        <Email className={classes.inputIconsColor} />
                                                    </InputAdornment>
                                                )
                                            }}
                                        />
                                        <CustomInput
                                            labelText="Password"
                                            id="password1"
                                            formControlProps={{
                                                fullWidth: true
                                            }}
                                            inputProps={{
                                                onChange: handleChange('password1'),
                                                type: "password",
                                                endAdornment: (
                                                    <InputAdornment position="end">
                                                        <LockIcon className={classes.inputIconsColor}>
                                                        </LockIcon>
                                                    </InputAdornment>
                                                ),
                                                autoComplete: "off"
                                            }}
                                        />

                                    </CardBody>
                                    <CardFooter className={classes.cardFooter}>
                                        <GridContainer justify="center" >
                                        <Button type="submit" simple color="primary" size="lg">
                                            LOGIN
                                        </Button>

                                            <GoogleLogin
                                                clientId={`${process.env.REACT_APP_GOOGLE_CLIENT_ID}`}
                                                onSuccess={responseGoogle}
                                                onFailure={responseGoogle}
                                                cookiePolicy={'single_host_origin'}
                                                render={renderProps => (
                                                    <GoogleLoginButton
                                                        onClick={renderProps.onClick}
                                                        disabled={renderProps.disabled}
                                                        className='w-full max-w-xs font-bold shadow-sm rounded-lg py-3 bg-indigo-100 text-gray-800 flex items-center justify-center transition-all duration-300 ease-in-out focus:outline-none hover:shadow focus:shadow-sm focus:shadow-outline'
                                                    >
                                                    </GoogleLoginButton >
                                                )}
                                            >
                                            </GoogleLogin>
                                            <FacebookLogin
                                                appId={`${process.env.REACT_APP_FACEBOOK_CLIENT}`}
                                                autoLoad={false}
                                                callback={responseFacebook}
                                                render={renderProps => (
                                                    <FacebookLoginButton
                                                        onClick={renderProps.onClick}
                                                    >
                                                    </FacebookLoginButton >
                                                )}
                                            />

                                                <div>
                                                    <Button variant="outlined" color="transparent" onClick={ForgetPassword && handleClickOpen}>
                                                        Forget password?
                                                    </Button>
                                                    <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                                                        <DialogTitle id="form-dialog-title">Forget password?</DialogTitle>
                                                        <DialogContent>
                                                            <ForgetPassword/>
                                                        </DialogContent>
                                                        <DialogActions>
                                                            <Button onClick={handleClose} color="primary">
                                                                Cancel
                                                            </Button>
                                                        </DialogActions>
                                                    </Dialog>
                                                </div>
                                        </GridContainer>
                                    </CardFooter>
                                </form>
                            </Card>
                        </GridItem>
                    </GridContainer>
                </div>
                <Footer whiteFont />
            </div>
        </div>
    );
};

export default LoginPage;
