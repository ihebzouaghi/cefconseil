import React, {useState} from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import InputAdornment from "@material-ui/core/InputAdornment";
import Icon from "@material-ui/core/Icon";
// @material-ui/icons
import Email from "@material-ui/icons/Email";
import People from "@material-ui/icons/People";
// core components
import Header from "./../../../components/Header/Header.js";
import HeaderLinks from "./../../../components/Header/HeaderLinks.js";
import Footer from "./../../../components/Footer/Footer.js";
import GridContainer from "./../../../components/Grid/GridContainer.js";
import GridItem from "./../../../components/Grid/GridItem.js";
import Button from "./../../../components/CustomButtons/Button.js";
import Card from "./../../../components/Card/Card.js";
import CardBody from "./../../../components/Card/CardBody.js";
import CardHeader from "./../../../components/Card/CardHeader.js";
import CardFooter from "./../../../components/Card/CardFooter.js";
import CustomInput from "./../../../components/CustomInput/CustomInput.js";
import LockIcon from '@material-ui/icons/Lock';
import styles from "./../../../assests/jss/material-kit-react/views/loginPage.js";

import image from "./../../../assests/img/bg7.jpg";
import axios from "axios";
import {toast} from "react-toastify";

const useStyles = makeStyles(styles);

const RegisterPage =() => {

    const [formData, setFormData , cardAnimaton] = useState({
        name: '',
        email: '',
        password1: '',
        textChange: 'sign Up'
    });
    const { name, email, password1, textChange } = formData;
    const handleChange = text => e => {
        setFormData({ ...formData, [text]: e.target.value });
    };
    const handleSubmit = e => {
        e.preventDefault();
        if (name && email && password1) {
            setFormData({ ...formData, textChange: 'Submitting' });
            axios
                .post(`http://localhost:5000/api/user/Register`, {
                    name,
                    email,
                    password: password1
                })
                .then(res => {
                    setFormData({
                        ...formData,
                        name: '',
                        email: '',
                        password1: '',
                        textChange: 'Submitted'
                    });
                    toast.success(res.data.message);
                    console.log(formData);
                })
                .catch(err => {
                    setFormData({
                        ...formData,
                        name: '',
                        email: '',
                        password1: '',
                        textChange: 'Sign Up'
                    });
                    console.log(err.response);
                    toast.error(err.response.data.errors);
                });
        } else {
            toast.error('Please fill all fields');
        }
    };

    const classes = useStyles();


    return (
        <div>
            <Header
                absolute
                color="transparent"
                brand="CEF CONSEIL"
                rightLinks={<HeaderLinks />}

            />
            <div
                className={classes.pageHeader}
                style={{
                    backgroundImage: "url(" + image + ")",
                    backgroundSize: "cover",
                    backgroundPosition: "top center"
                }}
            >
                <div className={classes.container}>
                    <GridContainer justify="center">
                        <GridItem xs={12} sm={12} md={4}>
                            <Card className={classes[cardAnimaton]}>
                                        <form onSubmit={handleSubmit}>
                                            <CardHeader color="primary" className={classes.cardHeader}>
                                                <h4>Register</h4>
                                            </CardHeader>
                                            <CardBody>
                                                <CustomInput
                                                    labelText="Name..."
                                                    id="name"
                                                    formControlProps={{
                                                        fullWidth: true
                                                    }}
                                                    inputProps={{
                                                        onChange: handleChange('name'),
                                                        type: "text",
                                                        endAdornment: (
                                                            <InputAdornment position="end">
                                                                <People className={classes.inputIconsColor} />
                                                            </InputAdornment>
                                                        )
                                                    }}
                                                />
                                                <CustomInput
                                                    labelText="Email..."
                                                    id="email"
                                                    formControlProps={{
                                                        fullWidth: true
                                                    }}
                                                    inputProps={{
                                                        onChange: handleChange('email'),
                                                        type: "email",
                                                        endAdornment: (
                                                            <InputAdornment position="end">
                                                                <Email className={classes.inputIconsColor} />
                                                            </InputAdornment>
                                                        )
                                                    }}
                                                />
                                                <CustomInput
                                                    labelText="Password"
                                                    id="password1"
                                                    formControlProps={{
                                                        fullWidth: true
                                                    }}
                                                    inputProps={{
                                                        onChange: handleChange('password1'),
                                                        type: "password",
                                                        endAdornment: (
                                                            <InputAdornment position="end">
                                                                <LockIcon className={classes.inputIconsColor}>
                                                                </LockIcon>
                                                            </InputAdornment>
                                                        ),
                                                        autoComplete: "off"
                                                    }}
                                                />
                                            </CardBody>
                                            <CardFooter className={classes.cardFooter}>
                                                <Button simple color="primary" type="submit" >
                                                    {textChange}
                                                </Button>
                                            </CardFooter>
                                        </form>
                            </Card>
                        </GridItem>
                    </GridContainer>
                </div>
                <Footer whiteFont />
            </div>
        </div>
    );
};

export default RegisterPage;
