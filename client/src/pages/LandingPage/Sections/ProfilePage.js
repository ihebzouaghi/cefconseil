import React , { useState, useEffect } from "react";

// nodejs library that concatenates classes
import classNames from "classnames";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// @material-ui/icons

// core components
import Header from "./../../../components/Header/Header.js";
import Footer from "./../../../components/Footer/Footer.js";
import GridContainer from "./../../../components/Grid/GridContainer.js";
import GridItem from "./../../../components/Grid/GridItem.js";
import HeaderLinks from "./../../../components/Header/HeaderLinks.js";
import Parallax from "./../../../components/Parallax/Parallax.js";
import Details from "./Details";

import image9 from "./../../../assests/img/profile-bg.jpg";
import {  isAuth, getCookie } from './../../../helpers/auth';
import axios from 'axios';
import { toast } from 'react-toastify';


import styles from "./../../../assests/jss/material-kit-react/views/profilePage.js";
import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper/Paper";
import Box from "@material-ui/core/Box";
import LinearProgress from "@material-ui/core/LinearProgress/LinearProgress";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles(styles);

function LinearProgressWithLabel(props) {
    return (
        <Box display="flex" alignItems="center">
            <Box width="100%" mr={1}>
                <LinearProgress variant="determinate" {...props} />
            </Box>
            <Box minWidth={35}>
                <Typography variant="body2" color="textSecondary">{`${Math.round(
                    props.value,
                )}%`}</Typography>
            </Box>
        </Box>
    );
}

const ProfilePage = (props) => {

    const [formData, setFormData] = useState({
        name: '',
        email: '',
        password1: '',
        textChange: 'Update',
        role: '',
        file : null,
        currentFile: undefined,
        previewImage: undefined,
        message: "",
        isError: false,
        imageInfos: [],
    });

    useEffect(() => {
        loadProfile();
    }, []);

    const loadProfile = () => {
        const token = getCookie('token');
        axios
            .get(`${process.env.REACT_APP_API_URL}/${isAuth()._id}`, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            })
            .then(res => {
                const { role, name, email } = res.data;
                setFormData({ ...formData, role, name, email   });
            })
            .catch(err => {
                toast.error(`Error To Your Information ${err.response.statusText}`);
                if (err.response.status === 401) {
                    console.log(name);
                }
            });
    };
    const { name , file , image , currentFile, previewImage, message, imageInfos, isError} = formData;

    const fileSelectedHandler = e => {
        setFormData({ ...formData, file : e.target.files[0] ,
            previewImage: URL.createObjectURL(e.target.files[0]),
            progress: "LOADING...",
            message: ""
        });
        console.log(file);
    };

    const fileUploadHandler = () => {
        const fd = new FormData();
        fd.append('image',file,file.name);
        fd.append('_id',isAuth()._id);
        axios.post(`${process.env.REACT_APP_API_URL}/upload`,fd).then(res => {
           // console.log(res);
          //  window.location.reload(false);
        });
    };

    const [progress, setProgress] = React.useState(10);

    React.useEffect(() => {
        const timer = setInterval(() => {
            setProgress((prevProgress) => (prevProgress >= 100 ? 10 : prevProgress + 10));
        }, 800);
        return () => {
            clearInterval(timer);
        };
    }, []);

    const classes = useStyles();
    const { ...rest } = props;
    const imageClasses = classNames(
        classes.imgRaised,
        classes.imgRoundedCircle,
        classes.imgFluid
    );
    const navImageClasses = classNames(classes.imgRounded, classes.imgGallery);

   // console.log(isAuth());
    return (
        <div>
            <Header
                color="transparent"
                brand="CEF CONSEIL"
                rightLinks={<HeaderLinks />}
                fixed
                changeColorOnScroll={{
                    height: 200,
                    color: "white"
                }}
                {...rest}
            />
            <Parallax small filter image={image9} />
            <div className={classNames(classes.main, classes.mainRaised)}>
                <div>
                    <div className={classes.container}>
                        <GridContainer justify="center">
                            <GridItem xs={12} sm={12} md={6}>
                                <div className={classes.profile}>
                                    {previewImage ? (
                                        <div>
                                            <img className={imageClasses} src={ previewImage } alt="" />
                                        </div>
                                        ) :
                                        <div>
                                            <img className={imageClasses} src={"/images/"+isAuth().image} alt="" />
                                        </div>}

                                        <label htmlFor="btn-upload" style={{ position : 'relative' , top : -80 }}>
                                            <input
                                                id="btn-upload"
                                                name="btn-upload"
                                                style={{ display: 'none' }}
                                                type="file"
                                                accept="image/*"
                                                onChange={fileSelectedHandler} />
                                            <Button
                                                className="btn-choose"
                                                variant="outlined"
                                                component="span" >
                                                Choose Image
                                            </Button>
                                        </label>
                                        <div className="file-name">
                                            {file ? file.name : null}
                                        </div>
                                        <Button style={{ position : 'relative' , top : -60 }}
                                            className="btn-upload"
                                            color="primary"
                                            variant="contained"
                                            component="span"
                                            disabled={!file}
                                            onClick={fileUploadHandler}>
                                            Upload
                                        </Button>
                                        {file && (
                                            <div>
                                                <LinearProgressWithLabel value={progress} />
                                            </div>)
                                        }

                                </div>
                            </GridItem>
                        </GridContainer>
                        <GridContainer >
                            <Details />
                        </GridContainer>
                    </div>
                </div>
            </div>



            <Footer />
        </div>
    );
};

export default ProfilePage;
