import React, {useEffect, useState} from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

// @material-ui/icons
import Dashboard from "@material-ui/icons/Dashboard";
import Schedule from "@material-ui/icons/Schedule";
import List from "@material-ui/icons/List";

// core components
import GridContainer from "./../../../components/Grid/GridContainer.js";
import GridItem from "./../../../components/Grid/GridItem.js";
import NavPills from "./../../../components/NavPills/NavPills.js";

import styles from "./../../../assests/jss/material-kit-react/views/componentsSections/pillsStyle.js";
import {getCookie, isAuth, updateUser} from "../../../helpers/auth";
import axios from "axios";
import {toast} from "react-toastify";
import work4 from "../../../assests/img/examples/mariya-georgieva.jpg";
import imagesStyles from "../../../assests/jss/material-kit-react/imagesStyles";
import CustomInput from "../../../components/CustomInput/CustomInput";
import Button from "@material-ui/core/Button";
import Search from "@material-ui/core/SvgIcon/SvgIcon";
import Card from "../../../components/Card/Card";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import FullCalendar from "./../../FullCalendar";
import Tooltip from "@material-ui/core/Tooltip/Tooltip";
import ArrowRightAltIcon from '@material-ui/icons/ArrowRightAlt';

const useStyles = makeStyles(styles);


const SectionPills = (props , {history}) => {

    const [formData, setFormData] = useState({
        name: '',
        email: '',
        password1: '',
        textChange: 'Update',
        role: ''
    });

    const [part , setPart] = useState({
        participations : [],
        learner : [],
        training : {}
    });

    useEffect(() => {
        loadProfile();
        loadParticipation();
    }, []);

    const loadProfile = () => {
        const token = getCookie('token');
        axios
            .get(`${process.env.REACT_APP_API_URL}/${isAuth()._id}`, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            })
            .then(res => {
                const { role, name, email } = res.data;
                setFormData({ ...formData, role, name, email });
            })
            .catch(err => {
                toast.error(`Error To Your Information ${err.response.statusText}`);
                if (err.response.status === 401) {
                    console.log(name);
                }
            });
    };

    const loadParticipation = () => {
        axios
            .get(`http://localhost:5000/api/participation/my/${isAuth()._id}`)
            .then(res => {
                setPart(res.data);
                console.log(res.data);
            })
            .catch(err => {
                if (err.res.status === 401) {
                    console.log(part);
                }
            });
    };

    const participationDetails = (id) => {
        axios.get(`http://localhost:5000/api/participation/`+id)
            .then(res => {
                console.log(res.data);
                history.push('/MyTraining/'+id);
            })
            .catch(e => {
                console.log(e);
            });
    };

  /*  const [searchTerm, setSearchTerm] = React.useState("");
    const [searchResults, setSearchResults] = React.useState([]);
    const handleChangee = e => {
        setSearchTerm(e.target.value);
    };

    const results = !searchTerm
        ? part.participations.training
        : part.participations.training.filter(title  =>
            title.training.title.toString().toLowerCase().includes(searchTerm.toLocaleLowerCase())
        ); */

    const { name, email, password1, textChange, role } = formData;
    const handleChange = text => e => {
        setFormData({ ...formData, [text]: e.target.value });
    };
    const handleSubmit = e => {
        const token = getCookie('token');
        console.log(token);
        e.preventDefault();
        setFormData({ ...formData, textChange: 'Submitting' });
        axios
            .put(
                `${process.env.REACT_APP_API_URL}/update`,
                {
                    name: formData.name,
                    email: formData.email,
                    password: formData.password1
                },
                {
                    headers: {
                        Authorization: `Bearer ${token}`
                    }
                }
            )
            .then(res => {
                updateUser(res, () => {
                    toast.success('Profile Updated Successfully');
                    setFormData({ ...formData, textChange: 'Update' });
                });
            })
            .catch(err => {
                console.log(err.response);
            });
    };
    const { ...rest } = props;
    const classes = useStyles();


    return (
        <div className={classes.section}>
            <div className={classes.container}>
                <div id="navigation-pills">
                    <GridContainer justify="center" >
                        <GridItem style={{ justify : 'center' , }} lg={12} >
                            <NavPills
                                color="primary"
                                tabs={[
                                    {
                                        tabButton: "Profile Update",
                                        tabIcon: Dashboard,
                                        tabContent: (
                                            <span>
                                <div className={classes.container}>

                                    <form
                                        onSubmit={handleSubmit}
                                    >
                                        <GridContainer justify="center">

                                            <CustomInput
                                                labelText={role}
                                                id="role"
                                                success
                                                inputProps={role}
                                                formControlProps={{
                                                    fullWidth : true,
                                                    disabled: true
                                                }}
                                            />
                                            <CustomInput
                                                labelText={email}
                                                id="email"
                                                success
                                                inputProps={email}
                                                formControlProps={{
                                                    fullWidth : true,
                                                    disabled: true
                                                }}
                                            />
                                            <CustomInput
                                                labelText="User Name"
                                                id="name"
                                                success
                                                formControlProps={{
                                                    fullWidth : true,
                                                }}
                                                inputProps={{
                                                    onChange: handleChange('name')
                                                }}
                                            />
                                            <CustomInput
                                                labelText="password"
                                                id="password"
                                                success
                                                formControlProps={{
                                                    fullWidth : true,
                                                }}
                                                inputProps={{
                                                    onChange: handleChange('password1'),
                                                    type: "password"
                                                }}
                                            />
                                            <Button
                                                variant='contained'
                                                type='submit'
                                                color="primary"
                                            >{textChange}
                                            </Button>
                                        </GridContainer>
                                    </form>
                                </div>
                      </span>
                                        )
                                    },
                                    {
                                        tabButton: "Schedule",
                                        tabIcon: Schedule,
                                        tabContent: (
                      <span>
                            {/*<FullCalendar/>*/}
                      </span>
                                        )
                                    },
                                    {
                                        tabButton: "My Trainings",
                                        tabIcon: List,
                                        tabContent: (
                      <span>
                                  <GridContainer className={classes.noscroll} >
{/*                                      <div className={classes.searchWrapper}>
                                         <CustomInput
                                         formControlProps={{
                                         className: classes.margin + " " + classes.search
                                         }}
                                         inputProps={{
                                             onChange : handleChangee,
                                             placeholder: "Search",
                                             inputProps: {
                                             "aria-label": "Search"
                                             }
                                         }}
                                         />
                                         <Button color="white" aria-label="edit" justIcon round>
                                            <Search />
                                         </Button>
                                      </div>*/}
                                      {part.participations.map((row) => (
                                          <Card className={classes.card}>
                                              <CardMedia
                                                  className={classes.media}
                                                  image={"/images/training/"+row.training.image}
                                              />
                                              <CardContent className={classes.content}>
                                                  <Typography
                                                      className={"MuiTypography--heading"}
                                                      variant={"h6"}
                                                      gutterBottom
                                                  >
                                                      {row.training.title}
                                                  </Typography>
                                                  <Typography
                                                      className={"MuiTypography--subheading"}
                                                      variant={"caption"}
                                                  >
                                                      {row.training.description}
                                                  </Typography>
                                                  <Divider className={classes.divider} light />
                                                  <Tooltip
                                                      id="tooltip-right"
                                                      placement="right"
                                                      classes={{ tooltip: classes.tooltip }}
                                                      title="Complete your training"
                                                  >
                                                      <Button color="transparent" className={classes.button1} href={"/MyTraining/"+row._id}   >
                                                          <ArrowRightAltIcon color="primary" style={{  width : 50 , height: 40 }} />
                                                      </Button>
                                                  </Tooltip>
                                              </CardContent>
                                          </Card>
                                      ))}
                                  </GridContainer>
                      </span>
                                        )
                                    }
                                ]}
                            />
                        </GridItem>
                    </GridContainer>
                </div>
            </div>
        </div>
    );
};

export default SectionPills;
