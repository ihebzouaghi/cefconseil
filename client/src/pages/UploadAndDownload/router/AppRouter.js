import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import App from '../components/App';
import Header from '../components/Header';
import Dashboard from "../../../dashboard/layouts/Admin.js";
import 'bootstrap/dist/css/bootstrap.min.css';
import '../utils/styles.scss';
import FilesList from '../components/FilesList';


const AppRouter = () => (
    <Dashboard>
        <div className="container">
            <Header />
            <div className="main-content">
                <Switch>
                    <Route component={App} path="/" exact={true} />
                    <Route component={FilesList} path="/listFile" />
                </Switch>
            </div>
        </div>
    </Dashboard>
);

export default AppRouter;
