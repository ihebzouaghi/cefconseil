import React, { useState, useRef } from 'react';
import { Form, Row, Col, Button } from 'react-bootstrap';
import Dropzone from 'react-dropzone';
import axios from 'axios';
import { API_URL } from '../utils/constants';
import img from '../../../dashboard/img/Upload.png';
import DialogContent from "@material-ui/core/DialogContent/DialogContent";

const App = props => {



    const [file, setFile] = useState(null); // state for storing actual image
    const [previewSrc, setPreviewSrc] = useState(''); // state for storing previewImage
    const [state, setState] = useState({
        title: '',
        description: ''
    });
    const [errorMsg, setErrorMsg] = useState('');
    const [isPreviewAvailable, setIsPreviewAvailable] = useState(false); // state to show preview only for images
    const dropRef = useRef(); // React ref for managing the hover state of droppable area
    const pRef = useRef();
    const previewRef = useRef();

    const handleInputChange = (event) => {
        setState({
            ...state,
            [event.target.name]: event.target.value
        });
    };
    const onDrop = (files) => {
        const [uploadedFile] = files;
        setFile(uploadedFile);

        const fileReader = new FileReader();
        fileReader.onload = () => {
            setPreviewSrc(fileReader.result);

        };
        fileReader.readAsDataURL(uploadedFile);
        setIsPreviewAvailable(uploadedFile.name.match(/\.(jpeg|jpg|png|pdf|doc|docx|xlsx|xls|PNG|txt|mp3|mp4)$/));
        dropRef.current.style.border = '4px dashed #e9ebeb';
        dropRef.current.style.backgroundImage= `` ;
    };

    const updateBorder = (dragState) => {
        if (dragState === 'over') {
            dropRef.current.style.border = '2px dashed #C0C0C0';
            pRef.current.style.color = ' ';
            pRef.current.style.fontSize = ' 0em';
            dropRef.current.style.textContent = "DRAG";
            dropRef.current.style.backgroundImage= `url(${img})` ;
            dropRef.current.style.backgroundPosition= 'center';
            dropRef.current.style.backgroundSize= '150px 150px';
            dropRef.current.style.backgroundRepeat= 'no-repeat';


        } else if (dragState === 'leave') {
            dropRef.current.style.border = '4px dashed #e9ebeb';
            pRef.current.style.color = '';
            pRef.current.style.fontSize = '';
            dropRef.current.style.backgroundImage= `` ;

        }
    };

    const handleOnSubmit = async (event) => {
        event.preventDefault();

        try {
            const { step, title, description } = state;
            if (title.trim() !== '' && description.trim() !== '') {
                if (file) {
                    const formData = new FormData();
                    formData.append('file', file);
                    formData.append('title', title);
                    formData.append('description', description);
                    formData.append('step', step);

                    setErrorMsg('');
                    await axios.post(`${API_URL}/upload`, formData, {
                        headers: {
                            'Content-Type': 'multipart/form-data'
                        }
                    });
                    props.history.push('/StepList2');
                } else {
                    setErrorMsg('Please select a file to add.');
                }
            } else {
                setErrorMsg('Please enter all the field values.');
            }
        } catch (error) {
            error.response && setErrorMsg(error.response.data);
        }
    };


    return (
        <DialogContent>
        <React.Fragment>
            <Form className="search-form" onSubmit={handleOnSubmit}>
                {errorMsg && <p className="errorMsg">{errorMsg}</p>}
                <Row>
                    <Col>
                        <Form.Group controlId="title">
                            <Form.Control
                                type="text"
                                name="title"
                                value={state.title || ''}
                                placeholder="Enter title"
                                onChange={handleInputChange}
                            />
                        </Form.Group>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Form.Group controlId="description">
                            <Form.Control
                                type="text"
                                name="description"
                                value={state.description || ''}
                                placeholder="Enter description"
                                onChange={handleInputChange}
                            />
                        </Form.Group>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Form.Group controlId="description">
                            <Form.Control
                                type="text"
                                name="step"
                                value={state.step || ''}
                                placeholder="Enter step id"
                                onChange={handleInputChange}
                            />
                        </Form.Group>
                    </Col>
                </Row>

                <div className="upload-section">
                    <Dropzone onDrop={onDrop}

                              onDragEnter={() => updateBorder('over')}
                              onDragLeave={() => updateBorder('leave')}
                    >

                        {({ getRootProps, getInputProps }) => (
                            <div {...getRootProps({ className: 'drop-zone' })} ref={dropRef} >
                                <input {...getInputProps()} />
                                <p ref={pRef}>Drag and drop a file OR click here to select a file</p>
                                {file && (
                                    <div>
                                        <strong>Selected file:</strong> {file.name}
                                    </div>
                                )}
                            </div>
                        )}
                    </Dropzone>


                    {previewSrc ? (
                        isPreviewAvailable ? (

                            <div className="image-preview" ref={previewRef} >
                                <embed className="preview-image" src={previewSrc} alt="Preview" />
                            </div>
                        ) : (
                            <div className="preview-message">
                                <p>No preview available for this file</p>
                            </div>
                        )
                    ) : (
                        <div className="preview-message">
                            <p>Image preview will be shown here after selection</p>
                        </div>
                    )}

                </div>
                <Button variant="primary" type="submit">
                    Submit
                </Button>
            </Form>
        </React.Fragment>
        </DialogContent>
    );
};

export default App;
