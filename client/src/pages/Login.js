import React, { useState } from 'react';
import authSvg from '../assests/login.svg';
import { ToastContainer, toast } from 'react-toastify';
import { makeStyles } from "@material-ui/core/styles";
import InputAdornment from "@material-ui/core/InputAdornment";
import axios from 'axios';
import {authenticate, isAuth, signout} from '../helpers/auth';
import { Link, Redirect } from 'react-router-dom';
import { GoogleLogin } from 'react-google-login';
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props';
import styles from "../assests/jss/material-kit-react/views/loginPage";
import GridContainer from "../components/Grid/GridContainer";
import GridItem from "../components/Grid/GridItem";
import Card from "../components/Card/Card";
import CardHeader from "../components/Card/CardHeader";
import CardBody from "../components/Card/CardBody.js";
import {FacebookLoginButton, GoogleLoginButton} from "react-social-login-buttons";
import Popup from "reactjs-popup";
import ForgetPassword from "./ForgetPassword";
import CardFooter from "../components/Card/CardFooter";
import CustomInput from "../components/CustomInput/CustomInput";
import Email from "@material-ui/icons/Email";
import LockIcon from "@material-ui/icons/Lock";
import Footer from "../components/Footer/Footer";
import Header from "../components/Header/Header";
import HeaderLinks from "../components/Header/HeaderLinks";
import image from "../assests/img/bg7.jpg";
import Dialog from "@material-ui/core/Dialog/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent/DialogContent";
import DialogActions from "@material-ui/core/DialogActions/DialogActions";
import Button from "../components/CustomButtons/Button";


const useStyles = makeStyles(styles);



const Login = (props,{ history }) => {
    const [formData, setFormData,cardAnimaton] = useState({
        email: '',
        password1: '',
        textChange: 'Sign In'
    });
    const { email, password1, textChange } = formData;
    const handleChange = text => e => {
        setFormData({ ...formData, [text]: e.target.value });
    };

    const [openProfile, setOpenProfile] = React.useState(null);

    const [open, setOpen] = React.useState(true);
    const handleClickOpen = () => {
        setOpen(true);
    };
    const handleClose = () => {
        setOpen(false);
    };

    const handleCloseProfile = () => {
        setOpenProfile(false);
    };

    const classes = useStyles();
    const sendGoogleToken = tokenId => {
        axios
            .post(`http://localhost:5000/api/user/googlelogin`, {
                idToken: tokenId
            })
            .then(res => {
                console.log(res.data);
                informParent(res);
            })
            .catch(error => {
               // console.log('GOOGLE SIGNIN ERROR', error.response);
            });
    };
    const informParent = response => {
        authenticate(response, () => {
            isAuth() && isAuth().role === 'Admin'
                ? history.push('/admin/AddTrainerOrLearner')
                : history.push('/');
        });
    };

    const sendFacebookToken = (userID, accessToken) => {
        axios
            .post(`http://localhost:5000/api/user/facebooklogin`, {
                userID,
                accessToken
            })
            .then(res => {
                console.log(res.data);
                informParent(res);
            })
            .catch(error => {
                console.log('GOOGLE SIGNIN ERROR', error.response);
            });
    };
    const responseGoogle = response => {
        console.log(response);
        sendGoogleToken(response.tokenId);
    };

    const responseFacebook = response => {
        console.log(response);
        sendFacebookToken(response.userID, response.accessToken)
    };

    const handleSubmit = e => {
        //console.log(process.env.REACT_APP_API_URL);
        e.preventDefault();
        if (email && password1) {
            setFormData({ ...formData, textChange: 'Submitting' });
            axios
                .post(`http://localhost:5000/api/user/login`, {
                    email,
                    password: password1
                })
                .then(res => {
                    authenticate(res, () => {
                        setFormData({
                            ...formData,
                            email: '',
                            password1: '',
                            textChange: 'Submitted'
                        });
                       //isAuth() && isAuth().role === 'Admin'
                       if(isAuth().role === 'Admin')
                        {window.location.href = '/admin/AdminProfile'}
                        else
                        {window.location.href = '/admin/addTraining'}


                    });
                })
                .catch(err => {
                    setFormData({
                        ...formData,
                        email: '',
                        password1: '',
                        textChange: 'Sign In'
                    });
                    console.log(err.response);
                });
        } else {
            toast.error('Please fill all fields');
        }
    };



    return (


        <div className={classes.container}  >
            <GridContainer justify="center">
                <GridItem xs={12} sm={12} md={4}>
        <Card className={classes[cardAnimaton]}>
            <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">

                <form onSubmit={handleSubmit}>

                    <CardHeader color="primary" className={classes.cardHeader}>
                        <DialogTitle id="form-dialog-title">
                        <h4>Login</h4>
                       </DialogTitle>
                    </CardHeader>

                    <CardBody>
                    <DialogContent>
                        <CustomInput
                            labelText="Email..."
                            id="email"
                            formControlProps={{
                                fullWidth: true
                            }}
                            inputProps={{
                                onChange: handleChange('email'),
                                type: "email",
                                endAdornment: (
                                    <InputAdornment position="end">
                                        <Email className={classes.inputIconsColor} />
                                    </InputAdornment>
                                )
                            }}
                        />
                        <CustomInput
                            labelText="Password"
                            id="password1"
                            formControlProps={{
                                fullWidth: true
                            }}
                            inputProps={{
                                onChange: handleChange('password1'),
                                type: "password",
                                endAdornment: (
                                    <InputAdornment position="end">
                                        <LockIcon className={classes.inputIconsColor}>
                                        </LockIcon>
                                    </InputAdornment>
                                ),
                                autoComplete: "off"
                            }}
                        />

                    </DialogContent>
                    </CardBody>
                    <DialogActions>
                        <CardFooter className={classes.cardFooter}>
                        <GridContainer justify="center" >
                            <Button type="submit" simple color="primary" size="lg" onClick={handleClose && handleCloseProfile} >
                                LOGIN
                            </Button>

                            <GoogleLogin
                                clientId={`${process.env.REACT_APP_GOOGLE_CLIENT_ID}`}
                                onSuccess={responseGoogle}
                                onFailure={responseGoogle}
                                cookiePolicy={'single_host_origin'}
                                render={renderProps => (
                                    <GoogleLoginButton
                                        onClick={renderProps.onClick}
                                        disabled={renderProps.disabled}
                                        className='w-full max-w-xs font-bold shadow-sm rounded-lg py-3 bg-indigo-100 text-gray-800 flex items-center justify-center transition-all duration-300 ease-in-out focus:outline-none hover:shadow focus:shadow-sm focus:shadow-outline'
                                    >
                                    </GoogleLoginButton >
                                )}
                            >
                            </GoogleLogin>
                            <FacebookLogin
                                appId={`${process.env.REACT_APP_FACEBOOK_CLIENT}`}
                                autoLoad={false}
                                callback={responseFacebook}
                                render={renderProps => (
                                    <FacebookLoginButton
                                        onClick={renderProps.onClick}
                                    >
                                    </FacebookLoginButton >
                                )}
                            />
                            <Popup trigger={<Button onClick={ForgetPassword} color="transparent" >
                                Forget password? </Button>} position="bottom center" >
                                <div className={classes.iheb} ><ForgetPassword/></div>
                            </Popup>
                        </GridContainer>
                        </CardFooter>
                    </DialogActions>
                </form>
            </Dialog>
        </Card>
                </GridItem>
            </GridContainer>
        </div>
    );
};

export default Login;
