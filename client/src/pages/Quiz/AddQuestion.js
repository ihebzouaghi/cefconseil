import React, {useEffect, useState} from "react";
import axios from "axios";
import CustomInput from "../../components/CustomInput/CustomInput";
import Button from "../../dashboard/CustomButtons/Button";
import {makeStyles} from "@material-ui/core/styles";
import GridContainer from "../../components/Grid/GridContainer.js";
import GridItem from "../../components/Grid/GridItem.js";
import classNames from"classnames";
import Fab from '@material-ui/core/Fab';
import IconButton from '@material-ui/core/IconButton';
import AddIcon from '@material-ui/icons/Add';
import DeleteIcon from '@material-ui/icons/Delete';
import TextField from '@material-ui/core/TextField';
import {useParams} from "react-router-dom";
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormGroup from '@material-ui/core/FormGroup';
import Tooltip from "@material-ui/core/Tooltip";
import ClearIcon from '@material-ui/icons/Clear';
import Card from "./../../components/Card/Card.js";


const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
    },
    main: {
        background: "#FFFFFF",
        position: "relative",
        zIndex: "3"
    },
    mainRaised: {
        margin: "-60px 30px 0px",
        borderRadius: "6px",
        boxShadow:
            "0 16px 24px 2px rgba(0, 0, 0, 0.14), 0 6px 30px 5px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(0, 0, 0, 0.2)"
    },
    margin: {
        margin: theme.spacing.unit,
    },
    extendedIcon: {
        marginRight: theme.spacing.unit,
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
    },
    full:{
        borderBottom: '1px solid',
    },
    flexRight:{
        marginLeft:'3%',
        marginTop:'2%',
        fontSize:'120%'

    },
    padding :{
        padding: '10px'
    },
    question:{
        width:'100%',
        display:'flex'
    },
    rightOption:{
        color: 'green',
        fontWeight: 'bold'
    },
    rot:{
        "& .hidden-button": {
            display: "none",

        },
        "&:hover .hidden-button": {
            display: 'inline',
            position: "absolute",
           // marginLeft:'90%',

        }
    }
}));
function AddQuestion() {
    const { StepId }:{StepId : string} = useParams();

    const [inputList, setInputList] = useState([{ OptionName: "", OptionId: "" }]);

    // handle input change
    const handleInputChange = (e, index) => {
        const { name, value } = e.target;
        const list = [...inputList];
        list[index][name] = value;
        setInputList(list);
    };

    // handle click event of the Remove button
    const handleRemoveClick = index => {
        const list = [...inputList];
        list.splice(index, 1);
        setInputList(list);
    };

    // handle click event of the Add button
    const handleAddClick = () => {
        setInputList([...inputList, { OptionName: "", OptionId: "" }]);
    };
    /**************************************        radio Form            **************************************************/
   // const [trueOptionList, setTrueOptionList] = useState(false);
    const [state, setState] = React.useState(false);
    const [trueOptionList, setTrueOptionList] = useState([{ selectId: "", selectState: ""}]);
    const handleInputChangeOption = (e,i) => {

        console.log(state[i+1]);
        trueOptionList.splice(e,1);
        setState({ ...state, [e.target.name]: e.target.checked });

        if(e.target.checked === true  && (state[i+2]===false || state[i+2]===undefined) && (state[i]===false || state[i]=== undefined) && (state[i+3]===false || state[i+3]=== undefined)
            && (state[i+4]===false || state[i+4]=== undefined) && (state[i-3]===false || state[i-3]=== undefined) && (state[i-2]===false || state[i-2]=== undefined)
            && (state[i-1]===false || state[i-1]=== undefined))
        {
            setTrueOptionList([...trueOptionList, { selectId:e.target.name, selectState: e.target.checked  }]);

        }


    };


    console.log(trueOptionList);
    console.log(state);

/**************************************        Submit Form            **************************************************/
const [formData, setFormData] = useState({
    step:'',
    name:'',
    options:[],
    trueOption:[],
    textChange:'Submit'
});
const { name, textChange } = formData;
    const handleChange = text => e => {
        setFormData({ ...formData, [text]: e.target.value });
    };

const handleSubmit = e => {
    e.preventDefault();

        setFormData({ ...formData, textChange: 'Submitting' });
        axios
            .post(`http://localhost:5000/api/question/add`, {
                step:StepId,
                name,
                options:inputList,
                trueOption:trueOptionList
            })
            .then(res => {
                setFormData({
                    ...formData,
                    step:StepId,
                    name:'',
                    options:inputList,
                    trueOption:trueOptionList,
                    textChange:'Submitted'
                });
                console.log(formData);
                window.location.reload(false);
            })
            .catch(err => {
                setFormData({
                    ...formData,
                    name:'',
                    options:inputList,
                    trueOption:trueOptionList,
                    textChange:'Not submitted'
                });
                console.log(err.response);
            });


};

    /**************************************        Get Question           **************************************************/
    const [formDataF,setDataF]= useState(
        {
            questions:[],
            data:[]
        }
    );
    useEffect(async () => {
        const result = await axios(
            'http://localhost:5000/api/question/get',
        );
        setDataF(result.data);
    }, []);


    /**************************************        delete Question           **************************************************/

    const deleteQuestion = (id) => {
        axios.delete(`http://localhost:5000/api/question/delete/`+id)
            .then(response => {
                console.log(response.data);
                window.location.reload();
            })
            .catch(e => {
                console.log(e);
            });
    };





    const classes = useStyles();

    return (
<div>
        <form onSubmit={handleSubmit}>
            <br></br><br></br>
            <div className={classNames(classes.main, classes.mainRaised)}>
                <div className={classes.container}>
                    <GridContainer justify="center">
         <GridItem xs={12} sm={12} md={11}>
            <CustomInput
                labelText=' question...'
                id="name"
                formControlProps={{
                    fullWidth:true,
                    disabled: false
                }}
                inputProps={{
                    onChange: handleChange('name'),
                    type: "name",

                }}
            />

         </GridItem>
                    </GridContainer>
                </div>
            </div>
            <br></br><br></br><br></br>
                        <div className={classNames(classes.main, classes.mainRaised)}>
                            <div className={classes.container}>
                                <GridContainer justify="center">

        <div className="App">
            {inputList.map( (x, i ) => {
                return (

                    <div className={classes.question}>
                        <br></br><br></br><br></br>
                        <FormGroup>
                                <FormControlLabel
                                    control={
                                        <Switch checked={ i.state }  onChange={e => handleInputChangeOption(e, i) } name={(i + 1)}/>}
                                    label="True"
                                    labelPlacement="start"
                                />
                        </FormGroup>

                        <TextField
                            id="outlined-search"
                            label={"Option " + (i + 1)}
                            type="search"
                            className={classes.textField}
                            margin="normal"
                            variant="outlined"
                            name="OptionName"
                            placeholder="Enter OptionName"
                            value={x.OptionName}
                            onChange={e => handleInputChange(e, i)}
                            fullWidth="True"
                        />

                        <input
                            hidden
                            className="ml10"
                            name="OptionId"
                            placeholder={i+1}
                            value={x.OptionId=i+1}
                            onChange={e => handleInputChange(e, i)}
                        />

                        <div className="btn-box">
                            {inputList.length !== 1 && <IconButton aria-label="Delete" className={classes.margin}
                                onClick={() => handleRemoveClick(i)}
                                >
                                <DeleteIcon fontSize="small" />
                                </IconButton>
                            }
                            {inputList.length - 1 === i && i < 3 &&
                                <Fab size="small" color="secondary" aria-label="Add" className={classes.margin}  onClick={handleAddClick}>
                                <AddIcon />
                                </Fab>
                            }
                        </div>
                    </div>

                );
            })}
        </div>
                                    <GridItem xs={12} sm={12} md={11} >
                                        <br></br>
                                    <Button type='submit' color="primary" >
                                        <span className='ml-3'  >{textChange}</span>
                                    </Button>
                                        <br></br><br></br>
                                    </GridItem>

                    </GridContainer>
                </div>
            </div>
        </form>

    <br></br><br></br><br></br>
    <div className={classNames(classes.main, classes.mainRaised)}>
        <div className={classes.container}>
            <br></br>
                    {formDataF.questions.map((row) => row.step._id === StepId ?
                        (
                            <table className={classes.flexRight} >
                                <tr  key={row._id}>

                        <div   className={classes.rot}>
                                <h3 valign="top"  align="left" className={classes.rot} > {row.name}<Tooltip
                                    title="Delete Question"
                                    placement={window.innerWidth > 959 ? "top" : "right"}

                                >
                                    <IconButton size="medium"
                                                className="hidden-button"
                                                color="default"
                                                onClick={() => deleteQuestion(row._id)}
                                    >
                                        <ClearIcon  />
                                    </IconButton>
                                </Tooltip></h3>




                                    <tr  align="left"  >

                                        { row.options.map((fow) =>
                                            row.trueOption.map((gow) =>
                                            (
                                                         <div key={fow._id} className={ fow.OptionId === gow.selectId && gow.selectState==='true' ? classes.rightOption :null } >
                                                             {fow.OptionId === gow.selectId && gow.selectState === 'true'
                                                                 ? <img>

                                                                 </img>
                                                                 :null
                                                             }
                                                             -{fow.OptionName}
                                                         </div>

                                                    )
                                        ))}
                                    </tr>

                        </div>
                                </tr>
                            </table>
                        ) :null)}
            <br></br><br></br>
        </div>
    </div>
</div>

    );
}

export default AddQuestion;
