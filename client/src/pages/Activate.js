import React, {useState, useEffect, useRef} from 'react';
import authSvg from '../assests/welcome.svg';
import { ToastContainer, toast } from 'react-toastify';
import axios from 'axios';
import jwt from 'jsonwebtoken';
import {  isAuth } from '../helpers/auth';
import {  Redirect } from 'react-router-dom';
import image from "../assests/img/bg7.jpg";
import {makeStyles} from "@material-ui/core";
import styles from "../assests/jss/material-kit-react/views/loginPage";
import Dialog from "@material-ui/core/Dialog/Dialog";
import Footer from "../components/Footer/Footer";
import Button from "../dashboard/CustomButtons/Button.js";
import GridItem from "../dashboard/Grid/GridItem.js";
import GridContainer from "../dashboard/Grid/GridContainer.js";
import DialogContent from "@material-ui/core/DialogContent/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import CustomInput from "../components/CustomInput/CustomInput";
import InputAdornment from "@material-ui/core/InputAdornment";
import Email from "@material-ui/core/SvgIcon/SvgIcon";

const useStyles = makeStyles(styles);



const Activate = ({ match }) => {
  const [formData, setFormData] = useState({
    name: '',
    token: '',
    show: true
  });

  useEffect(() => {
    let token = match.params.token;
    let { name } = jwt.decode(token);

    if (token) {
      setFormData({ ...formData, name, token });
    }

    console.log(token, name);
  }, [match.params]);
  const { name, token } = formData;

  const handleSubmit = e => {
    e.preventDefault();

    axios
      .post(`${process.env.REACT_APP_API_URL}/activation`, {
        token
      })
      .then(res => {
        setFormData({
          ...formData,
          show: false
        });
        console.log(formData);
        toast.success(res.data.message);
      })
      .catch(err => {

        toast.error(err.response.data.errors);
      });

  };
  const [open, setOpen] = React.useState(true);
  const handleClose = () => {
    setOpen(false);
  };
  const classes = useStyles();


  return (






  <DialogContent>
    <DialogContentText>
      To change your password, please enter your email address here. We will send you a reset link.
    </DialogContentText>
    <form onSubmit={handleSubmit}>

      <Button type='submit' color="primary">
        Activate
      </Button>
    </form>
    <a href='/RegisterPage'>
      <span className='ml-4'>Sign Up</span>
    </a>
  </DialogContent>
 // <Footer whiteFont />

);
};

export default Activate;
