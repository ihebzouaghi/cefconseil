import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import reportWebVitals from "./reportWebVitals";
import { BrowserRouter as Router ,Route,Switch } from "react-router-dom";
import App from "./App";
import ResetPassword from "./pages/ResetPassword";
import ProfilePage from "./pages/LandingPage/Sections/ProfilePage";
import PrivateRoute from "./Routes/PrivateRoute";
import LandingPage from "./pages/LandingPage/LandingPage";
import LoginPage from "./pages/LandingPage/Sections/LoginPage";
import HomePage from "./pages/HomePage";
import ForgetPassword from "./pages/ForgetPassword";
import RegisterPage from "./pages/LandingPage/Sections/RegisterPage";
import Register from "./pages/Register";
import Activate from "./pages/Activate";
import Calendar from "./pages/calender";
import FullCalendar from "./pages/FullCalendar";
import FilesList from "./pages/UploadAndDownload/components/FilesList";
import MyTraining from "./pages/LandingPage/Sections/MyTraining";
import ChatRoom from "./pages/ChatRoom/App";



ReactDOM.render(
    <Router>
            <Switch>
                    <Route path='/ForgetPassword' exact render={props => <ForgetPassword {...props} />} />
                    <Route path='/users/password/reset/:token' exact render={props => <ResetPassword {...props} />} />
                    <Route path="/landing-page" component={LandingPage} />
                    <Route path="/HomePage" component={HomePage} />
                    <Route path="/LoginPage" component={LoginPage} />
                    <Route path="/ChatRoom" component={ChatRoom} />
                    <Route path="/FullCalendar" component={FullCalendar} />
                    <Route path="/RegisterPage" exact render={props => <RegisterPage {...props} />} />
                    <Route path="/Register" exact render={props => <Register {...props} />} />
                    <Route path="/Calendar" exact render={props => <Calendar {...props} />} />
                    <Route path='/users/activate/:token' exact render={props => <Activate {...props} />} />
                    <Route path='/MyTraining/:id' exact render={props => <MyTraining {...props} />} />
                    <PrivateRoute path="/ProfilePage" exact component={ProfilePage} />

                    {/*<Route component={FilesList} path="/admin/listFile" />*/}


                    <Route path="/" component={App}/>
            </Switch>
    </Router>,
    document.getElementById("root")
);
// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
