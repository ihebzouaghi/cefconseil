const axios = require("axios");

export const ClientService = {
    addClient
};


async function addClient() {
    let response = await axios.post("http://localhost:3000/api/user/add");
    if (response.status === 200) {
        return response.data;
    }
    return "";
}
