import { container, title } from "./../../../../../assests/jss/material-kit-react.js";
import {whiteColor} from "../../../../../dashboard/jss/material-dashboard-react";

const pillsStyle = ( muiBaseTheme => ( {
  section: {
    padding: "50px 0",
    position : "relative",
    left : 0,
  },
  container,
  title: {
    ...title,
    marginTop: "30px",
    minHeight: "32px",
    textDecoration: "none"
  },
  card: {
    maxWidth: 300,
    margin: 30,
    transition: "0.3s",
    boxShadow: "0 8px 40px -12px rgba(0,0,0,0.3)",
    "&:hover": {
      boxShadow: "0 16px 70px -12.125px rgba(0,0,0,0.5)"
    }
  },
  media: {
    paddingTop: "56.25%"
  },
  content: {
    textAlign: "left",
    padding: muiBaseTheme.spacing.unit * 3
  },
  divider: {
    margin: `${muiBaseTheme.spacing.unit * 3}px 0`
  },
  heading: {
    fontWeight: "bold"
  },
  subheading: {
    lineHeight: 1.8
  },
  avatar: {
    display: "inline-block",
    border: "2px solid white",
    "&:not(:first-of-type)": {
      marginLeft: -muiBaseTheme.spacing.unit
    }
  },
  searchWrapper: {
    [muiBaseTheme.breakpoints.down("sm")]: {
      width: "-webkit-fill-available",
      margin: "10px 15px 0"
    },
    display: "inline-block",
    position: 'absolute',
    top : 50,
    right : 100
  },
  search: {
    "& > div": {
      marginTop: "0"
    },
    [muiBaseTheme.breakpoints.down("sm")]: {
      margin: "10px 15px !important",
      float: "none !important",
      paddingTop: "1px",
      paddingBottom: "1px",
      padding: "0!important",
      width: "60%",
      marginTop: "40px",
      "& input": {
        color: whiteColor
      }
    }
  },
  margin: {
    zIndex: "4",
    margin: "0"
  },
  button : {
    position : 'absolute',
    right : 10 ,
    bottom : 0
  },
  button2 : {
    position : 'absolute',
    right : 50 ,
    bottom : 0
  },
  text : {
    fontFamily : 'sans-serif',
    textAlign : 'center'
  },
  noscroll : {
    position : 'relative',
    width : 800,
    height : 400
  },
  button1 : {
    position : 'absolute',
    right : 10 ,
    bottom : 0,
  },
  tooltip: {
    padding: "10px 15px",
    minWidth: "130px",
    color: 'Purple',
    lineHeight: "1.7em",
    background: "#FFFFFF",
    border: "none",
    borderRadius: "3px",
    boxShadow:
        "0 8px 10px 1px rgba(0, 0, 0, 0.14), 0 3px 14px 2px rgba(0, 0, 0, 0.12), 0 5px 5px -3px rgba(0, 0, 0, 0.2)",
    maxWidth: "200px",
    textAlign: "center",
    fontFamily: '"Helvetica Neue",Helvetica,Arial,sans-serif',
    fontSize: "0.875em",
    fontStyle: "normal",
    fontWeight: "400",
    textShadow: "none",
    textTransform: "none",
    letterSpacing: "normal",
    wordBreak: "normal",
    wordSpacing: "normal",
    wordWrap: "normal",
    whiteSpace: "normal",
    lineBreak: "auto",
    position : 'absolute',
    top : 380
  }
}));

export default pillsStyle;
