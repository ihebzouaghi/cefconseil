import "./App.css";
import {Route, Switch} from "react-router-dom";
import addClient from "./pages/addClient";
import ListClient from "./pages/ListClient";
import Login from "./pages/Login";
import React from "react";
import AddTrainerOrLearner from "./pages/AddTrainerOrLearner";
import AdminProfile from "./pages/AdminProfile";
import Dashboard from "./dashboard/layouts/Admin.js";
//import MUIDrawer from "./MUIDrawer";
import "./dashboard/css/material-dashboard-react.css?v=1.9.0";
import List from "./pages/List";
//import FilesList from "./pages/UploadAndDownload/components/FilesList";
import Step from "./pages/Step/Step";
import StepList2 from "./pages/Step/StepList2";
import AddQuestion from "./pages/Quiz/AddQuestion";




export default function App() {
  return (
      <div className="App">

        <Dashboard>
          <Switch>
              <Route path='/addClient' exact render={props => <addClient {...props} />} />
              <Route path='/ListClient' render={props => <ListClient {...props} />} />
              <Route path='/Login' exact render={props => <Login {...props} />} />
              <Route path="/List" component={List} />
              <Route path='/AddTrainerOrLearner' exact render={props => <AddTrainerOrLearner {...props} />} />
              <Route path='/AdminProfile' exact render={props => <AdminProfile {...props} />} />
              <Route path='/addTraining' exact render={props => <addTraining {...props} />} />
              <Route path='/Step' exact render={props => <Step {...props} />} />
              <Route path='/AddQuestion' exact render={props => <AddQuestion {...props} />} />


            <Route render={() => <h2>404</h2>} />
          </Switch>
        </Dashboard>

      </div>
  );
}
