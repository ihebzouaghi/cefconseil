/*eslint-disable*/
import React from "react";
import DeleteIcon from "@material-ui/icons/Delete";
import IconButton from "@material-ui/core/IconButton";
// react components for routing our app without refresh
import {Link, Redirect} from 'react-router-dom';
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Tooltip from "@material-ui/core/Tooltip";
import InstagramIcon from '@material-ui/icons/Instagram';
import FacebookIcon from '@material-ui/icons/Facebook';
import { isAuth, signout} from "../../helpers/auth";


// @material-ui/icons
import { Apps, CloudDownload } from "@material-ui/icons";

// core components
import CustomDropdown from "./../../components/CustomDropdown/CustomDropdown.js";
import Button from "./../../components/CustomButtons/Button.js";

import styles from "./../../assests/jss/material-kit-react/components/headerLinksStyle.js";
import {toast} from "react-toastify";


const useStyles = makeStyles(styles);

export default function HeaderLinks(props, {history}) {
  const classes = useStyles();

  return (
    <List className={classes.list}>
      <ListItem className={classes.listItem}>
        <CustomDropdown
          noLiPadding
          buttonText="Fonctionnalités"
          buttonProps={{
            className: classes.navLink,
            color: "transparent"
          }}
          buttonIcon={Apps}
          dropdownList={[
            <Link to="/" className={classes.dropdownLink}>
              Gestion Administrative
            </Link>,
              <Link to="/ListClient" className={classes.dropdownLink}>
               Formation à Distance
              </Link>
          ]}
        />
      </ListItem>
      <ListItem className={classes.listItem}>
        <Tooltip
          id="instagram-twitter"
          title="Follow us on Instagram"
          placement={window.innerWidth > 959 ? "top" : "left"}
          classes={{ tooltip: classes.tooltip }}
        >
          <Button
            href="https://www.instagram.com/cefconseil/"
            target="_blank"
            color="transparent"
            className={classes.navLink}
          >
              <InstagramIcon color="secondary" >
              </InstagramIcon>
          </Button>
        </Tooltip>
      </ListItem>
      <ListItem className={classes.listItem}>
        <Tooltip
          id="instagram-facebook"
          title="Follow us on facebook"
          placement={window.innerWidth > 959 ? "top" : "left"}
          classes={{ tooltip: classes.tooltip }}
        >
          <Button
            color="transparent"
            href="https://www.facebook.com/cefcons"
            target="_blank"
            className={classes.navLink}
          >
              <FacebookIcon color="primary"></FacebookIcon>
          </Button>
        </Tooltip>
      </ListItem>
        <ListItem className={classes.listItem}>
            {isAuth() ?
                <Link to="/ProfilePage" className={classes.navLink}>
                    Profile </Link>
                :
                <Link to="/RegisterPage" className={classes.navLink}>
                    Register </Link>
            }

        </ListItem>

        <ListItem className={classes.listItem}>
            {isAuth() ? <Button
                color="transparent"
                className={classes.navLink}
                onClick={() => {
                    signout(() => {
                        window.location.href = '/landing-page';
                    });
                }}
            >
                Logout </Button>
                :
                <Link to="/LoginPage" className={classes.navLink}>
                    Login</Link>
            }
        </ListItem>
    </List>
  );
}
