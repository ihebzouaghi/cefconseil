import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
// creates a beautiful scrollbar
import PerfectScrollbar from "perfect-scrollbar";
import "perfect-scrollbar/css/perfect-scrollbar.css";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// core components
import Navbar from "./../Navbars/Navbar.js";
import Footer from "./../Footer/Footer.js";
import Sidebar from "./../Sidebar/Sidebar.js";
import FixedPlugin from "./../FixedPlugin/FixedPlugin.js";

import routes from "./../routes.js";

import styles from "./../jss/material-dashboard-react/layouts/adminStyle.js";

import bgImage from "./../img/sidebar-2.jpg";
import logo from "../img/reactlogo.png";
import FilesList from "../../pages/UploadAndDownload/components/FilesList";
import UploadAndDownload from "../../pages/UploadAndDownload/components/App";
import TrainingList from "../../pages/Training/TrainingList";
import AddTraining from "../../pages/Training/AddTraining";
import EditTraining from "../../pages/Training/EditTraining";
import Step from "../../pages/Step/Step";
import AddQuestion from "../../pages/Quiz/AddQuestion";
import {isAuth} from "../../helpers/auth";
import Login from "../../pages/Login";
import { ConfirmProvider } from 'material-ui-confirm';
import StepList2 from "../../pages/Step/StepList2";

let ps;

const switchRoutes = (
    <ConfirmProvider>
  <Switch>
    <Route path={"/admin" + "/listFile"} component={FilesList}/>
    <Route path={"/admin" + "/UAD"} component={UploadAndDownload}/>
    <Route path={"/admin" + "/Step"} component={Step}/>



    <Route path={"/admin" + "/AddQuestion/:StepId"} component={AddQuestion}/>

    <Route path='/TrainingList' exact render={props => <TrainingList {...props} />} />
    <Route path='/AddTraining' exact render={props => <AddTraining {...props} />} />
    <Route path='/EditTraining/:id' exact render={props => <EditTraining {...props} />} />



    {routes.map((prop, key) => {
      if (prop.layout === "/admin" &&  isAuth() ) {
        return (
          <Route
            path={prop.layout + prop.path}
            component={prop.component}
            key={key}
          />
        );
      }
      //return null;
        return(
            <Route path='/Login' exact render={props => <Login {...props} />} />
        );
    })}
    <Redirect from="/admin" to="/admin/dashboard" />
  </Switch>
    </ConfirmProvider>
);

const useStyles = makeStyles(styles);

export default function Admin({ ...rest }) {
  // styles
  const classes = useStyles();
  // ref to help us initialize PerfectScrollbar on windows devices
  const mainPanel = React.createRef();
  // states and functions
  const [image, setImage] = React.useState(bgImage);
  const [color, setColor] = React.useState("blue");
  const [fixedClasses, setFixedClasses] = React.useState("dropdown");
  const [mobileOpen, setMobileOpen] = React.useState(false);
  const handleImageClick = image => {
    setImage(image);
  };
  const handleColorClick = color => {
    setColor(color);
  };
  const handleFixedClick = () => {
    if (fixedClasses === "dropdown") {
      setFixedClasses("dropdown show");
    } else {
      setFixedClasses("dropdown");
    }
  };
  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };
  const getRoute = () => {
    return window.location.pathname !== "/admin/maps";
  };
  const resizeFunction = () => {
    if (window.innerWidth >= 960) {
      setMobileOpen(false);
    }
  };
  // initialize and destroy the PerfectScrollbar plugin
  React.useEffect(() => {
    if (navigator.platform.indexOf("Win") > -1) {
      ps = new PerfectScrollbar(mainPanel.current, {
        suppressScrollX: true,
        suppressScrollY: false
      });
      document.body.style.overflow = "hidden";
    }
    window.addEventListener("resize", resizeFunction);
    // Specify how to clean up after this effect:
    return function cleanup() {
      if (navigator.platform.indexOf("Win") > -1) {
        ps.destroy();
      }
      window.removeEventListener("resize", resizeFunction);
    };
  }, [mainPanel]);



  //button from left const
















  return (

    <div className={classes.wrapper}>

      <Sidebar
        routes={routes}
        logoText={"CEF Conseil"}
        logo={logo}
        image={image}
        handleDrawerToggle={handleDrawerToggle}
        open={mobileOpen}
        color={color}
        {...rest}
      />

      <div className={classes.mainPanel} ref={mainPanel}>
        <Navbar
          routes={routes}
          handleDrawerToggle={handleDrawerToggle}
          {...rest}
        />
        {/* On the /maps route we want the map to be on full screen - this is not possible if the content and conatiner classes are present because they have some paddings which would make the map smaller */}
        {getRoute() ? (
          <div className={classes.content}>
            <div className={classes.container}>{switchRoutes}</div>
          </div>
        ) : (
          <div className={classes.map}>{switchRoutes}</div>
        )}
        {getRoute() ? <Footer /> : null}
        <FixedPlugin
          handleImageClick={handleImageClick}
          handleColorClick={handleColorClick}
          bgColor={color}
          bgImage={image}
          handleFixedClick={handleFixedClick}
          fixedClasses={fixedClasses}
        />
      </div>
    </div>

  );
}
