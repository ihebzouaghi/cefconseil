/*!

=========================================================
* Material Dashboard React - v1.9.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-react
* Copyright 2020 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/material-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
// @material-ui/icons
import addClient from "./../pages/addClient.js";
import ListClient from "./../pages/ListClient.js";
import AddTrainerOrLearner from "./../pages/AddTrainerOrLearner.js";
import AdminProfile from "./../pages/AdminProfile.js";
import TrainingList from "./../pages/Training/TrainingList.js";
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import GroupAddIcon from '@material-ui/icons/GroupAdd';
import PersonIcon from '@material-ui/icons/Person';
import ListIcon from '@material-ui/icons/List';
import StepList2 from "../pages/Step/StepList2.js";

const dashboardRoutes = [

  {
    path: "/addClient",
    name: "New Client",
    rtlName: "",
    icon: PersonAddIcon,
    component: addClient,
    layout: "/admin"
  },
  {
    path: "/ListClient",
    name: "Client list",
    rtlName: "",
    icon: ListIcon,
    component: ListClient,
    layout: "/admin"
  },
  {
    path: "/AddTrainerOrLearner",
    name: "New Trainer/Learner",
    rtlName: "",
    icon: GroupAddIcon,
    component: AddTrainerOrLearner,
    layout: "/admin"
  },
  {
    path: "/AdminProfile",
    name: "Profile",
    rtlName: "",
    icon: PersonIcon,
    component: AdminProfile,
    layout: "/admin"
  },
  {
    path: "/TrainingList",
    name: "TrainingList",
    rtlName: "",
    icon: ListIcon,
    component: TrainingList,
    layout: "/admin"
  },
  {
    path: "/StepList2",
    name: "StepList",
    rtlName: "",
    icon: ListIcon,
    component: StepList2,
    layout: "/admin"
  },

];

export default dashboardRoutes;
