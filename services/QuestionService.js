const Question = require("../models/Question");

exports.addQuestion = async function (document) {
    try {
        const content = await Question.create(document);
        return content;
    } catch (e) {
        console.log(e);
        throw Error("Error while creating new Question");
    }
};


exports.getQuestion = async function (query) {
    try {
        Question.find()
        const question = await Question.find(query);
        return question;
    } catch (e) {
        throw Error("Error while Paginating Question");
    }
};

exports.getQuestionById = async function (id) {
    try {
        const question = await Question.findById(id);
        return question;
    } catch (e) {
        throw Error("Error while finding Question");
    }
};

exports.updateQuestion = async function (id, data, next ) {
    try {
        const content = await Question.findOneAndUpdate(id, data, {new: true}, next);
        return content;
    } catch (e) {
        throw Error("Error while updating Question");
    }
};
