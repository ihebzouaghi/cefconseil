const Step = require("../models/Step");

exports.addStep = async function (document) {
    try {
        const content = await Step.create(document);
        return content;
    } catch (e) {
        console.log(e);
        throw Error("Error while creating new Step");
    }
};


exports.getStep = async function (query) {
    try {
        Step.find()
        const step = await Step.find(query);
        return step;
    } catch (e) {
        throw Error("Error while Paginating Step");
    }
};

exports.getStepById = async function (id) {
    try {
        const step = await Step.findById(id);
        return step;
    } catch (e) {
        throw Error("Error while finding");
    }
};

exports.updateStep = async function (id, data, next ) {
    try {
        const content = await Step.findOneAndUpdate(id, data, {new: true}, next);
        return content;
    } catch (e) {
        throw Error("Error while updating");
    }
};
