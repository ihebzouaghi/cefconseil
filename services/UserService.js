//import {isValidId} from "../helpers/valid";
var User = require("../models/User");
exports.getUser = async function (query, res, limit) {
    try {
        User.find()
        var user = await User.find(query);
        return user;
    } catch (e) {
        throw Error("Error while Paginating users");
    }
};
exports.addUser = async function (document) {
    try {
        var content = await User.create(document);
        return content;
    } catch (e) {
        console.log(e);
        throw Error("Error while creating new User");
    }
};
exports.getUserById = async function (id) {
    try {
        var user = await User.findById(id);
        return user;
    } catch (e) {
        throw Error("Error while finding");
    }
};
exports.removeUser = async function (id) {
    try {
        var content = await User.findByIdAndDelete(id);
        return content;
    } catch (e) {
        throw Error("Error while deleting the user");
    }
};
/*
exports.updateUser = async function (id, data) {
    try {
        var content = await User.findByIdAndUpdate(id, data);
        return content;
    } catch (e) {
        throw Error("Error while updating the user");
    }
};
exports.getAll = async function () {
    const users = await User.find();
    return users.map(x => basicDetails(x));
};
exports.getById = async function (id) {
    const user = await getUser(id);
    return basicDetails(user);
};
exports.getUser = async function (id) {
    if (isValidId(id)) throw 'User not found';
    const user = await User.findById(id);
    if (!user) throw 'User not found';
    return user;
};
function basicDetails(user) {
    const { id, name, email, password, role } = user;
    return { id, name, email, password, role };
};
*/
