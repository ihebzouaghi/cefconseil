var Training = require("../models/Training");

exports.getTraining = async function (query, res, limit) {
    try {
        Training.find();
        var training = await Training.find(query);
        return training;
    } catch (e) {
        throw Error("Error while Paginating trainings");
    }
};
exports.addTraining = async function (document) {
    try {
        var content = await Training.create(document);
        return content;
    } catch (e) {
        console.log(e);
        throw Error("Error while creating new training");
    }
};
exports.getTrainingById = async function (id) {
    try {
        var training = await Training.findById(id);
        return training;
    } catch (e) {
        throw Error("Error while finding");
    }
};
exports.updateTraining = async function (id, data, next) {
    try {
        var content = await Training.findByIdAndUpdate(id, data , { new: true }, next);
        return content;
    } catch (e) {
        throw Error("Error while updating");
    }
};


