var Program = require("../models/Program");

exports.getProgram = async function (query, res, limit) {
    try {
        Program.find()
        var program = await Program.find(query);
        return program;
    } catch (e) {
        throw Error("Error while Paginating Programs");
    }
};
exports.addProgram = async function (document) {
    try {
        var content = await Program.create(document);
        return content;
    } catch (e) {
        console.log(e);
        throw Error("Error while creating new Program");
    }
};
