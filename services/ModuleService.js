var Module = require("../models/Module");
var Training = require("../models/Training");

exports.addModule = async function (document) {
    try {
        const content = await Module.create(document);
        return content;
    } catch (e) {
        console.log(e);
        throw Error("Error while creating new Module");
    }
};

exports.getModule = async function (query) {
    try {
        Module.find()
        const module = await Module.find(query);
        return module;
    } catch (e) {
        throw Error("Error while Paginating Module");
    }
};

exports.getModuleById = async function (id) {
    try {
        var module = await Module.findById(id);
        return module;
    } catch (e) {
        throw Error("Error while finding");
    }
};

exports.getModuleByTrainingId = async function (id) {
    try {
        var module = await Module.findById(id);
        return module;
    } catch (e) {
        throw Error("Error while finding");
    }
};

