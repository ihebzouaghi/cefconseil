const Participation = require("../models/Participation");

exports.addParticipation = async function (document) {
    try {
        const content = await Participation.create(document);
        return content;
    } catch (e) {
        console.log(e);
        throw Error("Error while creating new Participation");
    }
};


exports.getParticipation = async function (query) {
    try {
        Participation.find()
        const participation = await Participation.find(query);
        return participation;
    } catch (e) {
        throw Error("Error while Paginating Participation");
    }
};
