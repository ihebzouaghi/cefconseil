const File = require("../models/File");

exports.getFiles = async function (query) {
    try {
        File.find()
        const file = await File.find(query);
        return file;
    } catch (e) {
        throw Error("Error while Paginating File");
    }
};

exports.getFileById = async function (id) {
    try {
        const file = await File.findById(id);
        return file;
    } catch (e) {
        throw Error("Error while finding File");
    }
};
