const express = require("express");
const router = express.Router();
var Program = require("../models/Program");
var ProgramController = require("../controller/ProgramController");

router.post("/add", ProgramController.addProgram);
router.get("/", ProgramController.getProgram);

module.exports = router;
