const express = require("express");
const router = express.Router();
const { v4: uuidV4 } = require ('uuid');
const Step = require("../models/Step");
const StepController = require("../controller/StepController");


router.post("/add", StepController.addStep);
router.get("/get", StepController.getStep);
router.get('/:id', StepController.getStepById);
router.put('/update/:id' , StepController.updateStep);
router.delete('/delete/:id', (req, res) => {
    Step.findByIdAndRemove(req.params.id)
        .then(() => res.send('Step deleted'))
        .catch(err => res.status(400).send('Error: ' + err))
});

module.exports = router;
