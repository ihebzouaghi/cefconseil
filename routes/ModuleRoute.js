const express = require("express");
const router = express.Router();
const Module = require("../models/Module");
const ModuleController = require("../controller/ModuleController");

router.post("/add", ModuleController.addModule);
router.get("/", ModuleController.getModule);
router.get("/:id", ModuleController.getModuleById);
router.get("/all/:training", ModuleController.getModuleByTrainingId);

module.exports = router;
