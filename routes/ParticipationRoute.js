const express = require("express");
const router = express.Router();
var Participation = require("../models/Participation");
var User = require("../models/User");
var ParticipationController = require("../controller/ParticipationController");

router.post("/add", ParticipationController.addParticipation);
router.get("/", ParticipationController.getParticipation);
router.get("/my/:learner" , ParticipationController.getParticipationByLearner);
router.get("/:id" , ParticipationController.getParticipationById);
router.post("/likes/:id" , ParticipationController.addLikes);

router.delete('/delete/:id/:idd', async (req, res) => {
    try {
    const post = await Participation.findById(req.params.idd).populate('learner');
    const user = await User.findById(req.params.id);

    const removeIndex = post.learner
        .map((like) => like.id.toString())
        .indexOf(user._id);

    post.learner.splice(removeIndex, 1);
    await post.save();
    res.json(post.learner);
    console.log("Member removed");
} catch (err) {
    console.error(err.message);
    res.status(500).json("Server Error");
}
});

/*const index = Participation.learner.indexOf(req.params.id).populate('learner');
if (index > -1) {
         Participation.splice(removeIndex , 1 )
        .then(() => res.send('Member deleted'))
        .catch(err => res.status(400).send('Error: ' + err)) }

             const  index  = await  course.learner.indexOf(req.params.id)  ;
    if (index >= 0) {
        course.learner.splice(index , 1);
        await course.save()
        .then(() => res.send('Member deleted'))
        .catch(err => res.status(400).send('Error: ' + err))
    }
    else {
    }


@Authorized(['student'])
@Post('/:id/leave')
async leaveStudent(@Param('id') id: string, @CurrentUser() currentUser: IUser) {
    const course = await Course.findById(id);
    if (!course) {
        throw new NotFoundError();
    }
    const index: number = course.students.indexOf(currentUser._id);
    if (index >= 0) {
        course.students.splice(index, 1);
        await NotificationSettings.findOne({'user': currentUser, 'course': course}).remove();
        await course.save();
        return {};
    } else {
        // This equals an implicit !course.checkPrivileges(currentUser).userIsCourseStudent check.
        throw new ForbiddenError();
    }
}*/


//post like
router.put("/like/:id/:idd"  ,  async (req, res) => {
    try {
        const post = await Participation.findById(req.params.id).populate('learner');

        const user = await User.findById(req.params.idd);

        if (post.likes.filter((like) => like.user.toString() === user._id).length > 0)
        {
            return res.status(400).json({ msg: "Post already liked" });
        }

        post.likes.unshift({ user: user._id });

        await post.save();
        res.json(post.likes);
        console.log("Post liked");
    } catch (err) {
        console.error(err.message);
        res.status(500).json("Server Error");
    }
});

//post unlike
router.put("/unlike/:id/:idd", async (req, res) => {
    try {
        const post = await Participation.findById(req.params.id);
        const user = await User.findById(req.params.idd);

        if (
            post.likes.filter((like) => like.user.toString() === user._id)
                .length === 0
        ) {
            return res.status(400).json({ msg: "Post has not yet been liked" });
        }

        const removeIndex = post.likes
            .map((like) => like.user.toString())
            .indexOf(user._id);

        post.likes.splice(removeIndex, 1);
        await post.save();
        res.json(post.likes);
        console.log("Post Unliked");
    } catch (err) {
        console.error(err.message);
        res.status(500).json("Server Error");
    }
});

//post like
router.put("/dislike/:id/:idd", async (req, res) => {
    try {
        const post = await Participation.findById(req.params.id);
        const user = await User.findById(req.params.idd);

        if (
            post.dislikes.filter((like) => like.user.toString() === user._id).length >
            0
        ) {
            return res.status(400).json({ msg: "Post already Disliked" });
        }
        post.dislikes.unshift({ user: user._id });

        await post.save();
        res.json(post.dislikes);
        console.log("Post disliked");
    } catch (err) {
        console.error(err.message);
        res.status(500).json("Server Error");
    }
});

//post unlike
router.put("/undislike/:id/:idd", async (req, res) => {
    try {
        const post = await Participation.findById(req.params.id);
        const user = await User.findById(req.params.idd);

        if (
            post.dislikes.filter((like) => like.user.toString() === user._id)
                .length === 0
        ) {
            return res.status(400).json({ msg: "Post has not yet been disliked" });
        }

        const removeIndex = post.dislikes
            .map((like) => like.user.toString())
            .indexOf(user._id);

        post.dislikes.splice(removeIndex, 1);
        await post.save();
        res.json(post.dislikes);
        console.log("Post Undisliked");
    } catch (err) {
        console.error(err.message);
        res.status(500).json("Server Error");
    }
});

module.exports = router;
