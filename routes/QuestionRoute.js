const express = require("express");
const router = express.Router();
const { v4: uuidV4 } = require ('uuid');
const Question = require("../models/Question");
const QuestionController = require("../controller/QuestionController");


router.post("/add", QuestionController.addQuestion);
router.get("/get", QuestionController.getQuestion);
router.get('/:id', QuestionController.getQuestionById);
router.put('/update/:id' , QuestionController.updateQuestion);
router.delete('/delete/:id', (req, res) => {
    Question.findByIdAndRemove(req.params.id)
        .then(() => res.send('Step deleted'))
        .catch(err => res.status(400).send('Error: ' + err))
});

module.exports = router;
