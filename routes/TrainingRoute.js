const express = require("express");
const router = express.Router();
const multer = require('multer');
const { v4: uuidV4 } = require ('uuid');
var Training = require("../models/Training");
var TrainingController = require("../controller/TrainingController");

router.post("/add", TrainingController.addTraining);
router.get("/", TrainingController.getTraining);
router.get('/:id', TrainingController.getTrainingById);
router.put('/update/:id' , TrainingController.updateTraining);
//router.delete('/delete/:id' , TrainingController.deleteTraining);

router.delete('/delete/:id', (req, res) => {
    Training.findByIdAndRemove(req.params.id)
        .then(() => res.send('Training deleted'))
        .catch(err => res.status(400).send('Error: ' + err))
});



const DIR = './client/public/images/training';

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, DIR);
    },
    filename: (req, file, cb) => {
        const fileName = file.originalname.toLowerCase().split(' ').join('-');
        cb(null, uuidV4() + '-' + fileName)
    }
});

var upload = multer({
    storage: storage,
    fileFilter: (req, file, cb) => {
        if (file.mimetype == "image/png" || file.mimetype == "image/jpg" || file.mimetype == "image/jpeg") {
            cb(null, true);
        } else {
            cb(null, false);
            return cb(new Error('Only .png, .jpg and .jpeg format allowed!'));
        }
    }
});

router.post('/upload', upload.single('image'), (req, res, next) => {
    const url = req.protocol + '://' + req.get('host');
    Training.findOneAndUpdate({_id : req.body._id} ,  {image:  req.file.filename } , {res: true} , function (err,u) {
        if (err) res.json(err);
        else res.json(u)
    });

});

module.exports = router;
