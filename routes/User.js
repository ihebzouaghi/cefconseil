const express = require("express");
const router = express.Router();
const multer = require('multer');
const { v4: uuidV4 } = require ('uuid');
var User = require("../models/User");
var UserController = require("../controller/UserController");
const { requireSignin, adminMiddleware } = require('../controller/Authentication');
const { readController, updateController } = require('../controller/UserController');

router.get('/:id', requireSignin, readController);
router.put('/update', requireSignin, updateController);
router.put('/admin/update', requireSignin, adminMiddleware, updateController);
router.post("/add", UserController.addUser);
router.get("/", UserController.getUser);

router.delete('/delete/:id', (req, res) => {
    User.findByIdAndRemove(req.params.id)
        .then(() => res.send('Exercise deleted'))
        .catch(err => res.status(400).send('Error: ' + err))
});



const DIR = './client/public/images/';

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, DIR);
    },
    filename: (req, file, cb) => {
        const fileName = file.originalname.toLowerCase().split(' ').join('-');
        cb(null, uuidV4() + '-' + fileName)
    }
});

var upload = multer({
    storage: storage,
    fileFilter: (req, file, cb) => {
        if (file.mimetype == "image/png" || file.mimetype == "image/jpg" || file.mimetype == "image/jpeg" ) {
            cb(null, true);
        } else {
            cb(null, false);
            return cb(new Error('Only .png, .jpg and .jpeg format allowed!'));
        }
    }
});

router.post('/upload', upload.single('image'), (req, res, next) => {
    const url = req.protocol + '://' + req.get('host');
    User.findOneAndUpdate({_id : req.body._id} ,  {image:  req.file.filename } , {res: true} , function (err,u) {
        if (err) res.json(err);
        else res.json(u)
    });

});

/*router.delete('/delete/:id', (req, res) => {
    User.findByIdAndRemove(req.params.id)
        .then(() => res.send('Exercise deleted'))
        .catch(err => res.status(400).send('Error: ' + err))
});
 */

module.exports = router;
