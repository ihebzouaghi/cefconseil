const path = require('path');
const express = require('express');
const multer = require('multer');
const File = require('../models/File');
const Router = express.Router();
const FileController = require("../controller/FileController");

const upload = multer({
    storage: multer.diskStorage({
        destination(req, file, cb) {
            cb(null, './client/public/images/files');
        },
        filename(req, file, cb) {
            cb(null, `${new Date().getTime()}_${file.originalname}`);
        }
    }),
    limits: {
        fileSize: 10000000 // max file size 10MB = 10000000 bytes
    },
    fileFilter(req, file, cb) {
        if (!file.originalname.match(/\.(jpeg|jpg|png|pdf|doc|docx|xlsx|xls|PNG|JPG|mp4|mp3)$/)) {
            return cb(
                new Error(
                    'only upload files with jpg, jpeg, png, pdf, doc, docx, xslx, xls , mp3, mp4 format.'
                )
            );
        }
        cb(undefined, true); // continue with upload
    }
});

Router.post(
    '/upload',
    upload.single('file'),
    async (req, res) => {
        try {
            const { step, title, description } = req.body;
            //const { path, mimetype } = String(req.file).slice(27);
            const { path, mimetype } = req.file;
            const file = new File({
                step,
                title,
                description,
                file_path: path,
                file_mimetype: mimetype
            });
            await file.save();
            res.send('file uploaded successfully.');
        } catch (error) {
            res.status(400).send('Error while uploading file. Try again later.');
        }
    },
    (error, req, res, next) => {
        if (error) {
            res.status(500).send(error.message);
        }
    }
);

Router.get('/getAllFiles', async (req, res) => {
    try {
        const files = await File.find({})
            .populate('step')
            .populate({
                path: 'step',
                populate: {path: 'module',
                    populate: {path: 'training'}
                }

            })

        const sortedByCreationDate = files.sort(
            (a, b) => b.createdAt - a.createdAt
        );
        res.send(sortedByCreationDate);
    } catch (error) {
        res.status(400).send('Error while getting list of files. Try again later.');
    }
});


Router.get('/download/:id', async (req, res) => {
    try {
        const file = await File.findById(req.params.id);
        res.set({
            'Content-Type': file.file_mimetype
        });
        res.sendFile(path.join(__dirname, '..', file.file_path));
    } catch (error) {
        res.status(400).send('Error while downloading file. Try again later.');
    }
});



Router.get("/get", FileController.getFiles);

Router.delete('/delete/:id', (req, res) => {
    File.findByIdAndRemove(req.params.id)
        .then(() => res.send('File deleted'))
        .catch(err => res.status(400).send('Error: ' + err))
});
module.exports = Router;
